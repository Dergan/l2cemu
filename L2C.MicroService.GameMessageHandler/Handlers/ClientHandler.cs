﻿using L2C.MicroService.GenericLib;
using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.QueueMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2C.MicroService.GameMessageHandler.Handlers
{
    public class ClientHandler
    {
        private MessageBroker msgBroker;
        private Guid ConnectionId;

        public ClientHandler(QM_NewGameClient InitMessage, MessageBroker MsgBroker)
        {
            this.msgBroker = MsgBroker;
            this.ConnectionId = InitMessage.ConnectionId;
            this.msgBroker.SubscribeAsync<QM_GameData>(QueueMessageServiceType.GameMessageHandler, InitMessage.ConnectionId, HandleGameData);
            //this.msgBroker.SubscribeAsync<QM_ClientInit>(QueueMessageServiceType.LoginMessageHandler, InitMessage.ConnectionId, HandleClientInit);
        }

        //private Task HandleClientInit(QM_ClientInit Message)
        //{
        //    msgBroker.SendMessage(new GM_LoginInit(Message.ScrambledRsaModulus, Message.BlowfishKey), QueueMessageServiceType.LoginServer, Message.ConnectionId);
        //    Console.WriteLine("LoginMessageHandler.HandleClientInit");
        //    return Task.CompletedTask;
        //}

        private Task HandleGameData(QM_GameData Message)
        {
            GameMessageBase GameMessage = MessageHandler.DeserializeMessage(Message.Data);

            if(GameMessage != null)
            {
                Console.WriteLine($"[GameMessageHandler.HandleGameData] Message From game \"{GameMessage.GetType().Name}\" Forward it to BasicPlayer service");
                msgBroker.SendMessage(GameMessage, QueueMessageServiceType.BasicPlayerService, this.ConnectionId);
            }
            else
            {
                Console.WriteLine($"[GameMessageHandler.HandleGameData] not implemented yet: {BitConverter.ToString(Message.Data.Take(10).ToArray())}");
            }

            return Task.CompletedTask;
        }
    }
}
