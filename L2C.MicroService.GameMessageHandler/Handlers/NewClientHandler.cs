﻿using L2C.MicroService.GameMessageHandlerLib.GameMessages.Receive;
using L2C.MicroService.GenericLib;
using L2C.MicroService.GenericLib.QueueMessages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace L2C.MicroService.GameMessageHandler.Handlers
{
    public class NewClientHandler : SubscribeBase<QM_NewGameClient>
    {
        public List<ClientHandler> Clients { get; private set; }

        public NewClientHandler(MessageBroker MsgBroker)
            : base(MsgBroker)
        {
            this.Clients = new List<ClientHandler>();

            //initialize MessageHandler for receive
            MessageHandler.AddMessage(typeof(GM_RequestGameLogin));
            MessageHandler.AddMessage(typeof(GM_ProtocolVersion));
            MessageHandler.AddMessage(typeof(GM_SecondPasswordCheck));
            MessageHandler.AddMessage(typeof(GM_RequestGameStart));
            MessageHandler.AddMessage(typeof(GM_RequestEnterWorld));
        }

        public override Task HandleMessage(QM_NewGameClient Message)
        {
            ClientHandler clientHandler = new ClientHandler(Message, MsgBroker);
            Clients.Add(clientHandler);

            base.MsgBroker.SendMessage(Message, QueueMessageServiceType.BasicPlayerService);

            return Task.CompletedTask;
        }
    }
}
