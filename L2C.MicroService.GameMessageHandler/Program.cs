﻿using L2C.MicroService.GameMessageHandler.Handlers;
using L2C.MicroService.GenericLib;
using L2C.MicroService.GenericLib.QueueMessages;
using System;

namespace L2C.MicroService.GameMessageHandler
{
    class Program
    {
        static MessageBroker MsgBroker;
        static NewClientHandler newClientHandler;

        static void Main(string[] args)
        {
            MicroServiceSettings settings = ConfigurationHandler.LoadConfig<MicroServiceSettings>("AppSettings.json", "L2C:MicroServiceSettings");
            MsgBroker = new MessageBroker(settings.MessageBroker);
            newClientHandler = new NewClientHandler(MsgBroker);

            MsgBroker.SubscribeAsync<QM_NewGameClient>("", newClientHandler.HandleMessage, (config) => config.WithQueueName(QueueMessageServiceType.GameMessageHandler.ToString()));

            Console.WriteLine("Game Message Handler is running...");
            Console.ReadLine();
        }
    }
}
