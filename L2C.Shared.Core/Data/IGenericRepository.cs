﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace L2C.Shared.Core.Data
{
    public interface IGenericRepository<TEntity> : IQueryable<TEntity>, IDisposable
        where TEntity : class
    {

        IQueryable<TEntity> Query();
        Task Create(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(TEntity entity);
    }
}
