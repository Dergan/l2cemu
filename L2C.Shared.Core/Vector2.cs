﻿namespace L2C.Shared.Core
{
    public class Vector2
    {

        public uint X { get; set; }
        public uint Y { get; set; }
    }
}
