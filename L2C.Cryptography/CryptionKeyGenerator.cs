﻿using System;
using System.Threading.Tasks;

namespace L2C.Cryptography
{
    public class ScrambledKeyPairGenerator : ICryptKeyGenerator<ScrambledKeyPair>
    {
        private int _scrambledCount = 1;
        private ScrambledKeyPair[] _scrambledPairs;
        private Random rnd = new Random(DateTime.Now.Millisecond);

        public ScrambledKeyPairGenerator()
        {
            Initialize();
        }

        public void Initialize()
        {
            Task.WaitAll(GenerateScrambledPairs());
        }

        private async Task GenerateScrambledPairs()
        {
            await Task.Run(() =>
            {
                _scrambledPairs = new ScrambledKeyPair[_scrambledCount];
                for (int i = 0; i < _scrambledCount; i++)
                {
                    _scrambledPairs[i] = new ScrambledKeyPair();
                }
            }).ConfigureAwait(false);
        }

        public ScrambledKeyPair Generate()
        {
            return _scrambledPairs[rnd.Next((short)(_scrambledCount - 1))];
        }
    }

    public class BlowfishKeyGenerator : ICryptKeyGenerator<byte[]>
    {
        private Random rnd = new Random(DateTime.Now.Millisecond);

        public BlowfishKeyGenerator()
        {
        }

        public byte[] Generate()
        {
            byte[] key = new byte[16];
            rnd.NextBytes(key);
            return key;
        }
    }

    public class XorKeyGenerator : ICryptKeyGenerator<byte[]>
    {
        private Random rnd = new Random(DateTime.Now.Millisecond);

        public byte[] Generate()
        {
            byte[] key = new byte[16];
            rnd.NextBytes(key);

            // the last 8 bytes are static
            key[8] = (byte)0xc8;
            key[9] = (byte)0x27;
            key[10] = (byte)0x93;
            key[11] = (byte)0x01;
            key[12] = (byte)0xa1;
            key[13] = (byte)0x6c;
            key[14] = (byte)0x31;
            key[15] = (byte)0x97;
            return key;
        }
    }
}