﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2C.Cryptography
{
    public interface ICryptEngine
    {
        byte[] Encrypt(byte[] data, int offset, int count);
        byte[] Decrypt(byte[] data, int offset, int count);
        void SetKey(byte[] key);
    }
}
