﻿namespace L2C.Cryptography
{
    interface ICryptKeyGenerator<T>
    {

        T Generate();
    }
}
