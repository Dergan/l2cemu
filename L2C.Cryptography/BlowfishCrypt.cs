﻿using System;
using System.Security.Cryptography;

namespace L2C.Cryptography
{
    public class BlowfishCrypt : ICryptEngine
    {
        private static byte[] DefaultKey = new byte[] { 0x6b, 0x60, 0xcb, 0x5b, 0x82, 0xce, 0x90, 0xb1, 0xcc, 0x2b, 0x6c, 0x55, 0x6c, 0x6c, 0x6c, 0x6c };

        public byte[] Key { get; private set; } = DefaultKey;
        public byte[] NextKey { get; private set; }
        private BlowfishCipher cipher;
        private bool _keyUpdated = false;

        public BlowfishCrypt()
        {
            cipher = new BlowfishCipher(Key);
        }

        public byte[] Encrypt(byte[] data, int offset, int count)
        {
            var ret = new byte[count];
            Array.Copy(data, offset, ret, 0, count);
            cipher.Encrypt(ret, 0, count);
            if (_keyUpdated == false)
            {
                cipher.Key = NextKey;
                _keyUpdated = true;
            }
            return ret;
        }

        public byte[] Decrypt(byte[] data, int offset, int count)
        {
            var ret = new byte[count];
            Array.Copy(data, offset, ret, 0, count);
            cipher.Decrypt(ret, 0, count);
            if (_keyUpdated == false)
            {
                cipher.Key = NextKey;
                _keyUpdated = true;
            }
            return ret;
        }

        public static BlowfishCrypt GenerateKey()
        {
            BlowfishCrypt blowfish = new BlowfishCrypt();
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                byte[] nextKey = new byte[16];
                rng.GetBytes(nextKey);
                blowfish.SetKey(nextKey);
            }
            return blowfish;
        }

        public void SetKey(byte[] key)
        {
            NextKey = key;
            _keyUpdated = false;
        }
    }
}
