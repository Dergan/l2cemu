﻿using System;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace L2C.Cryptography
{
    public class XorCrypt : ICryptEngine
    {
        private byte[] _inKey = new byte[16];
        private byte[] _outKey = new byte[16];
        private bool _enabled = false;

        public byte[] GameKey { get; private set; }

        public void SetKey(byte[] key)
        {
            Array.Copy(key, 0, _inKey, 0, 16);
            Array.Copy(key, 0, _outKey, 0, 16);
            this.GameKey = key;
        }

        public byte[] Decrypt(byte[] input, int offset, int size)
        {
            byte[] raw = new byte[size];
            Array.Copy(input, offset, raw, 0, size);

            if (!_enabled)
            {
                return raw;
            }
            byte[] ret = new byte[size];
            int temp = 0;
            for (int i = 0; i < size; i++)
            {
                int temp2 = raw[i] & 0xFF;
                ret[i] = (byte)(temp2 ^ _inKey[i & 15] ^ temp);
                temp = temp2;
            }
            Array.Copy(BitConverter.GetBytes(BitConverter.ToInt32(_inKey, 8) + size), 0, _inKey, 8, 4);
            return ret;
        }

        public byte[] Encrypt(byte[] input, int offset, int size)
        {
            byte[] raw = new byte[size];
            Array.Copy(input, offset, raw, 0, size);

            if (!_enabled)
            {
                _enabled = true;
                return raw;
            }
            byte[] ret = new byte[size];
            int temp = 0;
            for (int i = 0; i < size; i++)
            {
                int temp2 = raw[i] & 0xFF;
                temp = temp2 ^ _outKey[i & 15] ^ temp;
                ret[i] = (byte)temp;
            }
            Array.Copy(BitConverter.GetBytes(BitConverter.ToInt32(_outKey, 8) + size), 0, _outKey, 8, 4);

            return ret;
        }

        public Task EncryptAsync(byte[] data, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public Task DecryptAsync(byte[] data, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public void GenerateNewKey()
        {
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                byte[] NewKey = new byte[16];
                rng.GetBytes(NewKey);

                // the last 8 bytes are static
                NewKey[8] = (byte)0xc8;
                NewKey[9] = (byte)0x27;
                NewKey[10] = (byte)0x93;
                NewKey[11] = (byte)0x01;
                NewKey[12] = (byte)0xa1;
                NewKey[13] = (byte)0x6c;
                NewKey[14] = (byte)0x31;
                NewKey[15] = (byte)0x97;
                SetKey(NewKey);
            }
        }
    }
}
