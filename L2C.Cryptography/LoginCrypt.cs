﻿// 
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.
// 
using System;
using System.Threading.Tasks;

namespace L2C.Cryptography
{
    public class LoginCrypt : ICryptEngine
    {
        private bool updatedKey = false;

        public LoginCrypt()
        {
        }

        public void SetKey(byte[] key)
        {
        }

        public byte[] Decrypt(byte[] data, int offset, int count)
        {
            byte[] ret = new byte[count];
            Array.Copy(data, offset, ret, 0, count);

            if (!updatedKey)
            {
                uint key = BitConverter.ToUInt32(ret, count - 8);
                DecXORPass(ret, 0, count, key);
            }

            if (VerifyChecksum(ret) == false)
                throw new Exception("Checksum failed");

            if (!updatedKey)
            {
                Array.Resize(ref ret, ret.Length - 4);
            }
            Array.Resize(ref ret, ret.Length - 4);
            return ret;
        }

        public byte[] Encrypt(byte[] data, int offset, int count)
        {
            byte[] ret = new byte[count];
            Array.Copy(data, offset, ret, 0, count);

            ret = AppendChecksum(ret, 0);
            if (!updatedKey) ret = AppendChecksum(ret, 0);
            ret = PadData(ret);

            uint checksum = 0;

            if (!updatedKey)
            {
                uint newKey = EncXORPass(ret, (uint)123456789);
                ret = SetKey(ret, newKey);
                updatedKey = true;
            }
            else
            {
                checksum = CalculateChecksum(ret);
                ret = SetChecksum(ret, checksum);
            }

            return ret;
        }

        private byte[] PadData(byte[] data)
        {
            byte[] ret = new byte[(data.Length + 8) - data.Length % 8];
            Array.Copy(data, 0, ret, 0, data.Length);
            return ret;
        }

        private byte[] SetKey(byte[] data, uint newKey)
        {
            byte[] ret = new byte[data.Length];
            Array.Copy(data, 0, ret, 0, data.Length);
            Array.Copy(BitConverter.GetBytes(newKey), 0, ret, data.Length - 4, 4);
            return ret;
        }

        private bool VerifyChecksum(byte[] data)
        {
            return CalculateChecksum(data) == 0;
        }
        private byte[] SetChecksum(byte[] data, uint checksum)
        {
            byte[] ret = new byte[data.Length];
            Array.Copy(data, 0, ret, 0, data.Length);
            Array.Copy(BitConverter.GetBytes(checksum), 0, ret, data.Length - 4, 4);
            return ret;
        }

        private byte[] AppendChecksum(byte[] data, uint checksum)
        {
            byte[] ret = new byte[data.Length + 4];
            Array.Copy(data, 0, ret, 0, data.Length);
            Array.Copy(BitConverter.GetBytes(checksum), 0, ret, data.Length, 4);
            return ret;
        }

        private uint CalculateChecksum(byte[] data)
        {
            uint chksum = 0;
            int count = data.Length - 8;
            int i;
            for (i = 0; i < count; i += 4)
                chksum ^= BitConverter.ToUInt32(data, i);
            return chksum;
        }

        private uint EncXORPass(byte[] data, uint key)
        {
            int stop = data.Length;
            uint ecx = key;
            uint edx;

            for (int i = 4; i < stop; i += 4)
            {
                edx = BitConverter.ToUInt32(data, i);
                ecx += edx;
                edx ^= ecx;
                Array.Copy(BitConverter.GetBytes(edx), 0, data, i, 4);
            }
            return ecx;
            Array.Copy(BitConverter.GetBytes(ecx), 0, data, stop, 4);
        }

        public void DecXORPass(byte[] data, int offset, int count, uint key)
        {
            int stop = 4 + offset;
            int pos = count + offset - 12;
            uint edx;
            uint ecx = key; // Initial xor key

            while (stop <= pos)
            {
                edx = BitConverter.ToUInt32(data, pos);
                edx ^= ecx;
                ecx -= edx;
                Array.Copy(BitConverter.GetBytes(edx), 0, data, pos, 4);
                pos -= 4;
            }
        }

        public Task EncryptAsync(byte[] data, int offset, int count)
        {
            return Task.Run(() => Encrypt(data, offset, count));
        }

        public Task DecryptAsync(byte[] data, int offset, int count)
        {
            return Task.Run(() => Decrypt(data, offset, count));
        }
    }
}