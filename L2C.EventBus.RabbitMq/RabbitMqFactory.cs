﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace L2C.EventBus.RabbitMq
{
    public class RabbitMqFactory : IEventBusFactory
    {
        private readonly IOptionsMonitor<RabbitMqSettings> _settingsAccessor;
        private readonly ILoggerFactory _loggerFactory;

        public RabbitMqFactory(IOptionsMonitor<RabbitMqSettings> settingsAccessor,
                               ILoggerFactory loggerFactory)
        {
            _settingsAccessor = settingsAccessor;
            _loggerFactory = loggerFactory;
        }

        public IEventBus Create()
        {
            return new RabbitMqEventBus(_settingsAccessor,
                                        _loggerFactory.CreateLogger<RabbitMqEventBus>());
        }
    }
}
