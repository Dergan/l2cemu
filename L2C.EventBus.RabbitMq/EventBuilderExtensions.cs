﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace L2C.EventBus.RabbitMq
{
    public static class EventBuilderExtensions
    {

        public static IEventBusBuilder UsingRabbitMq(this IEventBusBuilder builder, Action<RabbitMqSettings> settings)
        {
            builder.Services.Configure(settings);
            builder.Services.Add(new ServiceDescriptor(typeof(IEventBusFactory), typeof(RabbitMqFactory), ServiceLifetime.Singleton));
            return builder;
        }

        public static IEventBusBuilder UsingRabbitMq(this IEventBusBuilder builder)
        {
            builder.Services.Add(new ServiceDescriptor(typeof(IEventBusFactory), typeof(RabbitMqFactory), ServiceLifetime.Singleton));
            return builder;
        }
    }
}
