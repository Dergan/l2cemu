﻿using System;
using System.Threading.Tasks;
using EasyNetQ;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;

namespace L2C.EventBus.RabbitMq
{
    public class RabbitMqEventBus : IEventBus
    {
        private readonly IOptionsMonitor<RabbitMqSettings> _settingsAccessor;
        private readonly ILogger<RabbitMqEventBus> _logger;
        private IBus _bus;
        private Guid _subscriptionId = Guid.NewGuid();

        private RabbitMqSettings Settings => _settingsAccessor.CurrentValue;

        public RabbitMqEventBus(IOptionsMonitor<RabbitMqSettings> settingsAccessor,
                                ILogger<RabbitMqEventBus> logger)
        {
            if (settingsAccessor == null)
            {
                throw new ArgumentNullException(nameof(settingsAccessor));
            }

            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _settingsAccessor = settingsAccessor;
            _logger = logger;
            _logger.LogInformation("Initializing RabbitMq EventBus");

            _settingsAccessor.OnChange(OptionChangeListener);
            OptionChangeListener(Settings);
        }

        private void OptionChangeListener(RabbitMqSettings settings)
        {
            _logger.LogInformation("Settings of RabbitMq EventBus has changed");
            _bus?.Dispose();
            _bus = RabbitHutch.CreateBus(Settings.ConnectionString, s => { });
            _logger.LogInformation("EventBus created");
        }

        public Task PublishAsync<TEvent>(TEvent message)
            where TEvent : class, IEvent
        {
            return PublishAsync(message as object);
        }

        public Task PublishAsync(object message)
        {
            var evtName = message.GetType().Name;
            _logger.LogDebug($"Publish event: {message.GetType().Name}");

            return Policy.Handle<TimeoutException>()
                .WaitAndRetryAsync(5, i => TimeSpan.FromSeconds(1))
                .ExecuteAsync(() =>
                {
                    return _bus.PublishAsync(message, config => config.WithQueueName(evtName)
                                                                      .WithTopic(evtName));
                });
        }

        public void Subscribe<TEvent>(Action<TEvent> callback)
            where TEvent : class, IEvent
        {
            Subscribe(typeof(TEvent), (x) => callback(x as TEvent));
        }

        public Task SubscribeAsync<TEvent>(Func<TEvent, Task> callbackAsync)
            where TEvent : class, IEvent
        {
            return SubscribeAsync(typeof(TEvent), (x) => callbackAsync(x as TEvent));
        }

        public void Subscribe(Type eventType, Action<object> callback)
        {
            var evtName = eventType.Name;
            _logger.LogDebug($"Subscribing to event: {evtName}");

            Policy.Handle<TimeoutException>()
                .WaitAndRetry(5, i => TimeSpan.FromSeconds(5))
                .Execute(() =>
                {
                    _bus.Subscribe(_subscriptionId.ToString(), callback, config => config.WithQueueName(evtName)
                                                                      .WithTopic(evtName));
                });
        }

        public Task SubscribeAsync(Type eventType, Func<object, Task> callbackAsync)
        {
            var evtName = eventType.Name;
            _logger.LogDebug($"Subscribing to event: {evtName}");

            Policy.Handle<TimeoutException>()
                .WaitAndRetry(5, i => TimeSpan.FromSeconds(5))
                .Execute(() =>
                {
                    _bus.SubscribeAsync(_subscriptionId.ToString(),
                        callbackAsync, config =>
                        config.WithQueueName(evtName).WithTopic(evtName));
                });
            return Task.CompletedTask;
        }
    }
}
