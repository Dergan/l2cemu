﻿using EasyNetQ;
using L2C.MicroService.GenericLib;
using L2C.NetworkingLib.NetProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace L2C.NetworkingLib
{
    public class GenericServer
    {
        internal TcpListener TcpServer { get; private set; }
        internal TcpListener TcpServer6 { get; private set; }
        public List<GenericPeer> Peers { get; private set; }
        public ServerSettings Settings { get; private set; }

        private MessageBroker MsgBroker;
        private Type netProviderType;

        public GenericServer(ServerSettings Settings, MessageBroker messageBroker, Type Provider)
        {
            this.Peers = new List<GenericPeer>();
            this.Settings = Settings;
            this.netProviderType = Provider;

            this.MsgBroker = messageBroker;

            //start the server for IPv4
            this.TcpServer = new TcpListener(IPAddress.Parse(Settings.ListenIP), Settings.ListenPort);
            this.TcpServer.Start();
            this.TcpServer.BeginAcceptTcpClient(AcceptClientCallback, null);

            //if (String.IsNullOrWhiteSpace(Settings.ListenIP))
            //{
            //    //start the server for IPv6
            //    this.TcpServer6 = new TcpListener(IPAddress.Parse(Settings.ListenIp6), Settings.ListenPort);
            //    this.TcpServer6.Start();
            //    this.TcpServer6.BeginAcceptTcpClient(AcceptClient6Callback, null);
            //}
        }

        private void AcceptClientCallback(IAsyncResult result)
        {
            TcpClient client = this.TcpServer.EndAcceptTcpClient(result);

            lock(Peers)
            {
                GenericPeer peer = new GenericPeer(client, netProviderType, MsgBroker);
                Peers.Add(peer);
            }

            this.TcpServer.BeginAcceptTcpClient(AcceptClientCallback, null);
        }

        public GenericPeer GetClientById(Guid ConnectionId)
        {
            lock(Peers)
            {
                return Peers.FirstOrDefault(o => o.ConnectionId == ConnectionId && o.IsConnected);
            }
        }
    }
}
