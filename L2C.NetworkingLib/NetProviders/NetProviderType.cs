﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace L2C.NetworkingLib.NetProviders
{
    public enum NetProviderType
    {
        [EnumMember(Value = "Login")]
        Login,
        [EnumMember(Value = "Game")]
        Game
    }
}
