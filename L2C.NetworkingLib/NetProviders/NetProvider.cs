﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace L2C.NetworkingLib.NetProviders
{
    public abstract class NetProvider
    {
        protected GenericPeer Peer { get; set; }

        public NetProvider()
        {

        }

        public NetProvider(GenericPeer Peer)
        {
            this.Peer = Peer;
        }

        public abstract bool IsConnected { get; }

        public abstract Task<int> ReadAsync(byte[] buffer, int offset, int count);
        
        public abstract Task WriteAsync(byte[] buffer, int offset, int count);

        public abstract void HandlePacket(byte[] GameData, int Length);

        public abstract void onConnect();

        public abstract void Disconnect();
    }
}
