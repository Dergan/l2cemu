﻿using L2C.Cryptography;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace L2C.NetworkingLib.Streams
{
    public class CryptoStream : System.IO.Stream
    {
        private System.IO.Stream _innerStream;
        private readonly ICryptEngine _crypt;

        public CryptoStream(System.IO.Stream stream, ICryptEngine crypt)
        {
            _innerStream = stream;
            _crypt = crypt;
        }

        public void SetEncryptionKey(byte[] key)
        {
            _crypt.SetKey(key);
        }

        public override void Flush()
        {
            _innerStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            byte[] tempBuffer = new byte[count];
            int readed = _innerStream.Read(tempBuffer, 0, count);

            if (readed > 0)
                tempBuffer = _crypt.Decrypt(tempBuffer, 0, readed);

            Array.Copy(tempBuffer, 0, buffer, offset, tempBuffer.Length);
            return readed;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _innerStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _innerStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            var tempBuffer = _crypt.Encrypt(buffer, offset, count);
            _innerStream.Write(tempBuffer, 0, tempBuffer.Length);
        }

        public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            var tempBuffer = _crypt.Encrypt(buffer, offset, count);
            return base.WriteAsync(tempBuffer, 0, tempBuffer.Length, cancellationToken);
        }

        public override bool CanRead => _innerStream.CanRead;
        public override bool CanSeek => _innerStream.CanSeek;
        public override bool CanWrite => _innerStream.CanWrite;
        public override long Length => _innerStream.Length;
        public override long Position
        {
            get => _innerStream.Position;
            set => _innerStream.Position = value;
        }
    }
}
