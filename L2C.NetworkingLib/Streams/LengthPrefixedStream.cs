﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace L2C.NetworkingLib.Streams
{
    public class LengthPrefixedStream : System.IO.Stream
    {
        private System.IO.Stream innerStream;

        public LengthPrefixedStream(System.IO.Stream stream)
        {
            innerStream = stream;
        }

        public override bool CanRead => innerStream.CanRead;

        public override bool CanSeek => innerStream.CanSeek;

        public override bool CanWrite => innerStream.CanWrite;

        public override long Length => innerStream.Length;

        public override long Position { get => innerStream.Position; set => innerStream.Position = value; }

        public override void Flush()
        {
            innerStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int length = ReadLength() - 2;
            if (length > 0 && length < count)
            {
                byte[] buff = new byte[length];
                int readed = 0;
                while (readed < length)
                {
                    readed += innerStream.Read(buff, readed, length - readed);
                }
                Array.Copy(buff, 0, buffer, offset, readed);
                return readed;
            }
            return 0;
        }

        private int ReadLength()
        {
            try
            {
                byte[] buff = new byte[2];
                innerStream.Read(buff, 0, 2);
                return BitConverter.ToUInt16(buff, 0);
            }
            catch
            {
                return 0;
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return innerStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            innerStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            using (MemoryStream s = new MemoryStream())
            {
                s.Write(BitConverter.GetBytes((ushort)(count + 2)), 0, 2);
                s.Write(buffer, offset, count);

                if(innerStream.CanWrite)
                {
                    innerStream.Write(s.ToArray(), 0, (int)s.Length);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            innerStream.Dispose();
            base.Dispose(disposing);
        }
    }
}
