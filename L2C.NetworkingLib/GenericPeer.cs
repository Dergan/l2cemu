﻿using EasyNetQ;
using L2C.MicroService.GenericLib;
using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.NetworkingLib.NetProviders;
using L2C.NetworkingLib.Streams;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace L2C.NetworkingLib
{
    public class GenericPeer
    {
        public TcpClient Client { get; set; }
        public Guid ConnectionId { get; set; }

        private byte[] netBuffer { get; set; }
        private NetProvider netProvider;

        public bool IsConnected { get { return netProvider.IsConnected; } }
        public MessageBroker MsgBroker { get; private set; }

        public GenericPeer(TcpClient Client, Type ProviderType, MessageBroker MsgBroker)
        {
            this.Client = Client;
            this.MsgBroker = MsgBroker;
            this.ConnectionId = Guid.NewGuid();
            this.netProvider = Activator.CreateInstance(ProviderType, this) as NetProvider;

            netProvider.onConnect();
            netProvider_ReadAsync().ConfigureAwait(false);
        }

        private async Task netProvider_ReadAsync()
        {
            this.netBuffer = new byte[2480];
            int readed = await netProvider.ReadAsync(netBuffer, 0, netBuffer.Length).ConfigureAwait(false);
            if (readed > 0)
            {
                netProvider.HandlePacket(netBuffer, readed);
                await netProvider_ReadAsync();
            }
            else
            {
                Disconnect();
            }
        }

        public Task SendGameMessageAsync(byte[] GameMessageData)
        {
            return netProvider.WriteAsync(GameMessageData, 0, GameMessageData.Length);
        }

        public Task SendGameMessageAsync(GameMessageBase GameMessage)
        {
            byte[] Serialized = GameMessage.Serialize(GameMessage);
            return netProvider.WriteAsync(Serialized, 0, Serialized.Length);
        }

        public void Disconnect()
        {
            netProvider.Disconnect();
        }
    }
}
