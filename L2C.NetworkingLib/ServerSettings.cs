﻿using L2C.NetworkingLib.NetProviders;

namespace L2C.NetworkingLib
{
    public class ServerSettings
    {
        public string ListenIP { get; set; }
        public short ListenPort { get; set; }
        public NetProviderType NetProvider { get; set; }
    }
}
