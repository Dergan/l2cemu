﻿namespace L2C.GameServer.Events
{
    public class RequestLobbyEvent : L2C.Shared.Events.SessionBasedEvent
    {

        protected RequestLobbyEvent()
        {
        }

        public RequestLobbyEvent(string sessionId) : base(sessionId)
        {
        }
    }
}
