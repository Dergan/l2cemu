﻿namespace L2C.GameServer.Events
{
    public class EnterWorldEvent : L2C.Shared.Events.ObjectIdBasedEvent
    {

        public EnterWorldEvent(uint objectId) : base(objectId)
        {
        }

        protected EnterWorldEvent()
        {
        }
    }
}
