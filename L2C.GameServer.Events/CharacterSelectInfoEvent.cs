﻿using System.Collections.Generic;
using System.Drawing;

namespace L2C.GameServer.Events
{
    public class CharacterSelectInfoEvent : L2C.Shared.Events.SessionBasedEvent
    {
        public uint MaxCharacters { get; set; }
        public List<Character> Characters { get; set; }
    }

    public class Character
    {
        public string Name { get; set; }
        public Point Position { get; set; }
    }
}
