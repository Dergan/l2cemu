﻿namespace L2C.GameServer.Events
{
    public class LogoutEvent : L2C.Shared.Events.SessionBasedEvent
    {

        public LogoutEvent(string sessionId) : base(sessionId)
        {
        }

        protected LogoutEvent()
        {
        }
    }
}
