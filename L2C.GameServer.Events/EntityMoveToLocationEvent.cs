﻿using L2C.Shared.Core;

namespace L2C.GameServer.Events
{
    public class EntityMoveToLocationEvent : L2C.Shared.Events.ObjectIdBasedEvent
    {
        public Vector3 Source { get; set; }
        public Vector3 Destination { get; set; }

        protected EntityMoveToLocationEvent()
        {
        }

        public EntityMoveToLocationEvent(uint objectId) : base(objectId)
        {
        }
    }
}
