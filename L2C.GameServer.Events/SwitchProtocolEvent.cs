﻿namespace L2C.GameServer.Events
{
    public class SwitchProtocolEvent : L2C.Shared.Events.SessionBasedEvent
    {
        public uint Version { get; set; }
    }
}
