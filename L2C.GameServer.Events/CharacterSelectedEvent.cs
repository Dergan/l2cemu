﻿namespace L2C.GameServer.Events
{
    public class CharacterSelectedEvent : L2C.Shared.Events.SessionBasedEvent
    {
        public uint ObjectId { get; set; }
        public string Name { get; set; }
        public uint ClassId { get; set; }
        public double CurrentHP { get; set; }
        public double CurrentMP { get; set; }
        public long EXP { get; set; }
        public uint Gender { get; set; }
        public uint Karma { get; set; }
        public uint Level { get; set; }
        public uint PK { get; set; }
        public uint PledgeId { get; set; }
        public uint RaceId { get; set; }
        public long SP { get; set; }
        public string Title { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        protected CharacterSelectedEvent()
        {
        }

        public CharacterSelectedEvent(string sessionId) : base(sessionId)
        {
        }
    }
}
