﻿namespace L2C.GameServer.Events
{
    public class PlayerInfoChangedEvent : L2C.Shared.Events.ObjectIdBasedEvent
    {

        public PlayerInfoChangedEvent(uint objectId) : base(objectId)
        {
        }

        protected PlayerInfoChangedEvent()
        {
        }
    }
}
