﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib.QueueMessages
{
    public class QM_NewGameClient : MQConnectionBase
    {
        public Guid ConnectionId { get; set; }
        public byte[] XorGameKey { get; set; }
    }
}
