﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib.QueueMessages
{
    public class QM_NewLoginClient : MQConnectionBase
    {
        public Guid ConnectionId { get; set; }
        public byte[] ScrambledRsaModulus { get; set; }
        public string PrivateRsaPEM { get; set; }
        public byte[] BlowfishKey { get; set; }
    }
}
