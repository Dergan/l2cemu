﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib.QueueMessages
{
    public class QM_GameData : MQConnectionBase
    {
        public Guid ConnectionId { get; set; }
        public byte[] Data { get; set; } //the actual traffic from the game excluding the 2byte header
        public QM_GameDataType DataType { get; set; }
    }
}
