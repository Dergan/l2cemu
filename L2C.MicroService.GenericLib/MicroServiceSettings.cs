﻿using System;

namespace L2C.MicroService.GenericLib
{
    public class MicroServiceSettings
    {
        public MessageBrokerSettings MessageBroker { get; set; }
        public MicroServiceSqlSettings SqlSettings { get; set; }
    }
}
