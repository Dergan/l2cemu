﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib
{
    public enum QueueMessageServiceType
    {
        //login
        LoginServer,
        LoginMessageHandler,
        AuthenticationService,

        //game
        GameServer,
        GameMessageHandler,
        BasicPlayerService,
    }
}
