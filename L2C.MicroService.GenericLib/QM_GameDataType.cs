﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace L2C.MicroService.GenericLib
{
    public enum QM_GameDataType
    {
        [EnumMember(Value = "FromGame")]
        FromGame,
        [EnumMember(Value = "ToGame")]
        ToGame
    }
}
