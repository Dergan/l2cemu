﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib
{
    public class MessageBrokerSettings
    {
        public string Hostname { get; set; }
    }
}
