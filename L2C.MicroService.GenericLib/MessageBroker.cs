﻿using System;
using System.Threading.Tasks;
using EasyNetQ;
using EasyNetQ.FluentConfiguration;
using L2C.MicroService.GenericLib.GameMessageParser;
using Microsoft.Extensions.Options;

namespace L2C.MicroService.GenericLib
{
    public class MessageBroker : IDisposable
    {
        private IBus _msgBroker;
        private readonly MessageBrokerSettings _settings;

        public MessageBroker(IOptions<MessageBrokerSettings> settings) : this(settings.Value)
        {
        }

        public MessageBroker(MessageBrokerSettings settings)
        {
            _settings = settings;
            _msgBroker = MessageBrokerHelper.CreateMessageBroker(_settings.Hostname);
        }

        public ISubscriptionResult SubscribeAsync<T>(string subscriptionId, Func<T, Task> onMessage, Action<ISubscriptionConfiguration> configure) where T : class
        {
            return _msgBroker.SubscribeAsync<T>(subscriptionId, onMessage, configure);
        }

        public ISubscriptionResult SubscribeAsync<T>(QueueMessageServiceType ServiceType, Guid ConnectionId, Func<T, Task> onMessage) where T : class
        {
            return _msgBroker.SubscribeAsync<T>(String.Empty, onMessage, (configure) =>
            {
                configure.WithQueueName($"{ServiceType.ToString()}_{typeof(T).Name}_{ConnectionId}");
            });
        }

        public ISubscriptionResult SubscribeAsync<T>(QueueMessageServiceType ServiceType, Guid ConnectionId, Func<T, Task> onMessage, Action<ISubscriptionConfiguration> configure) where T : class
        {
            return _msgBroker.SubscribeAsync<T>(String.Empty, onMessage, (config) =>
            {
                config.WithQueueName($"{ServiceType.ToString()}_{ConnectionId}");
                configure(config);
            });
        }

        public Task SendMessage(object Message, QueueMessageServiceType TargetService)
        {
            _msgBroker.Send(TargetService.ToString(), Message);
            return Task.CompletedTask;

            //return msgBroker.SendAsync(TargetService.ToString(), Message);
        }

        public Task SendMessage(object Message)
        {
            return _msgBroker.SendAsync(Message.GetType().Name, Message);
        }

        public Task SendMessage(object Message, QueueMessageServiceType TargetService, Guid ConnectionId)
        {
            _msgBroker.Send($"{TargetService.ToString()}_{Message.GetType().Name}_{ConnectionId}", Message);
            return Task.CompletedTask;
            //return msgBroker.SendAsync($"{TargetService.ToString()}_{ConnectionId}", Message);
        }

        public Task SendGameMessage(GameMessageBase GameMessage, MessageServerType TargetService, Guid ConnectionId)
        {
            _msgBroker.Send($"{TargetService.ToString()}_{typeof(GameMessageBase).Name}_{ConnectionId}", GameMessage);
            return Task.CompletedTask;
            //return msgBroker.SendAsync($"{TargetService.ToString()}_{ConnectionId}", Message);
        }

        public void Dispose()
        {
            _msgBroker?.Dispose();
        }
    }
}
