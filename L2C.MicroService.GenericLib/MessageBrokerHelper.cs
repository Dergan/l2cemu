﻿using EasyNetQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib
{
    public class MessageBrokerHelper
    {
        public static IBus CreateMessageBroker(MessageBrokerSettings Settings)
        {
            return CreateMessageBroker(Settings.Hostname);
        }
        public static IBus CreateMessageBroker(string RabbitMQHost)
        {
            return RabbitHutch.CreateBus($"host={RabbitMQHost}");
        }
    }
}
