﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace L2C.MicroService.GenericLib
{
    public class ConfigurationHandler
    {
        public static T LoadConfig<T>(string SettingsFile, string SectionNamePath)
        {
            IConfiguration Configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                                                                     .AddJsonFile("AppSettings.json", false, true)
                                                                     .Build();
            
            var section = Configuration.GetSection(SectionNamePath);
            return section.Get<T>();
        }
    }
}