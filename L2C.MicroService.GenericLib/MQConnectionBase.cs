﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib
{
    public interface MQConnectionBase
    {
        Guid ConnectionId { get; set; }
    }
}
