﻿using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace L2C.MicroService.GenericLib.GameMessageParser
{
    public abstract class GameMessageBase
    {
        public abstract void onReceive();
        public abstract void onSend();
        public abstract void onDeserialize();
        public abstract void onSerialize();

        public GameMessageBase()
        {

        }

        public byte[] Serialize(GameMessageBase GameMessage, bool IgnorePacketId = false)
        {
            GameMessage.onSerialize();

            using (MemoryStream ms = new MemoryStream())
            {
                if (!IgnorePacketId && GameMessage.GetType().GetCustomAttributes(typeof(NoPacketIdAttribute), false).Length == 0)
                {
                    //first write the Packet Id
                    bool FoundPacketId = false;
                    foreach (PropertyInfo inf in GameMessage.GetType().GetProperties())
                    {
                        object[] temp = null;

                        if ((temp = inf.GetCustomAttributes(typeof(PacketIdAttribute), false)).Length > 0)
                        {
                            FoundPacketId = true;

                            PacketIdAttribute byteAttr = temp[0] as PacketIdAttribute;
                            if (byteAttr.SingleByte)
                            {
                                ms.WriteByte(byteAttr.PacketId);
                            }
                            else
                            {
                                ms.Write(byteAttr.PacketArrId);
                            }
                            break;
                        }
                    }
                    if (!FoundPacketId)
                    {
                        throw new Exception("No packet id was found in this packed");
                    }
                }

                foreach (PropertyInfo inf in GameMessage.GetType().GetProperties())
                {
                    object[] temp = null;

                    if ((temp = inf.GetCustomAttributes(typeof(ByteAttribute), false)).Length > 0)
                    {
                        ByteAttribute byteAttr = temp[0] as ByteAttribute;

                        if (byteAttr.UseConstant)
                        {
                            ms.WriteByte(byteAttr.ConstValue);
                        }
                        else
                        {
                            ms.WriteByte((byte)inf.GetValue(GameMessage, null));
                        }
                    }
                    if ((temp = inf.GetCustomAttributes(typeof(BoolAttribute), false)).Length > 0)
                    {
                        BoolAttribute byteAttr = temp[0] as BoolAttribute;

                        if (byteAttr.UseConstant)
                        {
                            ms.WriteByte((byte)(byteAttr.ConstValue ? 1 : 0));
                        }
                        else
                        {
                            ms.WriteByte((byte)((bool)inf.GetValue(GameMessage, null) ? 1 : 0));
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(ByteArrayAttribute), false)).Length > 0)
                    {
                        ByteArrayAttribute byteArrAttr = temp[0] as ByteArrayAttribute;
                        if (byteArrAttr.UseConstant)
                        {
                            if (byteArrAttr.SizeCheck >= 0 && byteArrAttr.ConstValue == null)
                                throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, Value=NULL");
                            if (byteArrAttr.SizeCheck >= 0 && byteArrAttr.ConstValue.Length != byteArrAttr.SizeCheck)
                                throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, ValueLength=" + byteArrAttr.ConstValue.Length + ", SizeCheck=" + byteArrAttr.SizeCheck);


                            ms.Write(byteArrAttr.ConstValue);
                        }
                        else
                        {
                            if (byteArrAttr.SizeCheck >= 0 && inf.GetValue(GameMessage, null) == null)
                                throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, Value=NULL");

                            byte[] byteArr = (byte[])inf.GetValue(GameMessage, null);
                            if (byteArrAttr.SizeCheck >= 0 && byteArr.Length != byteArrAttr.SizeCheck)
                                throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, ValueLength=" + byteArrAttr.ConstValue.Length + ", SizeCheck=" + byteArrAttr.SizeCheck);

                            ms.Write(byteArr);
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(ShortAttribute), false)).Length > 0)
                    {
                        ShortAttribute intAttr = temp[0] as ShortAttribute;
                        if (intAttr.UseConstant)
                        {
                            ms.Write(BitConverter.GetBytes(intAttr.ConstValue));
                        }
                        else
                        {
                            ms.Write(BitConverter.GetBytes((short)inf.GetValue(GameMessage, null)));
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(UShortAttribute), false)).Length > 0)
                    {
                        UShortAttribute intAttr = temp[0] as UShortAttribute;
                        if (intAttr.UseConstant)
                        {
                            ms.Write(BitConverter.GetBytes(intAttr.ConstValue));
                        }
                        else
                        {
                            ms.Write(BitConverter.GetBytes((ushort)inf.GetValue(GameMessage, null)));
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(IntegerAttribute), false)).Length > 0)
                    {
                        IntegerAttribute intAttr = temp[0] as IntegerAttribute;
                        if (intAttr.UseConstant)
                        {
                            ms.Write(BitConverter.GetBytes(intAttr.ConstValue));
                        }
                        else
                        {
                            ms.Write(BitConverter.GetBytes((int)inf.GetValue(GameMessage, null)));
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(UIntegerAttribute), false)).Length > 0)
                    {
                        UIntegerAttribute intAttr = temp[0] as UIntegerAttribute;
                        if (intAttr.UseConstant)
                        {
                            ms.Write(BitConverter.GetBytes(intAttr.ConstValue));
                        }
                        else
                        {
                            ms.Write(BitConverter.GetBytes((uint)inf.GetValue(GameMessage, null)));
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(LongAttribute), false)).Length > 0)
                    {
                        LongAttribute longAttr = temp[0] as LongAttribute;
                        if (longAttr.UseConstant)
                        {
                            ms.Write(BitConverter.GetBytes(longAttr.ConstValue));
                        }
                        else
                        {
                            ms.Write(BitConverter.GetBytes((long)inf.GetValue(GameMessage, null)));
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(DoubleAttribute), false)).Length > 0)
                    {
                        DoubleAttribute longAttr = temp[0] as DoubleAttribute;
                        if (longAttr.UseConstant)
                        {
                            ms.Write(BitConverter.GetBytes(longAttr.ConstValue));
                        }
                        else
                        {
                            ms.Write(BitConverter.GetBytes((double)inf.GetValue(GameMessage, null)));
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(StringAttribute), false)).Length > 0)
                    {
                        StringAttribute strAttr = temp[0] as StringAttribute;
                        if (strAttr.UseConstant)
                        {
                            ms.Write(System.Text.Encoding.Unicode.GetBytes(strAttr.ConstValue));
                            //pw.WriteString(strAttr.ConstValue);
                        }
                        else
                        {
                            ms.Write(System.Text.Encoding.Unicode.GetBytes((string)inf.GetValue(GameMessage, null)));
                        }
                        ms.WriteByte(0); //nul-terminator
                        ms.WriteByte(0); //nul-terminator
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(ListAttribute), false)).Length > 0)
                    {
                        ListAttribute listAttr = temp[0] as ListAttribute;

                        ICollection collection = (ICollection)inf.GetValue(GameMessage, null);
                        IEnumerator Enumerator = collection.GetEnumerator();

                        while (Enumerator.MoveNext())
                        {
                            GameMessageBase b = (GameMessageBase)Enumerator.Current;
                            ms.Write(b.Serialize(b, true));
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(PacketObjectAttribute), false)).Length > 0)
                    {
                        PacketObjectAttribute objAttr = temp[0] as PacketObjectAttribute;
                        GameMessageBase obj = (GameMessageBase)inf.GetValue(GameMessage, null);
                        ms.Write(obj.Serialize(obj, true));
                    }
                }

                return ms.ToArray();
            }

        }

        public void Deserialize(byte[] Data, GameMessageBase GameMessage)
        {
            using (MemoryStream ms = new MemoryStream(Data))
            {
                foreach (PropertyInfo inf in GameMessage.GetType().GetProperties())
                {
                    object[] temp = null;

                    if ((temp = inf.GetCustomAttributes(typeof(PacketIdAttribute), false)).Length > 0)
                    {
                        PacketIdAttribute byteAttr = temp[0] as PacketIdAttribute;

                        if (byteAttr.SingleByte)
                        {
                            inf.SetValue(GameMessage, byteAttr.PacketId, null);
                            ms.Position++;
                        }
                        else
                        {
                            inf.SetValue(GameMessage, byteAttr.PacketArrId, null);
                            ms.Position += byteAttr.PacketArrId.Length;
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(ByteAttribute), false)).Length > 0)
                    {
                        ByteAttribute byteAttr = temp[0] as ByteAttribute;

                        if (byteAttr.UseConstant)
                        {
                            inf.SetValue(GameMessage, byteAttr.ConstValue, null);
                            ms.Position++;
                        }
                        else
                        {
                            inf.SetValue(GameMessage, (byte)ms.ReadByte(), null);
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(ByteArrayAttribute), false)).Length > 0)
                    {
                        ByteArrayAttribute byteArrAttr = temp[0] as ByteArrayAttribute;
                        if (byteArrAttr.UseConstant)
                        {
                            if (byteArrAttr.SizeCheck >= 0 && byteArrAttr.ConstValue == null)
                                throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, Value=NULL");
                            if (byteArrAttr.SizeCheck >= 0 && byteArrAttr.ConstValue.Length != byteArrAttr.SizeCheck)
                                throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, ValueLength=" + byteArrAttr.ConstValue.Length + ", SizeCheck=" + byteArrAttr.SizeCheck);

                            inf.SetValue(GameMessage, byteArrAttr.ConstValue, null);
                            ms.Position += byteArrAttr.ConstValue.Length;
                        }
                        else
                        {
                            byte[] TempBuffer = new byte[0];
                            if (byteArrAttr.UnknownSize)
                            {
                                TempBuffer = new byte[ms.Length - ms.Position];
                                ms.Read(TempBuffer, 0, TempBuffer.Length);
                                inf.SetValue(GameMessage, TempBuffer, null);
                                break;
                            }

                            if (byteArrAttr.SizeCheck <= 0)
                            {
                                throw new Exception("Byte Array Length must be >=0");
                            }

                            TempBuffer = new byte[byteArrAttr.SizeCheck];
                            ms.Read(TempBuffer, 0, TempBuffer.Length);
                            inf.SetValue(GameMessage, TempBuffer, null);
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(ShortAttribute), false)).Length > 0)
                    {
                        ShortAttribute intAttr = temp[0] as ShortAttribute;
                        if (intAttr.UseConstant)
                        {
                            inf.SetValue(GameMessage, intAttr.ConstValue, null);
                            ms.Position += 2;
                        }
                        else
                        {
                            byte[] TempBuffer = new byte[2];
                            ms.Read(TempBuffer, 0, TempBuffer.Length);

                            inf.SetValue(GameMessage, BitConverter.ToInt16(TempBuffer), null);
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(UShortAttribute), false)).Length > 0)
                    {
                        UShortAttribute intAttr = temp[0] as UShortAttribute;
                        if (intAttr.UseConstant)
                        {
                            inf.SetValue(GameMessage, intAttr.ConstValue, null);
                            ms.Position += 2;
                        }
                        else
                        {
                            byte[] TempBuffer = new byte[2];
                            ms.Read(TempBuffer, 0, TempBuffer.Length);
                            inf.SetValue(GameMessage, BitConverter.ToUInt16(TempBuffer), null);
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(IntegerAttribute), false)).Length > 0)
                    {
                        IntegerAttribute intAttr = temp[0] as IntegerAttribute;
                        if (intAttr.UseConstant)
                        {
                            inf.SetValue(GameMessage, intAttr.ConstValue, null);
                            ms.Position += 4;
                        }
                        else
                        {
                            byte[] TempBuffer = new byte[4];
                            ms.Read(TempBuffer, 0, TempBuffer.Length);
                            inf.SetValue(GameMessage, BitConverter.ToInt32(TempBuffer), null);
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(UIntegerAttribute), false)).Length > 0)
                    {
                        UIntegerAttribute intAttr = temp[0] as UIntegerAttribute;
                        if (intAttr.UseConstant)
                        {
                            inf.SetValue(GameMessage, intAttr.ConstValue, null);
                            ms.Position += 4;
                        }
                        else
                        {
                            byte[] TempBuffer = new byte[4];
                            ms.Read(TempBuffer, 0, TempBuffer.Length);
                            inf.SetValue(GameMessage, BitConverter.ToUInt32(TempBuffer), null);
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(LongAttribute), false)).Length > 0)
                    {
                        LongAttribute longAttr = temp[0] as LongAttribute;
                        if (longAttr.UseConstant)
                        {
                            inf.SetValue(GameMessage, longAttr.ConstValue, null);
                            ms.Position += 8;
                        }
                        else
                        {
                            byte[] TempBuffer = new byte[8];
                            ms.Read(TempBuffer, 0, TempBuffer.Length);
                            inf.SetValue(GameMessage, BitConverter.ToInt64(TempBuffer), null);
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(DoubleAttribute), false)).Length > 0)
                    {
                        DoubleAttribute doubleAttr = temp[0] as DoubleAttribute;
                        if (doubleAttr.UseConstant)
                        {
                            inf.SetValue(GameMessage, doubleAttr.ConstValue, null);
                            ms.Position += 8;
                        }
                        else
                        {
                            byte[] TempBuffer = new byte[8];
                            ms.Read(TempBuffer, 0, TempBuffer.Length);
                            inf.SetValue(GameMessage, BitConverter.ToDouble(TempBuffer), null);
                        }
                    }
                    else if ((temp = inf.GetCustomAttributes(typeof(StringAttribute), false)).Length > 0)
                    {
                        StringAttribute strAttr = temp[0] as StringAttribute;
                        if (strAttr.UseConstant)
                        {
                            inf.SetValue(GameMessage, strAttr.ConstValue, null);
                        }
                        else
                        {
                            string result = System.Text.Encoding.Unicode.GetString(Data, (int)ms.Position, (int)(Data.Length - ms.Position));
                            int idx = result.IndexOf((char)0x00);
                            if (!(idx == -1))
                            {
                                result = result.Substring(0, idx);
                            }
                            ms.Position += (result.Length * 2) + 2;
                            inf.SetValue(GameMessage, result);

                            //

                            //inf.SetValue(GameMessage, pr.ReadString(), null);
                        }
                    }
                }
            }

            GameMessage.onDeserialize();
        }

        public int GetPacketId(GameMessageBase GameMessage)
        {
            foreach (PropertyInfo inf in GameMessage.GetType().GetProperties())
            {
                object[] temp = null;

                if ((temp = inf.GetCustomAttributes(typeof(PacketIdAttribute), false)).Length > 0)
                {
                    PacketIdAttribute byteAttr = temp[0] as PacketIdAttribute;
                    if (byteAttr.SingleByte)
                    {
                        return byteAttr.PacketId;
                    }
                    else
                    {
                        if (byteAttr.PacketArrId.Length == 1)
                            return byteAttr.PacketArrId[0];
                        else if (byteAttr.PacketArrId.Length == 2)
                            return BitConverter.ToUInt16(byteAttr.PacketArrId, 0);
                        throw new Exception("No support for Byte Arrays with a length of " + byteAttr.PacketArrId.Length);
                    }
                }
            }
            throw new Exception("No packet id was found in this packed");
        }
    }
}
