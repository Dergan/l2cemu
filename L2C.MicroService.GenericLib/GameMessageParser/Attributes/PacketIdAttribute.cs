﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib.GameMessageParser.Attributes
{
    public class PacketIdAttribute : Attribute
    {
        public bool SingleByte { get; private set; }
        public byte PacketId { get; private set; }
        public byte[] PacketArrId { get; private set; }

        public PacketIdAttribute(byte PacketId)
        {
            this.PacketId = PacketId;
            this.SingleByte = true;
        }

        public PacketIdAttribute(byte[] PacketId)
        {
            this.PacketArrId = PacketId;
        }
    }
}
