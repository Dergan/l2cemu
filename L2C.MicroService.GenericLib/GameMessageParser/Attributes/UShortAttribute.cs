﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib.GameMessageParser.Attributes
{
    public class UShortAttribute : Attribute
    {
        public bool UseConstant { get; private set; }
        public ushort ConstValue { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConstValue">The constant value used for the byte array if constant</param>
        public UShortAttribute(ushort ConstValue)
        {
            this.ConstValue = ConstValue;
            this.UseConstant = true;
        }

        public UShortAttribute()
        {

        }
    }
}
