﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib.GameMessageParser.Attributes
{
    public class IntegerAttribute : Attribute
    {
        public bool UseConstant { get; private set; }
        public int ConstValue { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConstValue">The constant value used for the byte array if constant</param>
        public IntegerAttribute(int ConstValue)
        {
            this.ConstValue = ConstValue;
            this.UseConstant = true;
        }

        public IntegerAttribute()
        {

        }
    }
}
