﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib.GameMessageParser.Attributes
{
    public class DoubleAttribute : Attribute
    {
        public bool UseConstant { get; private set; }
        public double ConstValue { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConstValue">The constant value used for the byte array if constant</param>
        public DoubleAttribute(double ConstValue)
        {
            this.ConstValue = ConstValue;
            this.UseConstant = true;
        }

        public DoubleAttribute()
        {

        }
    }
}
