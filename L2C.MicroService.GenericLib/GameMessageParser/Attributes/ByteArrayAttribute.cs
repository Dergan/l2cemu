﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib.GameMessageParser.Attributes
{
    public class ByteArrayAttribute : Attribute
    {
        public byte[] ConstValue { get; private set; }
        public bool UseConstant { get; private set; }
        public int SizeCheck { get; private set; }
        public bool UnknownSize { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConstValue">The constant value used for the byte array if constant</param>
        public ByteArrayAttribute(byte[] ConstValue = null)
        {
            this.ConstValue = ConstValue;
            this.UseConstant = true;
            this.SizeCheck = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConstValue">The constant value used for the byte array if constant</param>
        /// <param name="SizeCheck">Check the size of the array if it's exactly this size</param>
        public ByteArrayAttribute(byte[] ConstValue = null, int SizeCheck = -1)
        {
            this.ConstValue = ConstValue;
            this.UseConstant = true;
            this.SizeCheck = SizeCheck;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConstValue">The constant value used for the byte array if constant</param>
        /// <param name="SizeCheck">Check the size of the array if it's exactly this size</param>
        public ByteArrayAttribute(int SizeCheck = -1)
        {
            this.SizeCheck = SizeCheck;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UnknownSize">Get all the bytes from the packet that is left</param>
        public ByteArrayAttribute(bool UnknownSize)
        {
            this.UnknownSize = UnknownSize;
        }

        public ByteArrayAttribute()
        {
            this.SizeCheck = -1;
        }
    }
}
