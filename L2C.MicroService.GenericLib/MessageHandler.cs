﻿using L2C.MicroService.GenericLib.GameMessageParser;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib
{
    public class MessageHandler
    {
        private static SortedList<int, Type> messages;

        static MessageHandler()
        {
            messages = new SortedList<int, Type>();
        }

        public static void AddMessage(Type GameMessageType)
        {
            GameMessageBase gameMessage = Activator.CreateInstance(GameMessageType) as GameMessageBase;
            int MsgId = gameMessage.GetPacketId(gameMessage);
            messages.Add(MsgId, GameMessageType);
        }

        public static GameMessageBase DeserializeMessage(byte[] GameData)
        {
            Type GameMessageType = null;

            if (GameData?.Length <= 0 ||
                (!messages.TryGetValue(GameData[0], out GameMessageType) &&
                !messages.TryGetValue(BitConverter.ToUInt16(GameData, 0), out GameMessageType)))
            {
                return null;
            }
            
            GameMessageBase gameMessage = Activator.CreateInstance(GameMessageType) as GameMessageBase;
            gameMessage.Deserialize(GameData, gameMessage);
            return gameMessage;
        }
    }
}
