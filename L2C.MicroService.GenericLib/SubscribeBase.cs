﻿using EasyNetQ;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace L2C.MicroService.GenericLib
{
    public abstract class SubscribeBase<Message>
    {
        protected MessageBroker MsgBroker { get; private set; }

        public SubscribeBase(MessageBroker MsgBroker)
        {
            this.MsgBroker = MsgBroker;
        }

        public abstract Task HandleMessage(Message Message);
    }
}
