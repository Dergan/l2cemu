﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GenericLib
{
    public class MicroServiceSqlSettings
    {
        public string Hostname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string DatabaseName { get; set; }
    }
}
