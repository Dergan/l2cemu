﻿using System;
using L2C.EventBus;
using L2C.EventBus.RabbitMq;
using L2C.PlayerService.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace L2C.PlayerService
{
    class Program
    {
        public static IConfigurationRoot Configuration = new ConfigurationBuilder()
                                        .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                        .AddJsonFile("AppSettings.json", optional: false)
                                        .Build();

        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            var serviceProvider = services.BuildServiceProvider();

            var app = serviceProvider.GetRequiredService<App>();
            app.Run();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(builder => builder.SetMinimumLevel(LogLevel.Debug).AddConsole());
            services.AddEventBus(builder => builder.UsingRabbitMq());

            services.AddDbContext<PlayerServiceContext>(options
                => options.UseSqlServer(Configuration.GetSection("Database:Mssql").GetValue<string>("ConnectionString")));

            services.Configure<RabbitMqSettings>(Configuration.GetSection("EventBus:RabbitMq"));

            services.Scan(scan => scan
                .FromAssemblyOf<Program>()
                    .AddClasses(classes => classes.AssignableTo(typeof(IConsumer<>)))
                        .AsImplementedInterfaces()
                        .WithTransientLifetime());

            services.AddSingleton<App>();
        }
    }
}
