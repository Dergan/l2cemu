﻿using System.Collections.Generic;
using System.Diagnostics;
using L2C.EventBus;
using L2C.PlayerService.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace L2C.PlayerService
{
    internal class App
    {
        private readonly ILogger<App> _logger;
        private readonly IEnumerable<IConsumer> _consumers;
        private readonly IEventBus _messageBroker;

        public App(ILogger<App> logger,
            IEnumerable<IConsumer> consumers,
            IEventBus messageBroker,
            PlayerServiceContext context)
        {
            _logger = logger;
            _consumers = consumers;
            _messageBroker = messageBroker;
            _logger.LogInformation("Starting Player Service");

            _logger.LogInformation("Checking migration needed");
            if (!context.Database.EnsureCreated())
            {
                _logger.LogInformation("Migrating database to latest version");
                context.Database.Migrate();
            }
        }

        public void Run()
        {
            foreach (var handler in _consumers)
                _messageBroker.SubscribeAsync(handler.EventType, handler.HandleAsync);

            _logger.LogInformation("Player service started, waiting for events");
            Process.GetCurrentProcess().WaitForExit();
        }
    }
}
