﻿using System.Threading.Tasks;
using L2C.EventBus;
using L2C.GameServer.Events;

namespace L2C.PlayerService.EventHandlers
{
    internal class EnterWorldEventHandler : BaseEventHandler<EnterWorldEvent>
    {
        private readonly IEventBus _eventBus;

        public EnterWorldEventHandler(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public override Task HandleAsync(EnterWorldEvent message)
        {


            return _eventBus.PublishAsync(new PlayerInfoChangedEvent(message.ObjectId)
            {

            });
        }
    }
}
