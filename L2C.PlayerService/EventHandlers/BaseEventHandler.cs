﻿using System;
using System.Threading.Tasks;
using L2C.EventBus;

namespace L2C.PlayerService.EventHandlers
{
    abstract class BaseEventHandler<TEvent> : IConsumer<TEvent>
        where TEvent : class, IEvent
    {

        public Type EventType => typeof(TEvent);
        public Task HandleAsync(object message) => HandleAsync(message as TEvent);
        public abstract Task HandleAsync(TEvent message);
    }
}
