﻿using System;
using System.Linq;
using System.Reflection;
using L2C.PlayerService.Core;
using Microsoft.EntityFrameworkCore;

namespace L2C.PlayerService.Infrastructure.Data
{

    class PlayerServiceContext : DbContext
    {

        public DbSet<Player> Players { get; set; }


        public PlayerServiceContext(DbContextOptions<PlayerServiceContext> options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=L2C.PlayerService.Data;Trusted_Connection=True;ConnectRetryCount=0");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>(builder =>
            {
                builder.HasKey(x => x.ObjectId);
                builder.Property(x => x.Name);
                builder.Property(x => x.Level);
                builder.Property(x => x.Gender);
                builder.Property(x => x.Race);
                builder.Property(x => x.Class);
                builder.Property(x => x.VisibleClass);
                builder.Property(x => x.BuilderLevel);
            });

            ApplyAllConfigurations(modelBuilder);
        }

        private static void ApplyAllConfigurations(ModelBuilder builder)
        {
            var mappingInterfaces = new[]
            {
                typeof(IEntityTypeConfiguration<>),
                typeof(IQueryTypeConfiguration<>)
            };

            var mappingTypes = typeof(PlayerServiceContext).GetTypeInfo()
                .Assembly
                .GetTypes()
                .Where(x => x.GetInterfaces()
                .Any(y => y.GetTypeInfo()
                .IsGenericType && mappingInterfaces.Any(z => y.GetGenericTypeDefinition() == z)));

            foreach (Type mappingType in mappingTypes)
            {
                dynamic instance = Activator.CreateInstance(mappingType);
                builder.ApplyConfiguration(instance);
            }
        }
    }
}
