﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using L2C.Shared.Core.Data;

namespace L2C.PlayerService.Infrastructure.Data
{
    class GenericEFCoreRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class
    {
        private readonly Func<PlayerServiceContext> _readContextFactory;
        private readonly PlayerServiceContext _writeContext;

        public GenericEFCoreRepository(Func<PlayerServiceContext> readContextFactory)
        {
            _readContextFactory = readContextFactory;
            _writeContext = readContextFactory();
        }

        public IQueryable<TEntity> Query() => _readContextFactory().Query<TEntity>();

        public Task Create(TEntity entity) => _writeContext.Set<TEntity>().AddAsync(entity);

        public Task Delete(TEntity entity)
        {
            _writeContext.Set<TEntity>().Remove(entity);
            return Task.CompletedTask;
        }


        public Task Update(TEntity entity)
        {
            _writeContext.Set<TEntity>().Update(entity);
            return Task.CompletedTask;
        }

        public IEnumerator<TEntity> GetEnumerator() => GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => Query().GetEnumerator();

        public Type ElementType => Query().ElementType;

        public Expression Expression => Query().Expression;

        public IQueryProvider Provider => Query().Provider;

        public void Dispose()
        {
            _writeContext?.SaveChanges();
            _writeContext?.Dispose();
        }
    }
}
