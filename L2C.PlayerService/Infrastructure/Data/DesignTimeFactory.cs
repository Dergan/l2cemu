﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace L2C.PlayerService.Infrastructure.Data
{
    class DesignTimeFactory : IDesignTimeDbContextFactory<PlayerServiceContext>
    {

        public PlayerServiceContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PlayerServiceContext>()
                                .UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=L2C.PlayerService.Data;Trusted_Connection=True;ConnectRetryCount=0");
            return new PlayerServiceContext(builder.Options);
        }
    }
}
