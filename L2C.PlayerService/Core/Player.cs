﻿namespace L2C.PlayerService.Core
{
    class Player
    {
        public uint ObjectId { get; internal set; }
        public string Name { get; private set; }
        public byte BuilderLevel { get; private set; }
        public byte Race { get; private set; }
        public byte Gender { get; private set; }
        public byte VisibleClass { get; private set; }
        public byte Class { get; private set; }
        public byte Level { get; private set; }

        protected Player() { }

        public Player(string name)
        {
        }
    }
}
