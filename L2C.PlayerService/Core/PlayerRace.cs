﻿namespace L2C.PlayerService.Core
{
    class PlayerRace
    {
        public static PlayerRace HumanKnight => new PlayerRace(0);

        public byte Value { get; }

        public PlayerRace(byte value)
        {
            Value = value;
        }
    }
}
