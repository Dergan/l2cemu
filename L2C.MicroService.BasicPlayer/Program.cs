﻿using L2C.MicroService.BasicPlayer.Handlers;
using L2C.MicroService.GenericLib;
using L2C.MicroService.GenericLib.QueueMessages;
using System;

namespace L2C.MicroService.BasicPlayer
{
    class Program
    {
        static MessageBroker MsgBroker;
        static NewClientHandler newClientHandler;
        public static MicroServiceSettings settings;

        static void Main(string[] args)
        {
            settings = ConfigurationHandler.LoadConfig<MicroServiceSettings>("AppSettings.json", "L2C:MicroServiceSettings");
            MsgBroker = new MessageBroker(settings.MessageBroker);
            newClientHandler = new NewClientHandler(MsgBroker);

            //globally subscribe first to the NewClientHandler to accept new clients
            MsgBroker.SubscribeAsync<QM_NewGameClient>(string.Empty, newClientHandler.HandleMessage, (config) => config.WithQueueName(QueueMessageServiceType.BasicPlayerService.ToString()));

            Console.WriteLine("waiting for basic player requests...");
            Console.ReadLine();
        }
    }
}
