﻿using L2C.MicroService.GameMessageHandlerLib.GameMessages.Receive;
using L2C.MicroService.GameMessageHandlerLib.GameMessages.Send;
using L2C.MicroService.GenericLib;
using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.QueueMessages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace L2C.MicroService.BasicPlayer.Handlers
{
    public class PlayerHandler
    {
        private MessageBroker msgBroker;
        private Guid ConnectionId;
        private QM_NewGameClient InitMessage;

        public PlayerHandler(QM_NewGameClient InitMessage, MessageBroker MsgBroker)
        {
            this.msgBroker = MsgBroker;
            this.InitMessage = InitMessage;
            this.ConnectionId = InitMessage.ConnectionId;

            this.msgBroker.SubscribeAsync<GM_RequestGameLogin>(QueueMessageServiceType.BasicPlayerService, InitMessage.ConnectionId, HandleRequestGameLogin);
            this.msgBroker.SubscribeAsync<GM_ProtocolVersion>(QueueMessageServiceType.BasicPlayerService, InitMessage.ConnectionId, HandleProtocolVersion);
            this.msgBroker.SubscribeAsync<GM_SecondPasswordCheck>(QueueMessageServiceType.BasicPlayerService, InitMessage.ConnectionId, HandleSecondPasswordCheck);
            this.msgBroker.SubscribeAsync<GM_RequestGameStart>(QueueMessageServiceType.BasicPlayerService, InitMessage.ConnectionId, HandleRequestGameStart);
            this.msgBroker.SubscribeAsync<GM_RequestEnterWorld>(QueueMessageServiceType.BasicPlayerService, InitMessage.ConnectionId, HandleRequestEnterWorld);
        }

        private Task HandleRequestGameLogin(GM_RequestGameLogin Message)
        {
            msgBroker.SendGameMessage(new GM_LoginResult(), MessageServerType.GameServer, ConnectionId);
            msgBroker.SendGameMessage(new GM_CharacterSelectionInfo(), MessageServerType.GameServer, ConnectionId);
            return Task.CompletedTask;
        }

        private Task HandleProtocolVersion(GM_ProtocolVersion Message)
        {
            msgBroker.SendGameMessage(new GM_KeyPacket(InitMessage.XorGameKey), MessageServerType.GameServer, ConnectionId);
            return Task.CompletedTask;
        }

        private Task HandleSecondPasswordCheck(GM_SecondPasswordCheck Message)
        {
            msgBroker.SendGameMessage(new GM_ResponseSecondPasswordCheck(), MessageServerType.GameServer, ConnectionId);
            return Task.CompletedTask;
        }

        private Task HandleRequestGameStart(GM_RequestGameStart Message)
        {
            msgBroker.SendGameMessage(new GM_ResponseCharacterSelected()
            {
                Name = "Test",
                ObjectId = 1,
                Title = ""
            }, MessageServerType.GameServer, ConnectionId);
            return Task.CompletedTask;
        }

        private Task HandleRequestEnterWorld(GM_RequestEnterWorld Message)
        {

            Console.WriteLine("HandleRequestEnterWorld");

            return Task.CompletedTask;
        }
    }
}
