﻿using L2C.MicroService.GenericLib;
using L2C.MicroService.GenericLib.QueueMessages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace L2C.MicroService.BasicPlayer.Handlers
{
    public class NewClientHandler : SubscribeBase<QM_NewGameClient>
    {
        public List<PlayerHandler> Clients { get; private set; }

        public NewClientHandler(MessageBroker MsgBroker)
            : base(MsgBroker)
        {
            this.Clients = new List<PlayerHandler>();

            //initialize MessageHandler for receive
            //MessageHandler.AddMessage(typeof(GM_RequestAuthGameGuard));
            //MessageHandler.AddMessage(typeof(GM_RequestAuthLogin));
            //MessageHandler.AddMessage(typeof(GM_RequestGameServerList));
            //MessageHandler.AddMessage(typeof(GM_RequestLoginGameServer));
        }

        public override Task HandleMessage(QM_NewGameClient Message)
        {
            PlayerHandler clientHandler = new PlayerHandler(Message, MsgBroker);
            Clients.Add(clientHandler);

            return base.MsgBroker.SendMessage(Message, QueueMessageServiceType.AuthenticationService);
        }
    }
}
