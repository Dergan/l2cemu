﻿using L2C.EventBus;

namespace L2C.Shared.Events
{
    public class SessionBasedEvent : IEvent
    {

        public string SessionId { get; set; }


        public SessionBasedEvent()
        {
        }

        public SessionBasedEvent(string sessionId)
        {
            SessionId = sessionId;
        }
    }
}
