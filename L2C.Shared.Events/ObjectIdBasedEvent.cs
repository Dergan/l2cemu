﻿using L2C.EventBus;

namespace L2C.Shared.Events
{
    public class ObjectIdBasedEvent : IEvent
    {

        public uint ObjectId { get; set; }


        public ObjectIdBasedEvent()
        {
        }

        public ObjectIdBasedEvent(uint objectId)
        {
            ObjectId = objectId;
        }
    }
}
