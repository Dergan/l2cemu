﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using L2C.EventBus;
using L2C.Shared.Events.Authentication;
using Microsoft.Extensions.Logging;

namespace L2C.AuthenticationService
{
    internal class App
    {
        private readonly ILogger<App> _logger;
        private readonly IEventBus _messageBroker;

        public App(ILogger<App> logger,
            IEventBus messageBroker)
        {
            _logger = logger;
            _messageBroker = messageBroker;
            _logger.LogInformation("Starting Authentication Service");
        }

        public void Run()
        {
            _messageBroker.SubscribeAsync<AuthenticationRequest>(Handle);

            _logger.LogInformation("Authentication service started, waiting for connections");
            Process.GetCurrentProcess().WaitForExit();
        }

        private async Task Handle(AuthenticationRequest arg)
        {
            _logger.LogInformation($"Received event {arg.GetType().Name}");
            if (arg.Username == "test")
            {
                var reason = Convert.ToInt32(arg.Password);
                await _messageBroker.PublishAsync(new AuthenticationFailed(arg.SessionId)
                {
                    Reason = (AuthenticationFailed.FailReason)reason
                });
            }
            await _messageBroker.PublishAsync(new AuthenticationSuccess(arg.SessionId));
        }
    }
}
