﻿using System;
using L2C.EventBus;
using L2C.EventBus.RabbitMq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace L2C.AuthenticationService
{
    class Program
    {
        public static IConfigurationRoot Configuration = new ConfigurationBuilder()
                                        .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                        .AddJsonFile("AppSettings.json", optional: false)
                                        .Build();

        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            var serviceProvider = services.BuildServiceProvider();

            var app = serviceProvider.GetRequiredService<App>();
            app.Run();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(builder => builder.SetMinimumLevel(LogLevel.Debug).AddConsole());
            services.AddEventBus(builder => builder.UsingRabbitMq());

            services.Configure<RabbitMqSettings>(Configuration.GetSection("EventBus:RabbitMq"));

            services.AddSingleton<App>();
        }
    }
}
