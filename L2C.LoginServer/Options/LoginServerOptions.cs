﻿namespace L2C.LoginServer.Options
{
    internal class LoginServerOptions
    {

        public string Host { get; set; }
        public int Port { get; set; }
    }
}
