﻿using System;
using System.Threading.Tasks;
using DotNetty.Common.Internal.Logging;
using L2C.EventBus;
using L2C.EventBus.RabbitMq;
using L2C.LoginServer.Network;
using L2C.LoginServer.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace L2C.LoginServer
{
    class Program
    {
        public static IConfigurationRoot Configuration { get; } = new ConfigurationBuilder()
                                        .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                        .AddJsonFile("AppSettings.json", optional: false)
                                        .Build();

        public static void SetConsoleLogger() => InternalLoggerFactory.DefaultFactory.AddProvider(provider: new ConsoleLoggerProvider((s, level) => true, false));

        static async Task Main(string[] args)
        {
            SetConsoleLogger();

            IServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            var serviceProvider = services.BuildServiceProvider();

            var app = serviceProvider.GetRequiredService<App>();
            await app.Run();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(builder => builder.SetMinimumLevel(LogLevel.Debug).AddConsole());
            services.AddEventBus(builder => builder.UsingRabbitMq());

            services.Configure<LoginServerOptions>(Configuration.GetSection("Server"));
            services.Configure<RabbitMqSettings>(Configuration.GetSection("EventBus:RabbitMq"));

            services.AddSingleton<ISessionManager, SessionManager>();

            services.AddSingleton<App>();
        }
    }
}
