﻿using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using DotNetty.Handlers.Logging;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using L2C.Cryptography;
using L2C.EventBus;
using L2C.LoginServer.Extensions;
using L2C.LoginServer.Handlers;
using L2C.LoginServer.Network;
using L2C.LoginServer.Options;
using L2C.Shared.Events.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace L2C.LoginServer
{
    internal class App
    {
        private readonly IEventBus _messageBroker;
        private readonly ISessionManager _sessionManager;
        private readonly IOptionsMonitor<LoginServerOptions> _serverSettingsAccessor;
        private readonly ILogger<App> _logger;
        private ServerBootstrap _server;

        private LoginServerOptions Settings => _serverSettingsAccessor.CurrentValue;

        public App(IEventBus messageBroker,
            ISessionManager sessionManager,
            IOptionsMonitor<LoginServerOptions> serverSettingsAccessor,
            ILogger<App> logger)
        {
            _messageBroker = messageBroker;
            _sessionManager = sessionManager;
            _serverSettingsAccessor = serverSettingsAccessor;
            _serverSettingsAccessor.OnChange(OnSettingsChangeListener);
            _logger = logger;

            _logger.LogInformation("Starting Auth Gateway");

            _logger.LogInformation("Bootstrapping dotNetty");
            _server = new ServerBootstrap();

            _logger.LogInformation("Initialize EventLoop Groups");
            var bossGroup = new MultithreadEventLoopGroup();
            var workerGroup = new MultithreadEventLoopGroup();
            var blowfishGenerator = new BlowfishKeyGenerator();
            var scambledGenerator = new ScrambledKeyPairGenerator();

            _logger.LogInformation("Setup network pipeline");
            _server.Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .Handler(new LoggingHandler("LSTN"))
                .Option(ChannelOption.SoBacklog, 8192)
                .ChildHandler(new ActionChannelInitializer<IChannel>(channel =>
                {
                    var scrambledPair = scambledGenerator.Generate();

                    var blowfishCrypt = new BlowfishCrypt();
                    var blowfishKey = blowfishGenerator.Generate();
                    blowfishCrypt.SetKey(blowfishKey);

                    var loginCrypt = new LoginCrypt();

                    channel.Pipeline
                           .AddLengthPrefix()
                           .AddCryption(blowfishCrypt)
                           .AddCryption(loginCrypt)
                           .AddLast(new LoggingHandler("CONN"))
                           .AddBinarySerializer<ReceiveMessage, SendMessage>()
                           .AddFrameWrapper()
                           .AddHandler(new SessionHandler(_sessionManager))
                           .AddHandler(new InitHandler(blowfishKey, scrambledPair, _sessionManager))
                           .AddHandler<GameGuardHandler>()
                           .AddHandler(new RabbitMqHandler(messageBroker, scrambledPair));
                }));


            //_logger.LogInformation("Subscribe to all events from EventBus");
            _messageBroker.SubscribeAsync<AuthenticationFailed>(HandleAuthenticationFailed);
            _messageBroker.SubscribeAsync<AuthenticationSuccess>(HandleAuthenticationSuccess);
            _messageBroker.SubscribeAsync<ServerListResponse>(HandlerServerListResponse);
            _messageBroker.SubscribeAsync<ServerLoginSuccess>(HandleServerLoginSuccess);
            _messageBroker.SubscribeAsync<ServerLoginFail>(HandleServerLoginFail);

            _messageBroker.SubscribeAsync<RequestServerList>(HandleServerList);
            _messageBroker.SubscribeAsync<RequestServerLogin>(HandleServerLogin);
        }

        private async Task HandleServerLoginFail(ServerLoginFail arg)
        {
            //var session = _sessionManager.FindSession(arg.SessionId);
            //await session.SendAsync(new Messages.Send.PlayFail()
            //{
            //    Hash = 1234567891
            //});
        }

        private async Task HandleServerLoginSuccess(ServerLoginSuccess arg)
        {
            var session = _sessionManager.FindSession(arg.SessionId);
            await session.SendAsync(new Messages.Send.PlayOk()
            {
                Hash = 1234567891
            });
        }

        private async Task HandlerServerListResponse(ServerListResponse arg)
        {
            var session = _sessionManager.FindSession(arg.SessionId);
            await session.SendAsync(new Messages.Send.ServerList()
            {
                ServerCount = 1,
                Servers = new System.Collections.Generic.List<Messages.Send.Server>()
                {
                    new Messages.Send.Server()
                    {
                        ServerId = 1,
                        IP = new byte[] { 127,0,0,1 },
                        Port = 7777,
                        AgeLimit = 0,
                        IsPvp = false,
                        CurrentPlayers = 0,
                        MaxPlayers = 10000,
                        IsUp = true,
                        ServerType = 1024,
                        HasBrackets = false
                    }
                }
            });
        }

        private async Task HandleServerLogin(RequestServerLogin arg)
        {
            await _messageBroker.PublishAsync(new ServerLoginFail(arg.SessionId));
            await _messageBroker.PublishAsync(new ServerLoginSuccess(arg.SessionId));
        }

        private async Task HandleServerList(RequestServerList arg)
        {
            await _messageBroker.PublishAsync(new ServerListResponse(arg.SessionId));
        }

        private async Task HandleAuthenticationSuccess(AuthenticationSuccess arg)
        {
            var session = _sessionManager.FindSession(arg.SessionId);
            await session.SendAsync(new Messages.Send.LoginOk()
            {
                Hash = 1234567891
            });
        }

        private async Task HandleAuthenticationFailed(AuthenticationFailed arg)
        {
            var session = _sessionManager.FindSession(arg.SessionId);
            await session.SendAsync(new Messages.Send.LoginFail()
            {
                Reason = (Messages.Send.LoginFail.FailReason)(int)arg.Reason
            });
        }

        private void OnSettingsChangeListener(LoginServerOptions settings)
        {
        }

        public async Task Run()
        {
            _logger.LogInformation($"Binding to endpoint {Settings.Host}:{Settings.Port}");
            IChannel channel = await _server.BindAsync(IPAddress.Parse(Settings.Host), Settings.Port);

            _logger.LogInformation("Auth Gateway started, waiting for connections");
            Process.GetCurrentProcess().WaitForExit();
        }
    }
}
