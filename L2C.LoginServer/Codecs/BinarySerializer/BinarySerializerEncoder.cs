﻿using BinarySerialization;
using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace L2C.LoginServer
{
    class BinarySerializerEncoder<TInput> : MessageToMessageEncoder<TInput>
    {
        private BinarySerializer _serializer;

        public BinarySerializerEncoder()
        {
            _serializer = new BinarySerialization.BinarySerializer()
            {
                Encoding = Encoding.UTF8,
                Endianness = Endianness.Little
            };
        }

        protected override void Encode(IChannelHandlerContext ctx, TInput msg, List<object> output)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                _serializer.Serialize(stream, msg);
                var buf = Unpooled.WrappedBuffer(stream.ToArray());
                output.Add(buf);
            }
        }
    }
}
