﻿using System.Collections.Generic;
using System.Text;
using BinarySerialization;
using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;

namespace L2C.LoginServer
{
    class BinarySerializerDecoder<TOutput> : MessageToMessageDecoder<IByteBuffer>
    {
        private BinarySerializer _serializer;

        public BinarySerializerDecoder()
        {
            _serializer = new BinarySerializer()
            {
                Encoding = Encoding.UTF8,
                Endianness = Endianness.Little
            };
        }

        protected override void Decode(IChannelHandlerContext context, IByteBuffer message, List<object> output)
        {
            output.Add(_serializer.Deserialize<TOutput>(message.Array));
        }
    }
}
