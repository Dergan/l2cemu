﻿using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using L2C.Cryptography;
using System.Collections.Generic;

namespace L2C.LoginServer
{
    class CryptionCodec : MessageToMessageCodec<IByteBuffer, IByteBuffer>
    {
        private ICryptEngine _cryptEngine;

        public CryptionCodec(ICryptEngine cryptEngine)
        {
            _cryptEngine = cryptEngine;
        }

        protected override void Decode(IChannelHandlerContext ctx, IByteBuffer msg, List<object> output)
        {
            var dec = _cryptEngine.Decrypt(msg.Array, msg.ArrayOffset + msg.ReaderIndex, msg.ReadableBytes);
            var decBuf = Unpooled.WrappedBuffer(dec);
            output.Add(decBuf);
        }

        protected override void Encode(IChannelHandlerContext ctx, IByteBuffer msg, List<object> output)
        {
            var enc = _cryptEngine.Encrypt(msg.Array, msg.ReaderIndex, msg.ReadableBytes);
            var encBuf = Unpooled.WrappedBuffer(enc);
            output.Add(encBuf);
        }
    }
}
