﻿using System.Collections.Generic;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;

namespace L2C.LoginServer.Codecs
{



    class FrameWrapper : MessageToMessageCodec<ReceiveMessage, SendBaseFrame>
    {
        protected override void Decode(IChannelHandlerContext ctx, ReceiveMessage msg, List<object> output)
        {
            output.Add(msg.Frame);
        }

        protected override void Encode(IChannelHandlerContext ctx, SendBaseFrame msg, List<object> output)
        {
            output.Add(new SendMessage() { Frame = msg });
        }
    }
}
