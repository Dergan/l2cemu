﻿using BinarySerialization;

namespace L2C.LoginServer
{
    abstract class BaseMessage<TFrame>
        where TFrame : BaseFrame
    {
        [FieldOrder(0)]
        public byte Opcode { get; set; }

        [FieldOrder(1)]
        public abstract TFrame Frame { get; set; }
    }
}
