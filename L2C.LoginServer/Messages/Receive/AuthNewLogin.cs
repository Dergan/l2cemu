﻿using System.Linq;
using L2C.LoginServer.Network.Client.Packets.Send;

namespace L2C.LoginServer.Network.Client.Packets.Receive
{
    public class AuthNewLogin : L2ReceiveBaseFrame
    {

        public override void Run(GameClient Client)
        {
            Client.SendLoginOk();
        }
    }
}