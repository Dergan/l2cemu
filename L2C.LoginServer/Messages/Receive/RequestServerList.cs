using BinarySerialization;

namespace L2C.LoginServer.Messages.Receive
{
    class RequestServerList : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        public ulong LoginOk;
    }
}