﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Receive
{
    class ResponseAuthGameGuard : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        public int SessionId { get; set; }

        [FieldOrder(1)]
        [FieldLength(35)]
        public byte[] GGData { get; set; }
    }
}