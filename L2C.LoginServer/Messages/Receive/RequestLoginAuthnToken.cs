﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Receive
{
    class RequestLoginAuthnToken : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        public int Unk1;
        [FieldOrder(1)]
        public int Unk2;
        [FieldOrder(2)]
        public int Unk3;
        [FieldOrder(3)]
        public int Unk4;
        [FieldOrder(4)]
        public int Unk5;
        [FieldOrder(5)]
        [FieldLength(2)]
        public string Unk6;

        [FieldOrder(6)]
        [FieldLength(100)]
        public string Token;

        [FieldOrder(7)]
        public int Unk7;
        [FieldOrder(8)]
        public int Unk8;

        [FieldOrder(9)]
        public string Unk9;

        //12 
        //01 00 00 00 
        //00 00 00 00 
        //00 00 00 00 
        //00 00 00 00 
        //00 00 00 00 
        //6E 70 
        //00 
        //52 54 52 42 4E 55 51 30 4D 54 41 74 4F 55 56 44 
        //52 69 31 46 4D 54 45 78 4C 54 6C 45 51 54 4D 74 
        //52 54 51 78 4D 54 56 43 51 6B 49 33 4D 44 67 79 
        //4F 6B 4A 45 4D 7A 46 43 52 6A 63 31 4C 54 63 79 
        //4D 55 55 74 4E 44 49 30 51 53 30 34 52 44 45 30 
        //4C 55 4A 42 4E 44 46 47 4D 6A 52 47 4E 6A 41 31 
        //4E 67 41 3D 
        //00 08 00 00 
        //00 00 00 00 
        //45 6D 70 74 79 00 00 
        //00 00 00 00 
        //00 62 47 53 
        //08 00 00 00 
        //00 00 00 00 00 
    }
}