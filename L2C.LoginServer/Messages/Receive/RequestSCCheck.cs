﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Receive
{
    class RequestSCCheck : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        public int Unk1;

        [FieldOrder(1)]
        [FieldLength(128)]
        public byte[] Scambled;

        [FieldOrder(2)]
        public byte Unk2;

        [FieldOrder(3)]
        public byte Unk3;

        [FieldOrder(4)]
        public byte Unk4;

        [FieldOrder(5)]
        public int Unk5;

        [FieldOrder(6)]
        public int Unk6;

        [FieldOrder(7)]
        public int Unk7;
    }
}