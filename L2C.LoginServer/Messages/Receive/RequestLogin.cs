﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Receive
{
    class RequestLogin : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        [FieldLength(128)]
        public byte[] ScrambledUsername;

        [FieldOrder(1)]
        [FieldLength(128)]
        public byte[] ScrambledPassword;

        [FieldOrder(2)]
        [FieldLength(63)]
        public byte[] UnkData2;
    }
}