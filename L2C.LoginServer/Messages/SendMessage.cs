﻿using BinarySerialization;
using L2C.LoginServer.Messages.Send;

namespace L2C.LoginServer
{
    class SendMessage : BaseMessage<SendBaseFrame>
    {
        [Subtype("Opcode", 0x00, typeof(SetEncryption))]
        [Subtype("Opcode", 0x01, typeof(LoginFail))]
        //[Subtype("Opcode", 0x02, typeof(BlockedAccount))]
        [Subtype("Opcode", 0x03, typeof(LoginOk))]
        [Subtype("Opcode", 0x04, typeof(ServerList))]
        //[Subtype("Opcode", 0x05, typeof(ServerFail))]
        //[Subtype("Opcode", 0x06, typeof(PlayFail))]
        [Subtype("Opcode", 0x07, typeof(PlayOk))]
        //[Subtype("Opcode", 0x08, typeof(AccountKicked))]
        //[Subtype("Opcode", 0x09, typeof(BlockedAccountWithMsg))]
        [Subtype("Opcode", 0x0A, typeof(SCCheckReq))]
        [Subtype("Opcode", 0x0B, typeof(AuthGameGuard))]
        //[Subtype("Opcode", 0x0C??, typeof(LoginOtpFail))]
        //[Subtype("Opcode", 0x0D??, typeof(PIAgreementAck))]
        //[Subtype("Opcode", 0x0E??, typeof(PIAgreementCheckAck))]
        [Subtype("Opcode", 0x0F, typeof(TelAuthStarted))]
        //[Subtype("Opcode", 0x11, typeof(Unkown11))]
        public override SendBaseFrame Frame { get; set; }
    }
}
