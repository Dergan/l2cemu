﻿using BinarySerialization;
using L2C.LoginServer.Messages.Receive;

namespace L2C.LoginServer
{
    class ReceiveMessage : BaseMessage<ReceiveBaseFrame>
    {

        [Subtype("Opcode", 0x00, typeof(RequestLogin))]
        [Subtype("Opcode", 0x02, typeof(RequestServerLogin))]
        [Subtype("Opcode", 0x05, typeof(RequestServerList))]
        [Subtype("Opcode", 0x06, typeof(RequestSCCheck))]
        [Subtype("Opcode", 0x07, typeof(ResponseAuthGameGuard))]
        //[Subtype("Opcode", 0x0B, typeof(RequestLoginWeb))]
        //[Subtype("Opcode", 0x0E, typeof(Unknown0E))]
        //[Subtype("Opcode", 0x12, typeof(RequestLoginAuthnToken))]
        public override ReceiveBaseFrame Frame { get; set; }
    }
}
