﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Send
{
    class PlayOk : SendBaseFrame
    {

        [FieldOrder(0)]
        public ulong Hash { get; set; }

        //[FieldOrder(1)]
        //public uint ServerId { get; set; }

        //[FieldOrder(2)]
        //public ulong Unk1 { get; set; } = 0x00;//0x2623D707000000;

        //[FieldOrder(3)]
        //public ushort Unk2 { get; set; } = 0x00;

        //[FieldOrder(4)]
        //public byte Unk3 { get; set; } = 0x00;


    }
}