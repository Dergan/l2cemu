﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Send
{
    class SetEncryption : SendBaseFrame
    {
        [FieldOrder(0)]
        public int SessionId { get; set; }
        [FieldOrder(1)]
        public int ProtocolVersion { get; set; }
        [FieldOrder(2)]
        [FieldLength(128)]
        public byte[] RsaPublicKey { get; set; }

        [FieldOrder(3)]
        [FieldLength(16)]
        public byte[] GameGuard { get; set; }

        [FieldOrder(4)]
        [FieldLength(16)]
        public byte[] BlowfishKey { get; set; }
        [FieldOrder(5)]
        public byte Null => 0x00;
    }
}
