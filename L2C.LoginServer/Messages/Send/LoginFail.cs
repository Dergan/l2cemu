﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Send
{
    internal class LoginFail : SendBaseFrame
    {

        public enum FailReason : int
        {
            Nothing = 0,
            SystemErrorTryAgain = 1,
            PassWrong = 2,
            UserOrPassWrong = 3,
            AccessFailedTryAgain = 4,
            IncorrectAccountInfo = 5,
            AccountInUse = 7,
            TooYoung = 12,
            ServerOverloaded = 15,
            ServerMaintenance = 16,
            ChangeTempPass = 17,
            TempPassExpired = 18,
            NoTimeLeft = 19,
            SystemError = 20,
            AccessFailed = 21,
            RestrictedIp = 22,
            Six = 25, //Just the number 6 LOL!
            WeekTimeFinished = 30,
            SecurityCard = 31, //Enter Security card number
            NotVerifyAge = 32,
            NoAccessCoupon = 33,
            DualBox = 35,
            InactiveReactivate = 36,
            AcceptUserAgreement = 37,
            GuardianConsent = 38,
            DeclinedAgreementOrWidthdrawl = 39,
            AccountSuspended = 40,
            ChangePassQuiz = 41,
            Accessed10Accounts = 42,
            MasterAccountRestricted = 43,
            CertificationFailed = 46,
            PhoneServiceOffline = 47,
            PhoneSignalDelay = 48,
            PhoneCallNotReceived = 49,
            PhoneExpired = 50,
            PhoneChecked = 51,
            PhoneHeavyValume = 52,
            PhoneExpiredBlocked = 53,
            PhoneFailed3Times = 54,
            MaxPhoneUsesExceeded = 55,
            PhoneUnderway = 56
        };

        [FieldOrder(0)]
        public FailReason Reason { get; set; }
    }
}