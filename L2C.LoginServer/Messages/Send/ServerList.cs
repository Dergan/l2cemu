﻿using System.Collections.Generic;
using BinarySerialization;

namespace L2C.LoginServer.Messages.Send
{
    class ServerList : SendBaseFrame
    {
        [FieldOrder(0)]
        public byte ServerCount { get; set; }

        [FieldOrder(1)]
        public byte LastServer { get; set; }

        [FieldOrder(2)]
        [FieldCount("ServerCount")]
        public List<Server> Servers { get; set; }

        [FieldOrder(3)]
        public ushort Unk1 { get; set; } = 0xA4;

        //[FieldOrder(4)]
        //public byte Unk2 { get; set; } = 0x01;

        //[FieldOrder(5)]
        //public byte Unk3 { get; set; } = 0x06;

        //[FieldOrder(4)]
        //public List<ServerCharacter> CharactersOnServers { get; set; }

        //[FieldOrder(5)]
        //public ushort Unk3 { get; set; } = 0;

        //[FieldOrder(6)]
        //public byte[] Unk4 { get; set; } = new byte[16];
        //    new byte[]{
        //    0x00,0x00,0x52,0xBB,
        //    0x4C,0xC9,0xCC,0xBB,
        //    0xCA,0xBD,0x5E,0x8D,
        //    0xEB,0x3F,0x35,0x2F
        //};
    }
}