﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Send
{
    class LoginOk : SendBaseFrame
    {
        [FieldOrder(0)]
        public long Hash { get; set; }
        [FieldOrder(1)]
        public int Unk1 => 0x00;
        [FieldOrder(2)]
        public int Unk2 => 0x00;
        [FieldOrder(3)]
        public int Unk3 => 0x000003ea;
        [FieldOrder(4)]
        public int Unk4 => 0x00;
        [FieldOrder(5)]
        public int Unk5 => 0x00;
        [FieldOrder(6)]
        public int Unk6 => 0x00;
        [FieldOrder(7)]
        [FieldLength(16)]
        public byte[] Unk7 => new byte[16];
    }
}