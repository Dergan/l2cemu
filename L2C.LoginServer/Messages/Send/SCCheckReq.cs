﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Send
{
    class SCCheckReq : SendBaseFrame
    {

        [FieldOrder(0)]
        public uint AccountId { get; set; }

        [FieldOrder(1)]
        [FieldLength(128)]
        public byte[] Key { get; set; } = new byte[128];

        [FieldOrder(2)]
        [FieldLength(3)]
        public byte[] Padding1 { get; set; } = new byte[3];

        [FieldOrder(3)]
        public uint Checksum { get; set; }

        [FieldOrder(4)]
        [FieldLength(12)]
        public byte[] Padding2 { get; set; } = new byte[12];
    }
}