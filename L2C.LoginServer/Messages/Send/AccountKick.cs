﻿using System.Linq;
using BinarySerialization;

namespace L2C.LoginServer.Network.Client.Packets.Send
{
    public class AccountKick : L2SendBaseFrame
    {
        public enum KickReason : int
        {
            ReasonDataStealer = 0x01,
            ReasonGenericViolation = 0x08,
            Reason7DaysSuspended = 0x10,
            ReasonPermanentlyBanned = 0x20
        };

        [FieldOrder(0)]
        public KickReason Reason { get; set; }

        public override void Run(GameClient client, params object[] parameters)
        {
        }
    }
}