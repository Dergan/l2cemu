﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Send
{

    class Server
    {
        [FieldOrder(0)]
        public byte ServerId { get; set; }
        [FieldOrder(1)]
        [FieldLength(4)]
        public byte[] IP { get; set; }
        [FieldOrder(2)]
        public int Port { get; set; }
        [FieldOrder(3)]
        public byte AgeLimit { get; set; }
        [FieldOrder(4)]
        public bool IsPvp { get; set; }
        [FieldOrder(5)]
        public ushort CurrentPlayers { get; set; }
        [FieldOrder(6)]
        public ushort MaxPlayers { get; set; }
        [FieldOrder(7)]
        public bool IsUp { get; set; }
        [FieldOrder(8)]
        public int ServerType { get; set; }//ServerType  1: Normal, 2: Relax, 4: Public Test, 8: No Label, 16: Character Creation Restricted, 32: Event, 64: Free
        [FieldOrder(9)]
        public bool HasBrackets { get; set; }
    }
}