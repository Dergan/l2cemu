﻿using BinarySerialization;

namespace L2C.LoginServer.Messages.Send
{
    class AuthGameGuard : SendBaseFrame
    {
        [FieldOrder(0)]
        public byte[] GameGuardData { get; set; }
    }
}