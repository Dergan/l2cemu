﻿namespace L2C.LoginServer.Messages.Send
{
    public class ServerCharacter
    {
        public byte ServerId { get; set; }
        public byte CharacterCount { get; set; }
    }
}