﻿using DotNetty.Transport.Channels;

namespace L2C.LoginServer.Network
{
    class DotNettySessionId : ISessionId
    {
        private readonly IChannelId _channelId;

        public DotNettySessionId(IChannelId channelId)
        {
            if (channelId == null)
            {
                throw new System.ArgumentNullException(nameof(channelId));
            }

            _channelId = channelId;
        }

        public bool Equals(ISessionId other)
        {
            if (other == null)
            {
                return false;
            }

            return other.GetHashCode() == GetHashCode();
        }

        public override int GetHashCode()
        {
            return _channelId.GetHashCode();
        }

        public override string ToString()
        {
            return _channelId.AsLongText();
        }
    }

}
