﻿using System.Threading.Tasks;
using DotNetty.Transport.Channels;

namespace L2C.LoginServer.Network
{
    internal class DotNettySession : ISession
    {
        private readonly IChannel _channel;

        public ISessionId Id { get; }

        public DotNettySession(IChannel channel)
        {
            _channel = channel;
            Id = new DotNettySessionId(_channel.Id);
        }

        public Task SendAsync<T>(T packet)
        {
            return _channel.WriteAndFlushAsync(packet);
        }
    }
}