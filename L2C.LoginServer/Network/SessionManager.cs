﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace L2C.LoginServer.Network
{
    class SessionManager : ISessionManager
    {
        Dictionary<ISessionId, ISession> _sessions = new Dictionary<ISessionId, ISession>();

        public SessionManager()
        {

        }

        public void Add<T>(T session)
            where T : ISession
        {
            _sessions.Add(session.Id, session);
        }

        public ISession FindSession(string sessionId)
        {
            return _sessions.Values.FirstOrDefault(x => x.Id.ToString() == sessionId);
        }

        public ISession GetSession(ISessionId sessionId)
        {
            if (sessionId == null)
            {
                throw new ArgumentNullException(nameof(sessionId));
            }
            return _sessions[sessionId];
        }

        public void Remove(ISessionId sessionId)
        {
            _sessions.Remove(sessionId);
        }
    }
}
