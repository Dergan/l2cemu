﻿using System;
using System.Threading.Tasks;

namespace L2C.LoginServer.Network
{
    interface ISessionManager
    {

        void Add<T>(T session)
               where T : ISession;
        void Remove(ISessionId sessionId);
        ISession GetSession(ISessionId sessionId);
        ISession FindSession(string sessionId);
    }

    public interface ISessionId : IEquatable<ISessionId>
    {
        string ToString();
    }

    public interface ISession
    {
        ISessionId Id { get; }
        Task SendAsync<T>(T packet);
    }

}
