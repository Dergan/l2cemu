﻿using System;
using System.Linq;
using System.Text;
using DotNetty.Transport.Channels;
using L2C.EventBus;
using L2C.LoginServer.Messages.Receive;
using L2C.LoginServer.Network;
using L2C.Shared.Events.Authentication;

namespace L2C.LoginServer
{
    class RabbitMqHandler : SimpleChannelInboundHandler<ReceiveBaseFrame>
    {
        private readonly IEventBus _messageBroker;
        private readonly Cryptography.ScrambledKeyPair _scrambledPair;

        public RabbitMqHandler(IEventBus messageBroker, Cryptography.ScrambledKeyPair scrambledPair)
        {
            _messageBroker = messageBroker;
            _scrambledPair = scrambledPair;
        }

        protected override void ChannelRead0(IChannelHandlerContext ctx, ReceiveBaseFrame msg)
        {
            var sessionId = new DotNettySessionId(ctx.Channel.Id).ToString();
            switch (msg)
            {
                case RequestLogin auth:
                    var evt = new AuthenticationRequest(sessionId);

                    Memory<byte> username = _scrambledPair.Decrypt(auth.ScrambledUsername).Where(x => x != 0).ToArray();
                    Memory<byte> password = _scrambledPair.Decrypt(auth.ScrambledPassword).Where(x => x != 0).ToArray();
                    username = username.Slice(1, username.Length - 1);
                    password = password.Slice(1, password.Length - 1);

                    evt.Username = Encoding.ASCII.GetString(username.ToArray()).ToLower();
                    evt.Password = Encoding.ASCII.GetString(password.ToArray());

                    _ = _messageBroker.PublishAsync(evt);
                    break;
                case Messages.Receive.RequestServerList requestServerList:
                    _ = _messageBroker.PublishAsync(new Shared.Events.Authentication.RequestServerList(sessionId)
                    {
                        Hash = requestServerList.LoginOk
                    });
                    break;
                case Messages.Receive.RequestServerLogin loginGameServerRequest:
                    _ = _messageBroker.PublishAsync(new Shared.Events.Authentication.RequestServerLogin(sessionId)
                    {
                    });
                    break;
                default:
                    //_ = _messageBroker.PublishAsync(msg);
                    break;
            }
        }
    }
}
