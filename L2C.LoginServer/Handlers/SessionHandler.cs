﻿using DotNetty.Transport.Channels;
using L2C.LoginServer.Network;

namespace L2C.LoginServer.Handlers
{
    internal class SessionHandler : ChannelHandlerAdapter
    {
        private readonly ISessionManager _sessionManager;

        public SessionHandler(ISessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }

        public override void ChannelActive(IChannelHandlerContext context)
        {
            base.ChannelActive(context);
            _sessionManager.Add(new DotNettySession(context.Channel));
        }

        public override void ChannelInactive(IChannelHandlerContext context)
        {
            base.ChannelInactive(context);

            var sessionId = new DotNettySessionId(context.Channel.Id);
            _sessionManager.Remove(sessionId);
        }
    }
}
