﻿using DotNetty.Transport.Channels;
using L2C.Cryptography;
using L2C.LoginServer.Messages.Send;

namespace L2C.LoginServer.Handlers
{
    class InitHandler : ChannelHandlerAdapter
    {
        private readonly byte[] _blowfishKey;
        private readonly ScrambledKeyPair _scrambledPair;
        private readonly Network.ISessionManager _sessionManager;
        private bool initSent = false;

        public InitHandler(byte[] blowfishKey, ScrambledKeyPair scrambledPair, Network.ISessionManager sessionManager)
        {
            _blowfishKey = blowfishKey;
            _scrambledPair = scrambledPair;
            _sessionManager = sessionManager;
        }

        public override void ChannelActive(IChannelHandlerContext context)
        {
            base.ChannelActive(context);

            if (!initSent)
            {
                context.WriteAndFlushAsync(new SetEncryption()
                {
                    SessionId = 0x01,//context.Channel.Id.GetHashCode(),
                    ProtocolVersion = 0xC621,
                    RsaPublicKey = _scrambledPair.GetScrambledModulus(),
                    GameGuard = new byte[]
                    {
                        0x4E, 0x95, 0xDD, 0x29,
                        0xFC, 0x9C, 0xC3, 0x77,
                        0x20, 0xB6, 0xAD, 0x97,
                        0xF7, 0xE0, 0xBD, 0x07
                    },
                    BlowfishKey = _blowfishKey
                });
                initSent = true;

                base.ChannelReadComplete(context);
            }
        }
    }
}
