﻿using DotNetty.Transport.Channels;
using L2C.LoginServer.Messages.Receive;
using L2C.LoginServer.Messages.Send;

namespace L2C.LoginServer.Handlers
{
    class GameGuardHandler : SimpleChannelInboundHandler<ResponseAuthGameGuard>
    {
        protected override void ChannelRead0(IChannelHandlerContext ctx, ResponseAuthGameGuard msg)
        {
            ctx.WriteAndFlushAsync(new AuthGameGuard()
            {
                GameGuardData = msg.GGData
            });
            base.ChannelReadComplete(ctx);
        }
    }
}
