﻿using DotNetty.Transport.Channels;

namespace L2C.LoginServer.Extensions
{
    internal static class ChannelIdExtensions
    {

        public static int AsInt(this IChannelId channelId)
            => int.Parse(channelId.AsShortText(), System.Globalization.NumberStyles.AllowHexSpecifier);
    }
}
