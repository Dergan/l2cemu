﻿namespace L2C.EventBus
{
    public interface IEventBusFactory
    {

        IEventBus Create();
    }
}
