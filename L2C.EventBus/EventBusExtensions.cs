﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace L2C.EventBus
{
    public static class EventBusExtensions
    {

        public static IServiceCollection AddEventBus(this IServiceCollection services, Action<IEventBusBuilder> builder)
        {
            var busBuilder = new DefaultEventBusBuilder(services);
            builder(busBuilder);
            services.AddTransient(typeof(IEventBus), s => s.GetService<IEventBusFactory>().Create());
            return services;
        }
    }
}
