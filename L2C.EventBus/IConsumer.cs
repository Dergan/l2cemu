﻿using System;
using System.Threading.Tasks;

namespace L2C.EventBus
{
    public interface IConsumer<TEvent> : IConsumer
        where TEvent : IEvent
    {

        Task HandleAsync(TEvent @event);
    }

    public interface IConsumer
    {
        Type EventType { get; }
        Task HandleAsync(object @event);
    }
}
