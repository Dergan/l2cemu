﻿using System;
using System.Threading.Tasks;

namespace L2C.EventBus
{
    public interface IEventBus
    {

        Task PublishAsync<TEvent>(TEvent message) where TEvent : class, IEvent;
        Task PublishAsync(object message);
        void Subscribe<TEvent>(Action<TEvent> callback) where TEvent : class, IEvent;
        void Subscribe(Type eventType, Action<object> callback);
        Task SubscribeAsync<TEvent>(Func<TEvent, Task> callbackAsync) where TEvent : class, IEvent;
        Task SubscribeAsync(Type eventType, Func<object, Task> callbackAsync);
    }
}
