﻿using Microsoft.Extensions.DependencyInjection;

namespace L2C.EventBus
{

    public interface IEventBusBuilder
    {

        IServiceCollection Services { get; }

        IEventBusBuilder AddConsumer<TConsumer, TConsumerType>()
            where TConsumer : class, IConsumer<TConsumerType>
            where TConsumerType : IEvent;
    }

    class DefaultEventBusBuilder : IEventBusBuilder
    {
        public IServiceCollection Services { get; }

        public DefaultEventBusBuilder(IServiceCollection services)
        {
            Services = services;
        }

        public IEventBusBuilder AddConsumer<TConsumer, TConsumerType>()
            where TConsumer : class, IConsumer<TConsumerType>
            where TConsumerType : IEvent
        {
            Services.AddTransient<IConsumer, TConsumer>();
            Services.AddTransient<IConsumer<TConsumerType>, TConsumer>();
            return this;
        }
    }
}