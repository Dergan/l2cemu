﻿using System.Collections.Generic;
using L2C.GameServer.World;
using L2C.GameServer.Objects;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_ValidatePos : L2ReceiveBaseFrame
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(L2WorldRegion));

        private int _x;
        private int _y;
        private int _z;
        private int _heading;
        private int _VehicleId;

        public override void Run(GameClient client)
        {
            client.PlayerInstance.Position.X = _x;
            client.PlayerInstance.Position.Y = _y;
            client.PlayerInstance.Position.Z = _z;
            client.PlayerInstance.Position.Heading = _heading;

            if (L2World.Instance.CheckPlayerRegion(client.PlayerInstance))
            {
                //L2Npc[] npcs = L2World.Instance.GetNpcsFromRegionAndArround(L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString());
                List<uint> ObjectIds = new List<uint>();
                //for (int i = 0; i < npcs.Length; i++)
                //{
                //    if (!client.PlayerInstance.KnownList.Contains(npcs[i].ObjectId))
                //    {
                //        client.PacketProcessor.SendPacket(new S_NpcInfo(npcs[i]), PacketPriority.Normal);
                //        client.PlayerInstance.KnownList.Add(npcs[i].ObjectId);
                //    }
                //    ObjectIds.Add(npcs[i].ObjectId);
                // }
                L2Player[] players = L2World.Instance.GetPlayersFromRegionAndArround(L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString());
                for (int i = 0; i < players.Length; i++)
                {
                    if (!client.PlayerInstance.KnownList.Contains(players[i].ObjectId)
                        && players[i].ObjectId != client.PlayerInstance.ObjectId)
                    {
                        //client.PacketProcessor.SendPacket(new S_CharInfo(players[i]), PacketPriority.Normal);
                        client.PlayerInstance.KnownList.Add(players[i].ObjectId);
                    }
                    ObjectIds.Add(players[i].ObjectId);
                }

                for (int i = 0; i < ObjectIds.Count; i++)
                {
                    if (!client.PlayerInstance.KnownList.Contains(ObjectIds[i])
                        && ObjectIds[i] != client.PlayerInstance.ObjectId)
                        client.PlayerInstance.KnownList.Remove(ObjectIds[i]);
                }
                
                //L2World.Instance.NpcAIHandlerRegionAndArround(L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString());
                Logger.Debug("Player: " + client.PlayerInstance.Name + " is in a new region " + L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString());
                client.PlayerInstance.prevPosition.X = client.PlayerInstance.Position.X;
                client.PlayerInstance.prevPosition.Y = client.PlayerInstance.Position.Y;
                client.PlayerInstance.prevPosition.Z = client.PlayerInstance.Position.Z;
                client.PlayerInstance.prevPosition.Region = L2World.Instance.GetRegionFromPosition(client.PlayerInstance.prevPosition).ToString();
            }
        }
    }
}