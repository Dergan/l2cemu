﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Network.GameServer.Packets.Send;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_CommunityBoard : L2ReceiveBaseFrame
    {

        public override void Run(GameClient client)
        {
            client.PacketProcessor.SendPacket(new S_CommunityBoard(), PacketPriority.Normal);
        }
    }
}