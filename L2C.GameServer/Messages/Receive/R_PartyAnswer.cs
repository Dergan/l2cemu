﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Network.GameServer.Packets.Send;
using L2C.GameServer.Objects;
using L2C.GameServer.Collections;
using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_PartyAnswer : L2ReceiveBaseFrame
    {

        [FieldOrder(0)]
        private PartyAnswer answer;

        public override void Run(GameClient client)
        {
            L2Player Receiver = client.PlayerInstance;
            L2Player Invitor = (L2Player)Receiver.PartyInvitor;
            Invitor.client.PacketProcessor.SendPacket(new S_PartyAnswer(answer), PacketPriority.Normal);
                        
            if (answer == PartyAnswer.Yes)
            {
                Logger.Info(client.PlayerInstance.Name + " joined the party", Logging.Logger.LogType.Debug);

                //create a new party if not already exists.
                if (Invitor.Party == null)
                    Invitor.Party = new PartyCollection(Invitor, PartyLoot.Normal);
                Invitor.Party.Add(Receiver);
            }
            else
            {
                Logger.Info(client.PlayerInstance.Name + " declined the party invite", Logging.Logger.LogType.Debug);
            }
        }
    }
}