﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Objects;
using L2C.GameServer.Collections;
using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    class R_ShortcutRegister : L2ReceiveBaseFrame
    {
        private int type;
        private int id;
        private int slot;

        [Ignore]
        private int page;
        private int level;
        private int charType;

        public override void Run(GameClient client)
        {
            page = slot / 12;
            slot = slot % 12;

            L2ShortcutInfo shortcut = new L2ShortcutInfo(type, id, slot, page, level, charType);
            client.PlayerInstance.ShortCuts.Add(shortcut);
        }
    }
}