﻿using L2C.GameServer.Network.GameServer.Packets.Send;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_UnselectTarget : L2ReceiveBaseFrame
    {
        bool Unselect;

        public override void Run(GameClient client)
        {
            if (client.PlayerInstance == null)
            {
                client.Disconnect();
                return;
            }
            client.PlayerInstance.PrevObjectId = 0;
            client.PacketProcessor.SendPacket(new S_UnselectTarget(client.PlayerInstance), PacketPriority.Normal);
        }
    }
}