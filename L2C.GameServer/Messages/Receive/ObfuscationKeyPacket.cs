﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Receive
{
    class ObfuscationKeyPacket : ReceiveBaseFrame
    {

        [FieldOrder(0)]
        [FieldLength(40)]
        byte[] ObfuscateKey;
    }
}