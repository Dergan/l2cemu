﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Network.GameServer.Packets.Send;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    class R_RequestDuelStart : L2ReceiveBaseFrame
    {
        public override void Run(GameClient client)
        {
            if (client.PlayerInstance.Target.ObjectId == 0)
            {
                Logger.Info("Player " + client.PlayerInstance.Name + " tried to start a duel without a target", Logging.Logger.LogType.Warning);
                return;
            }

            if(client.PlayerInstance.ObjectId == client.PlayerInstance.Target.ObjectId)
            {
                Logger.Info("Player " + client.PlayerInstance.Name + " tried to start a duel with his self", Logging.Logger.LogType.Warning);
                client.PacketProcessor.SendPacket(new S_ActionFailed(), PacketPriority.Normal);
                return;
            }
        }
    }
}