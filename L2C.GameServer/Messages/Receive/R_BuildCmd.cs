﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Handler;
using System.Threading;
using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_BuildCmd : L2ReceiveBaseFrame
    {
        [FieldOrder(0)]
        [SerializeAs(SerializedType.LengthPrefixedString)]
        [FieldEncoding("UTF8")]
        private string CmdCommand;

        public override void Run(GameClient client)
        {
            if(!client.PlayerInstance.isGM)
            {
                //this cant be done if ur not gm.... hlapex ?
                client.Disconnect();
                return;
            }

            Type pck = ProcessCommand.Processcommand(CmdCommand.Split(' ')[0], client.PlayerInstance);
            if (pck != null)
            {
                L2BasePacket sbp = (L2BasePacket)Activator.CreateInstance(pck, client, CmdCommand);
                client.PacketProcessor.SendPacket(sbp, PacketPriority.Normal);
            }
        }
    }
}