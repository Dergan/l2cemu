﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Objects;
using L2C.GameServer.World;
using L2C.GameServer.Database.Tables;
using L2C.GameServer.Network.GameServer.Packets.Send;
using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_JoinParty : L2ReceiveBaseFrame
    {
        [FieldOrder(0)]
        [SerializeAs(SerializedType.LengthPrefixedString)]
        [FieldEncoding("UTF8")]
        private string playerName;

        public override void Run(GameClient client)
        {
            if (client.PlayerInstance.Party != null)
            {
                if (client.PlayerInstance.Party.Owner.ObjectId == client.PlayerInstance.ObjectId)
                {
                    Logger.Info("Player " + client.PlayerInstance.Name + " tried to invite, but is not party leader.", Logging.Logger.LogType.Debug);
                    return;
                }
            }

            int ObjId = Characters.Instance.GetObjectIdFromName(playerName);
            if (ObjId != 0)
            {
                L2Player player = L2World.Instance.FindPlayer(ObjId);
                if (player != null)
                {
                    player.PartyInvitor = client.PlayerInstance;
                    Logger.Info("player " + client.PlayerInstance.Name + " tried to invite " + playerName + " to the party", Logging.Logger.LogType.Debug);
                    player.client.PacketProcessor.SendPacket(new S_RequestJoinParty(player, client.PlayerInstance.Name), PacketPriority.Normal);
                }
            }
            else
            {
                Logger.Info("Player " + client.PlayerInstance.Name + " tried to invite a not existing player " + playerName, Logging.Logger.LogType.Debug);
            }
        }
    }
}