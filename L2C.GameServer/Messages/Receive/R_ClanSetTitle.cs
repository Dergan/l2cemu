﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Objects;
using L2C.GameServer.Database.Tables;
using L2C.GameServer.World;
using L2C.GameServer.Network.GameServer.Packets.Send;
using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_ClanSetTitle : L2ReceiveBaseFrame
    {

        [FieldOrder(0)]
        [SerializeAs(SerializedType.LengthPrefixedString)]
        [FieldEncoding("UTF8")]
        string TargetName;

        [FieldOrder(1)]
        [SerializeAs(SerializedType.LengthPrefixedString)]
        [FieldEncoding("UTF8")]
        string title;

        L2Player target;
        int ObjectId;

        public override void Run(GameClient client)
        {
            ObjectId = Characters.Instance.GetObjectIdFromName(TargetName);
            target = L2World.Instance.FindPlayer(ObjectId);

            if (client.PlayerInstance.ObjectId == ObjectId &&
                client.PlayerInstance.isNoble == 1 )
            {
                if (title.Length <= 16)
                {
                    target.Title = title;
                    S_SystemMessage sm = new S_SystemMessage((int)SystemMessages.TITLE_CHANGED);
                    client.PacketProcessor.SendPacket(sm, PacketPriority.Normal);
                    L2World.Instance.BroadcastRegionAndArround(L2World.Instance.GetRegionFromPosition(target.Position).ToString(), new S_TitleUpdate(client.PlayerInstance));
                }
            }
        }
    }
}