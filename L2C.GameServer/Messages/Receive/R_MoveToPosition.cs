﻿using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_MoveToPosition : L2ReceiveBaseFrame
    {
        [FieldOrder(1)]
        private int targetX;
        [FieldOrder(2)]
        private int targetY;
        [FieldOrder(3)]
        private int targetZ;
        [FieldOrder(4)]
        private int originX;
        [FieldOrder(5)]
        private int originY;
        [FieldOrder(6)]
        private int originZ;
        [FieldOrder(7)]
        private int movementType;

        public override void Run(GameClient client)
        {
            if (client.PlayerInstance == null)
            {
                client.Disconnect();
                return;
            }

            client.PlayerInstance.Position.X = originX;
            client.PlayerInstance.Position.Y = originY;
            client.PlayerInstance.Position.Z = originZ;
            client.PlayerInstance.Position.X += 50;
            //client.PlayerInstance.AI.RaiseDesire(new Desire_Move(), new object[] {new L2Position(targetX, targetY, targetZ, client.PlayerInstance.Position.Heading)} );            
        }
    }
}
