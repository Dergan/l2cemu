﻿using System;
using L2C.GameServer.Database.Tables;
using L2C.GameServer.Handler;
using L2C.GameServer.Objects;
using L2C.GameServer.Network.GameServer.Packets.Send;
using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_BypassHandler : L2ReceiveBaseFrame
    {

        [FieldOrder(0)]
        [SerializeAs(SerializedType.LengthPrefixedString)]
        [FieldEncoding("UTF8")]
        private string _cmd;

        public override void Run(GameClient client)
        {
            Logger.Info("Received BypassHandler command: " + _cmd, Logging.Logger.LogType.Debug);

            if (_cmd.StartsWith("cmd_") && client.PlayerInstance.isGM)
            {
                Type pck = ProcessCommand.Processcommand(_cmd.Split(' ')[0], client.PlayerInstance);
                if (pck != null)
                {
                    L2BasePacket sbp = (L2BasePacket)Activator.CreateInstance(pck, client, _cmd);
                    client.PacketProcessor.SendPacket(sbp, PacketPriority.Normal);
                    return;
                }
            }

            if (_cmd.StartsWith("npc_"))
            {
                try
                {
                    if (_cmd.Split('_')[2].ToLower().StartsWith("quest"))
                    {
                        L2Quest quest = Quests.Instance.GetQuest(Convert.ToInt32(_cmd.Split('_')[1]));
                        if(!client.PlayerInstance.Questslist._Quests.ContainsKey((short)quest.ID))
                            quest.BeginQuest(client.PlayerInstance, quest);
                        else
                            quest.DoingQuest(client.PlayerInstance, quest);
                    }
                    else if (_cmd.Split('_')[2].ToLower().StartsWith("chat") && client.PlayerInstance.Target != null)
                    {
                        if(client.PlayerInstance.Target.GetType() == typeof(L2Npc))
                        {
                            S_HtmlMessage html = new S_HtmlMessage((L2Npc)client.PlayerInstance.Target, 0);
                            html.LoadHTML(Convert.ToInt16(_cmd.Split('_')[2].Substring(4)));
                            client.PacketProcessor.SendPacket(html, PacketPriority.Normal);
                        }
                    }

                }
                catch
                {
                    //hlapex...
                    client.Disconnect();
                    Logger.Info("Possible cheater tried to play around with the BypassHandler!", Logging.Logger.LogType.Cheater);
                }
            }

            if (_cmd.StartsWith("html_"))
            {
                try
                {
                    switch (_cmd.Substring(5))
                    {
                        case "communityboard":
                            break;
                    }
                }
                catch
                {
                    //hlapex...
                    client.Disconnect();
                    Logger.Info("Possible cheater tried to play around with the BypassHandler!", Logging.Logger.LogType.Cheater);
                }
            }

            switch (_cmd)
            {
                //CommunityBoard commands
                case "_bbsgetfav":
                    break;
                case "_bbslink":
                    break;
                case "_bbsloc":
                    break;
                case "_bbsclan":
                    break;
                case "_bbsmemo":
                    break;
                case "_maillist_0_1_0_":
                    break;
                case "_friendlist_0_":
                    break;
            }
        }
    }
}