﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Network.GameServer.Packets.Send;
using L2C.GameServer.Objects;
using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    class R_DropItem : L2ReceiveBaseFrame
    {

        [FieldOrder(0)]
        private int ObjectId;
        [FieldOrder(1)]
        private long Count;
        [FieldOrder(2)]
        private int X;
        [FieldOrder(3)]
        private int Y;
        [FieldOrder(4)]
        private int Z;

        public override void Run(GameClient client)
        {
            L2Item item = client.PlayerInstance.Inventory.GetItem(ObjectId);

            if(client.PlayerInstance.isFishing == 1 ||
               client.PlayerInstance.isDead ||
               item == null ||
               Formulas.Distance2D(client.PlayerInstance.Position.X, client.PlayerInstance.Position.Y, X, Y) > 150)
            {
                client.PacketProcessor.SendPacket(new S_ActionFailed(), PacketPriority.Normal);
                return;
            }



            client.PacketProcessor.SendPacket(new S_DropItem(client.PlayerInstance, item, X, Y, Z, Count), PacketPriority.Normal);
        }
    }
}