﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.AI.Desire;
using L2C.GameServer.Objects;
using L2C.GameServer.Database.Tables;
using L2C.GameServer.Network.GameServer.Packets.Send;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_UseSkill : L2ReceiveBaseFrame
    {
        private int _skillId;
        private bool ControlPressed;
        private bool UnkPressed;
        private bool Unk2Pressed;
        private bool Unk3Pressed;
        private bool ShiftPressed;

        public override void Run(GameClient client)
        {
            L2Skill skill = client.PlayerInstance.SkillList.GetById(_skillId);

            if (skill.ID == 0 || skill.Level <= 0 ||
                client.PlayerInstance.isCasting == true ||
                !client.PlayerInstance.SkillList.Contains(skill))
            {
                client.PacketProcessor.SendPacket(new S_ActionFailed(), PacketPriority.Normal);
                return;
            }

            //let the AI handle this...
            client.PlayerInstance.AI.RaiseDesire(new Desire_Cast(), new object[] { skill });
        }
    }
}