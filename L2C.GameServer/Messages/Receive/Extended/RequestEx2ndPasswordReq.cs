﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Receive.Extended
{
    class RequestEx2ndPasswordReq : ExtendedReceiveBaseFrame
    {
        [FieldOrder(0)]
        public byte unk0 { get; set; }

        [FieldOrder(1)]
        public SerializedString pin { get; set; }

        [FieldOrder(2)]
        public SerializedString unkStr { get; set; }
    }
}
