﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Receive.Extended
{
    class RequestCharacterNameCreatable : ExtendedReceiveBaseFrame
    {
        [FieldOrder(0)]
        public SerializedString Name { get; set; }
    }
}
