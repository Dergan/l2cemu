﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Receive.Extended
{
    class RequestEx2ndPasswordVerify : ExtendedReceiveBaseFrame
    {

        [FieldOrder(0)]
        public SerializedString pin { get; set; }
    }
}
