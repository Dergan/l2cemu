﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Receive
{
    class RequestLogin : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        public SerializedString Username { get; set; }

        [FieldOrder(1)]
        public ulong PlayHash { get; set; }

        [FieldOrder(2)]
        public ulong LoginHash { get; set; }

        [FieldOrder(3)]
        public uint Localization { get; set; }
    }
}
