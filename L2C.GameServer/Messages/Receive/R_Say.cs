﻿namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public enum ChatType
    {
        ALL = 0,
        SHOUT = 1,
        TELL = 2,
        PARTY = 3,
        CLAN = 4,
        GM = 5,
        PETITION_PLAYER = 6,
        PETITION_GM = 7,
        TRADE = 8, //+
        ALLIANCE = 9, //$
        ANNOUNCEMENT = 10,
        BOAT = 11,
        L2FRIEND = 12,
        MSNCHAT = 13,
        PARTYMATCH_ROOM = 14,
        Party_COMMANDER = 15, //(Yellow)
        Party_ALL = 16, //(Red)
        HERO_VOICE = 17,
        CRITICAL_ANNOUNCE = 18,
        SCREEN_ANNOUNCE = 19,
        BATTLEFIELD = 20,
        MPCC_ROOM = 21
    }

    public class R_Say : L2ReceiveBaseFrame
    {
        private string _text;
        private ChatType _type;
        private string _target;

        public override void Run(GameClient client)
        {
            //Logger.Info(client.PlayerInstance.Name + " said: " + _text, Logging.Logger.LogType.Debug);

            if (_text == null || _text.Length > 105)
            {
                //Logger.Info("Player " + client.PlayerInstance.Name + ", said nothing... with message length: " + _text.Length, Logging.Logger.LogType.Warning);
                client.Disconnect();
                return;
            }

            switch (_type)
            {
                case ChatType.ALL:
                    //L2World.Instance.BroadcastRegion(L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString(), new S_Say(new L2Say(client.PlayerInstance, (int)_type, client.PlayerInstance.Name, _text)));
                    break;
                case ChatType.SHOUT:
                case ChatType.TRADE:
                    //L2World.Instance.BroadcastRegionAndArround(L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString(), new S_Say(new L2Say(client.PlayerInstance, (int)_type, client.PlayerInstance.Name, _text)));
                    break;
                case ChatType.GM:
                case ChatType.PETITION_GM:
                case ChatType.ANNOUNCEMENT:
                case ChatType.CRITICAL_ANNOUNCE:
                case ChatType.SCREEN_ANNOUNCE:
                    if(!client.PlayerInstance.isGM)
                        client.Disconnect();
                    break;
                case ChatType.PARTY:
                    //if(client.PlayerInstance.Party != null)
                    //    client.PlayerInstance.Party.Broadcast(new S_Say(new L2Say(client.PlayerInstance, (int)_type, client.PlayerInstance.Name, _text)), PacketPriority.Normal);
                    break;
                case ChatType.CLAN:
                    //if(client.PlayerInstance.clan != null)
                    //    client.PlayerInstance.clan.Broadcast(new S_Say(new L2Say(client.PlayerInstance, (int)_type, client.PlayerInstance.Name, _text)), PacketPriority.Normal, 0);
                    break;
            }
        }
    }
}