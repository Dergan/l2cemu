﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Receive
{
    class RequestGameStart : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        public byte CharSlot { get; set; }

        [FieldOrder(1)]
        [SerializeWhen(nameof(CharSlot), 0xAD)]
        public byte CharSlotEx { get; set; }
    }
}
