﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Receive
{
    class SendProtocolVersion : ReceiveBaseFrame
    {

        [FieldOrder(0)]
        public int Version { get; set; }
    }
}
