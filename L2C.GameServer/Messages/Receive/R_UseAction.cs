﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Network.GameServer.Packets.Send;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_UseAction : L2ReceiveBaseFrame
    {
        private int _ActionId;
        private bool _CtrlPressed;
        private bool _UnkPressed;
        private bool _Unk1Pressed;
        private bool _Unk2Pressed;
        private bool _ShiftPressed;

        public override void Run(GameClient client)
        {
            switch (_ActionId)
            {
                case 12:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Hello, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 13:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Victory, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 14:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Charge, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 24:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Yes, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 25:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.No, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 26:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Bow, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 29:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Unaware, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 30:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Waiting, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 31:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Laugh, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 33:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Applause, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 34:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Dance, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 35:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Sad, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 62:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Charm, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 66:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.Shyness, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 71:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.TwoPersonBow, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 72:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.HiFive, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
                case 73:
                    client.PacketProcessor.SendPacket(new S_SocialAction(S_SocialAction.SocialActions.CoupleDance, client.PlayerInstance.ObjectId), PacketPriority.Normal);
                    break;
            }
        }
    }
}