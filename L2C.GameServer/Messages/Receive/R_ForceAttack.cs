﻿using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    class R_ForceAttack : L2ReceiveBaseFrame
    {
        [FieldOrder(0)]
        private int ObjectId;
        [FieldOrder(1)]
        private int X;
        [FieldOrder(2)]
        private int Y;
        [FieldOrder(3)]
        private int Z;
        [FieldOrder(4)]
        private byte AttackType;

        public override void Run(GameClient client)
        {
            //object[] FoundObject = L2World.Instance.FindObject(ObjectId);

            //if((string)FoundObject[0] == "L2Npc")
            //    client.PlayerInstance.AI.RaiseDesire(new Desire_Attack(), L2World.Instance.FindObject(ObjectId));
        }
    }
}
