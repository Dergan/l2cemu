﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Receive
{
    class RequestEnterWorld : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        [FieldCount(5)]
        public uint[] Tracert { get; set; }

        [FieldOrder(1)]
        [FieldCount(21)]
        public uint[] GG { get; set; }
    }
}