﻿using L2C.GameServer.Network.GameServer.Packets.Send;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_GotoLobby : L2ReceiveBaseFrame
    {

        public override void Run(GameClient client)
        {
            client.PacketProcessor.SendPacket(new S_CharSelectInfo(client), PacketPriority.Normal);
        }
    }
}