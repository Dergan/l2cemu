﻿using BinarySerialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_BypassUserCommand : L2ReceiveBaseFrame
    {

        [FieldOrder(0)]
        [SerializeAs(SerializedType.LengthPrefixedString)]
        [FieldEncoding("UTF8")]
        string command;

        public override void Run(GameClient client)
        {
            Logger.Info("[Bypass user command]: Command: " + command, Logging.Logger.LogType.Debug);
        }
    }
}