﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Network.GameServer.Packets.Send;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_LeaveParty : L2ReceiveBaseFrame
    {

        public override void Run(GameClient client)
        {
            if (client.PlayerInstance.Party != null)
                client.PlayerInstance.Party.Remove(client.PlayerInstance);
        }
    }
}