﻿using L2C.GameServer.Network.GameServer.Packets.Send;
using L2C.GameServer.World;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    public class R_EnterWorld : L2ReceiveBaseFrame
    {

        public override void Run(GameClient client)
        {
                        
            //Database.Tables.Inventory.Instance.GetItems(client);
            //Database.Tables.Skills.Instance.GetSkills(client.PlayerInstance);
            //client.PacketProcessor.SendPacket(new S_SkillList(client.PlayerInstance.SkillList), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_GameGuardQuery(), PacketPriority.Normal);

            //client.PacketProcessor.SendPacket(new S_SocialActionList(), PacketPriority.Normal);
            //Database.Tables.Shortcuts.Instance.GetShortcuts(client.PlayerInstance);
            //client.PacketProcessor.SendPacket(new S_ShortCuts(client.PlayerInstance), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_SystemMessage(34), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_SystemMessage("This server is developed by DragonHunter and Boeman"), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_SystemMessage("Please do not remove the credits, show some respect ;)"), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_SystemMessage("http://www.l2c-emu.com"), PacketPriority.Normal);
            
            client.PlayerInstance.Spawn();
            //client.PlayerInstance.AI.AITimer.Enabled = true;
            
            client.PlayerInstance.prevPosition.X = client.PlayerInstance.Position.X;
            client.PlayerInstance.prevPosition.Y = client.PlayerInstance.Position.Y;
            client.PlayerInstance.prevPosition.Z = client.PlayerInstance.Position.Z;
            client.PlayerInstance.prevPosition.Region = L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString();


            client.PacketProcessor.SendPacket(new S_UserInfo_New(client.PlayerInstance), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_ExUserInfoInvenWeight(client.PlayerInstance), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_ExUserInfoEquipSlot(client.PlayerInstance), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_ExUserInfoCubic(client.PlayerInstance), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_ExUserInfoAbnormalVisualEffect(client.PlayerInstance), PacketPriority.Normal);
            
            //client.PacketProcessor.SendPacket(new S_CharInfo(client.PlayerInstance), PacketPriority.Normal);
            //client.PacketProcessor.SendPacket(new S_ExBrUserInfo(client.PlayerInstance), PacketPriority.Normal);

            //Clan Information
            /*client.PlayerInstance.clan = Clans.Instance.GetClanId(client.PlayerInstance.clan.ClanId);
            if (client.PlayerInstance.clan != null)
            {
                client.PlayerInstance.clan.members[client.PlayerInstance.ObjectId].player = client.PlayerInstance;
                client.PacketProcessor.SendPacket(new S_ClanShowMemberListAll(client.PlayerInstance.clan), PacketPriority.Normal);

                //let other players know we are online
                S_SystemMessage sm = new S_SystemMessage(SystemMessages.CLAN_MEMBER_S1_LOGGED_IN, client.PlayerInstance.Name);
                client.PlayerInstance.clan.Broadcast(sm, PacketPriority.Normal, client.PlayerInstance.ObjectId);
                client.PlayerInstance.clan.Broadcast(new S_ClanMemberUpdate(client.PlayerInstance, true), PacketPriority.Normal, client.PlayerInstance.ObjectId);
            }*/

            //send players
            /*L2Player[] players = L2World.Instance.GetPlayersFromRegionAndArround(L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString());
            for (int i = 0; i < players.Length; i++)
            {
                players[i].KnownList.Add(client.PlayerInstance.ObjectId);
                client.PlayerInstance.KnownList.Add(players[i].ObjectId);

                L2World.Instance.BroadcastRegionAndArround(L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString(), new S_CharInfo(players[i]));
            }*/

            //send npc's
            //L2Npc[] npcs = L2World.Instance.GetNpcsFromRegionAndArround(L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString());
            //for (int i = 0; i < npcs.Length; i++)
            //{
            //    client.PacketProcessor.SendPacket(new S_NpcInfo(npcs[i]), PacketPriority.Lowest);
            //    client.PlayerInstance.KnownList.Add(npcs[i].ObjectId);
            //}


            //some memory cleaning
            //players = null;
            //npcs = null;

            //L2World.Instance.NpcAIHandler(L2World.Instance.GetRegionFromPosition(client.PlayerInstance.Position).ToString());
        }
    }
}