﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Network.GameServer.Packets.Send;
using L2C.GameServer.Objects;
using L2C.GameServer.Database.Tables;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    class R_UseItem : L2ReceiveBaseFrame
    {
        private int ObjId;

        public override void Run(GameClient client)
        {
            L2Item item = client.PlayerInstance.Inventory.GetItem(ObjId);
            if(item != null)
                client.PlayerInstance.Inventory.UseItem(client.PlayerInstance, item);
        }
    }
}