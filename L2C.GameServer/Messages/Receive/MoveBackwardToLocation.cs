﻿using BinarySerialization;
using L2C.GameServer.Messages.SerializableTypes;

namespace L2C.GameServer.Messages.Receive
{
    class MoveBackwardToLocation : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        public Vector3 Destination { get; set; } = new Vector3();

        [FieldOrder(1)]
        public Vector3 Current { get; set; } = new Vector3();

        [FieldOrder(2)]
        public uint ControllerType { get; set; }
    }
}