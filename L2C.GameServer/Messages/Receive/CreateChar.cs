﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Receive
{
    class CreateChar : ReceiveBaseFrame
    {
        [FieldOrder(0)]
        private string _CharName;
        [FieldOrder(1)]
        private int _raceId;
        [FieldOrder(2)]
        private int _sex;
        [FieldOrder(3)]
        private int _classId;
        [FieldOrder(4)]
        private int _int;
        [FieldOrder(5)]
        private int _str;
        [FieldOrder(6)]
        private int _con;
        [FieldOrder(7)]
        private int _men;
        [FieldOrder(8)]
        private int _dex;
        [FieldOrder(9)]
        private int _wit;
        [FieldOrder(10)]
        private byte _hairStyle;
        [FieldOrder(11)]
        private byte _hairColor;
        [FieldOrder(12)]
        private byte _face;

        //public override void Run(GameClient client)
        //{
        //    //Check here the CharName
        //    /*if(Characters.Instance.CharacterExist(_CharName) == false)
        //    {

        //    }*/

        //    uint ID = IDFactory.Instance.GenerateID();
        //    Logger.Info("CharCreate ID: " + ID);
        //    Logger.Info("CharCreate Create: " + _CharName);
        //    Logger.Info("CharCreate raceId: " + _raceId);
        //    Logger.Info("CharCreate sex: " + _sex);
        //    Logger.Info("CharCreate classId: " + _classId);
        //    Logger.Info("CharCreate int: " + _int);
        //    Logger.Info("CharCreate str: " + _str);
        //    Logger.Info("CharCreate con: " + _con);
        //    Logger.Info("CharCreate men: " + _men);
        //    Logger.Info("CharCreate dex: " + _dex);
        //    Logger.Info("CharCreate wit: " + _wit);
        //    Logger.Info("CharCreate hairStyle: " + _hairStyle);
        //    Logger.Info("CharCreate hairColor: " + _hairColor);
        //    Logger.Info("CharCreate face: " + _face);

        //    //Add player to database
        //    client.PlayerList.Add(new L2Player(ID, 1, _CharName, "", -71338, 258271, -3104, 100, 100, 100, 0, 0, 0, 0, 0, _sex, _classId, _hairStyle, _hairColor, _face, 0, false, Convert.ToInt16(client.PlayerList.Count + 1), 9.0f, 23.0f, 1, 0, 0, client, 0, _raceId));
        //    //client.PacketProcessor.SendPacket(new S_NewCharCreated() , PacketPriority.Normal);
        //}
    }
}