﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Database.Tables;
using L2C.GameServer.Objects;
using L2C.GameServer.World;
using L2C.GameServer.Network.GameServer.Packets.Send;
using BinarySerialization;

namespace L2C.GameServer.Network.GameServer.Packets.Receive
{
    class R_ClanMemberInfo : L2ReceiveBaseFrame
    {
        [FieldOrder(0)]
        private int unk;

        [FieldOrder(1)]
        [SerializeAs(SerializedType.LengthPrefixedString)]
        [FieldEncoding("UTF8")]
        private string name;

        public override void Run(GameClient client)
        {
            int ObjectId = Characters.Instance.GetObjectIdFromName(name);
            if (ObjectId != 0)
            {
                L2Player player = L2World.Instance.FindPlayer(ObjectId);
                if (player != null)
                {
                    client.PacketProcessor.SendPacket(new S_ClanMemberInfo(player), PacketPriority.Normal);
                }
            }
        }
    }
}