﻿using BinarySerialization;
using L2C.GameServer.Messages.Send.Extended;

namespace L2C.GameServer.Messages
{
    class ExtendedSendBaseMessage : SendBaseFrame
    {
        [FieldOrder(0)]
        public ushort ExtendedOpcode { get; set; }

        [FieldOrder(1)]
        //[Subtype(nameof(ExtendedOpcode), 0x01, typeof(ExRegenMax))]
        //[Subtype(nameof(ExtendedOpcode), 0x02, typeof(ExEventMatchUserInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x03, typeof(ExColosseumFenceInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x04, typeof(ExEventMatchSpelledInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x05, typeof(ExEventMatchFirecracker))]
        //[Subtype(nameof(ExtendedOpcode), 0x06, typeof(ExEventMatchTeamUnlocked))]
        //[Subtype(nameof(ExtendedOpcode), 0x07, typeof(ExEventMatchGMTest))]
        //[Subtype(nameof(ExtendedOpcode), 0x08, typeof(ExPartyRoomMember))]
        //[Subtype(nameof(ExtendedOpcode), 0x09, typeof(ExClosePartyRoom))]
        //[Subtype(nameof(ExtendedOpcode), 0x0A, typeof(ExManagePartyRoomMember))]
        //[Subtype(nameof(ExtendedOpcode), 0x0B, typeof(ExEventMatchLockResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x0C, typeof(ExAutoSoulShot))]
        //[Subtype(nameof(ExtendedOpcode), 0x0D, typeof(ExEventMatchList))]
        //[Subtype(nameof(ExtendedOpcode), 0x0E, typeof(ExEventMatchObserver))]
        //[Subtype(nameof(ExtendedOpcode), 0x0F, typeof(ExEventMatchMessage))]
        //[Subtype(nameof(ExtendedOpcode), 0x10, typeof(ExEventMatchScore))]
        //[Subtype(nameof(ExtendedOpcode), 0x11, typeof(ExServerPrimitive))]
        //[Subtype(nameof(ExtendedOpcode), 0x12, typeof(ExOpenMPCC))]
        //[Subtype(nameof(ExtendedOpcode), 0x13, typeof(ExCloseMPCC))]
        //[Subtype(nameof(ExtendedOpcode), 0x14, typeof(ExShowCastleInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x15, typeof(ExShowFortressInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x16, typeof(ExShowAgitInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x17, typeof(ExShowFortressSiegeInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x18, typeof(ExPartyPetWindowAdd))]
        //[Subtype(nameof(ExtendedOpcode), 0x19, typeof(ExPartyPetWindowUpdate))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A, typeof(ExAskJoinMPCC))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B, typeof(ExPledgeEmblem))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C, typeof(ExEventMatchTeamInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x1D, typeof(ExEventMatchCreate))]
        //[Subtype(nameof(ExtendedOpcode), 0x1E, typeof(ExFishingStart))]
        //[Subtype(nameof(ExtendedOpcode), 0x1F, typeof(ExFishingEnd))]
        //[Subtype(nameof(ExtendedOpcode), 0x20, typeof(ExShowQuestInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x21, typeof(ExShowQuestMark))]
        //[Subtype(nameof(ExtendedOpcode), 0x22, typeof(ExSendManorList))]
        //[Subtype(nameof(ExtendedOpcode), 0x23, typeof(ExShowSeedInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x24, typeof(ExShowCropInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x25, typeof(ExShowManorDefaultInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x26, typeof(ExShowSeedSetting))]
        //[Subtype(nameof(ExtendedOpcode), 0x27, typeof(ExFishingStartCombat))]
        //[Subtype(nameof(ExtendedOpcode), 0x28, typeof(ExFishingHpRegen))]
        //[Subtype(nameof(ExtendedOpcode), 0x29, typeof(ExEnchantSkillList))]
        //[Subtype(nameof(ExtendedOpcode), 0x2A, typeof(ExEnchantSkillInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x2B, typeof(ExShowCropSetting))]
        //[Subtype(nameof(ExtendedOpcode), 0x2C, typeof(ExShowSellCropList))]
        //[Subtype(nameof(ExtendedOpcode), 0x2D, typeof(ExOlympiadMatchEnd))]
        //[Subtype(nameof(ExtendedOpcode), 0x2E, typeof(ExMailArrived))]
        //[Subtype(nameof(ExtendedOpcode), 0x2F, typeof(ExStorageMaxCount))]
        //[Subtype(nameof(ExtendedOpcode), 0x30, typeof(ExEventMatchManage))]
        //[Subtype(nameof(ExtendedOpcode), 0x31, typeof(ExMultiPartyCommandChannelInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x32, typeof(ExPCCafePointInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x33, typeof(ExSetCompassZoneCode))]
        //[Subtype(nameof(ExtendedOpcode), 0x34, typeof(ExGetBossRecord))]
        //[Subtype(nameof(ExtendedOpcode), 0x35, typeof(ExAskJoinPartyRoom))]
        //[Subtype(nameof(ExtendedOpcode), 0x36, typeof(ExListPartyMatchingWaitingRoom))]
        //[Subtype(nameof(ExtendedOpcode), 0x37, typeof(ExSetMpccRouting))]
        //[Subtype(nameof(ExtendedOpcode), 0x38, typeof(ExShowAdventurerGuideBook))]
        //[Subtype(nameof(ExtendedOpcode), 0x39, typeof(ExShowScreenMessage))]
        //[Subtype(nameof(ExtendedOpcode), 0x3A, typeof(PledgeSkillList))]
        //[Subtype(nameof(ExtendedOpcode), 0x3B, typeof(PledgeSkillListAdd))]
        //[Subtype(nameof(ExtendedOpcode), 0x3C, typeof(PledgeSkillListRemove))]
        //[Subtype(nameof(ExtendedOpcode), 0x3D, typeof(PledgePowerGradeList))]
        //[Subtype(nameof(ExtendedOpcode), 0x3E, typeof(PledgeReceivePowerInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x3F, typeof(PledgeReceiveMemberInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x40, typeof(PledgeReceiveWarList))]
        //[Subtype(nameof(ExtendedOpcode), 0x41, typeof(PledgeReceiveSubPledgeCreated))]
        //[Subtype(nameof(ExtendedOpcode), 0x42, typeof(ExRedSky))]
        //[Subtype(nameof(ExtendedOpcode), 0x43, typeof(PledgeReceiveUpdatePower))]
        //[Subtype(nameof(ExtendedOpcode), 0x44, typeof(FlySelfDestination))]
        //[Subtype(nameof(ExtendedOpcode), 0x45, typeof(ShowPCCafeCouponShowUI))]
        //[Subtype(nameof(ExtendedOpcode), 0x46, typeof(ExSearchOrc))]
        //[Subtype(nameof(ExtendedOpcode), 0x47, typeof(ExCursedWeaponList))]
        //[Subtype(nameof(ExtendedOpcode), 0x48, typeof(ExCursedWeaponLocation))]
        //[Subtype(nameof(ExtendedOpcode), 0x49, typeof(ExRestartClient))]
        //[Subtype(nameof(ExtendedOpcode), 0x4A, typeof(ExRequestHackShield))]
        //[Subtype(nameof(ExtendedOpcode), 0x4B, typeof(ExUseSharedGroupItem))]
        //[Subtype(nameof(ExtendedOpcode), 0x4C, typeof(ExMPCCShowPartyMemberInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x4D, typeof(ExDuelAskStart))]
        //[Subtype(nameof(ExtendedOpcode), 0x4E, typeof(ExDuelReady))]
        //[Subtype(nameof(ExtendedOpcode), 0x4F, typeof(ExDuelStart))]
        //[Subtype(nameof(ExtendedOpcode), 0x50, typeof(ExDuelEnd))]
        //[Subtype(nameof(ExtendedOpcode), 0x51, typeof(ExDuelUpdateUserInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x52, typeof(ExShowVariationMakeWindow))]
        //[Subtype(nameof(ExtendedOpcode), 0x53, typeof(ExShowVariationCancelWindow))]
        //[Subtype(nameof(ExtendedOpcode), 0x54, typeof(ExPutItemResultForVariationMake))]
        //[Subtype(nameof(ExtendedOpcode), 0x55, typeof(ExPutIntensiveResultForVariationMake))]
        //[Subtype(nameof(ExtendedOpcode), 0x56, typeof(ExPutCommissionResultForVariationMake))]
        //[Subtype(nameof(ExtendedOpcode), 0x57, typeof(ExVariationResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x58, typeof(ExPutItemResultForVariationCancel))]
        //[Subtype(nameof(ExtendedOpcode), 0x59, typeof(ExVariationCancelResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x5A, typeof(ExDuelEnemyRelation))]
        //[Subtype(nameof(ExtendedOpcode), 0x5B, typeof(ExPlayAnimation))]
        //[Subtype(nameof(ExtendedOpcode), 0x5C, typeof(ExMPCCPartyInfoUpdate))]
        //[Subtype(nameof(ExtendedOpcode), 0x5D, typeof(ExPlayScene))]
        //[Subtype(nameof(ExtendedOpcode), 0x5E, typeof(ExSpawnEmitter))]
        //[Subtype(nameof(ExtendedOpcode), 0x5F, typeof(ExEnchantSkillInfoDetail))]
        //[Subtype(nameof(ExtendedOpcode), 0x60, typeof(ExBasicActionList))]
        //[Subtype(nameof(ExtendedOpcode), 0x61, typeof(ExAirShipInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x62, typeof(ExAttributeEnchantResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x63, typeof(ExChooseInventoryAttributeItem))]
        //[Subtype(nameof(ExtendedOpcode), 0x64, typeof(ExGetOnAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x65, typeof(ExGetOffAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x66, typeof(ExMoveToLocationAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x67, typeof(ExStopMoveAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x68, typeof(ExShowTrace))]
        //[Subtype(nameof(ExtendedOpcode), 0x69, typeof(ExItemAuctionInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x6A, typeof(ExNeedToChangeName))]
        //[Subtype(nameof(ExtendedOpcode), 0x6B, typeof(ExPartyPetWindowDelete))]
        //[Subtype(nameof(ExtendedOpcode), 0x6C, typeof(ExTutorialList))]
        //[Subtype(nameof(ExtendedOpcode), 0x6D, typeof(ExRpItemLink))]
        //[Subtype(nameof(ExtendedOpcode), 0x6E, typeof(ExMoveToLocationInAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x6F, typeof(ExStopMoveInAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x70, typeof(ExValidateLocationInAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x71, typeof(ExUISetting))]
        //[Subtype(nameof(ExtendedOpcode), 0x72, typeof(ExMoveToTargetInAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x73, typeof(ExAttackInAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x74, typeof(ExMagicSkillUseInAirShip))]
        //[Subtype(nameof(ExtendedOpcode), 0x75, typeof(ExShowBaseAttributeCancelWindow))]
        //[Subtype(nameof(ExtendedOpcode), 0x76, typeof(ExBaseAttributeCancelResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x77, typeof(ExSubPledgeSkillAdd))]
        //[Subtype(nameof(ExtendedOpcode), 0x78, typeof(ExResponseFreeServer))]
        //[Subtype(nameof(ExtendedOpcode), 0x79, typeof(ExShowProcureCropDetail))]
        //[Subtype(nameof(ExtendedOpcode), 0x7A, typeof(ExHeroList))]
        //[Subtype(nameof(ExtendedOpcode), 0x7B, typeof(ExOlympiadUserInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x7C, typeof(ExOlympiadSpelledInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x7D, typeof(ExOlympiadMode))]
        //[Subtype(nameof(ExtendedOpcode), 0x7E, typeof(ExShowFortressMapInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x7F, typeof(ExPVPMatchRecord))]
        //[Subtype(nameof(ExtendedOpcode), 0x80, typeof(ExPVPMatchUserDie))]
        //[Subtype(nameof(ExtendedOpcode), 0x81, typeof(ExPrivateStorePackageMsg))]
        //[Subtype(nameof(ExtendedOpcode), 0x82, typeof(ExPutEnchantTargetItemResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x83, typeof(ExPutEnchantSupportItemResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x84, typeof(ExRequestChangeNicknameColor))]
        //[Subtype(nameof(ExtendedOpcode), 0x85, typeof(ExGetBookMarkInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x86, typeof(ExNotifyPremiumItem))]
        //[Subtype(nameof(ExtendedOpcode), 0x87, typeof(ExGetPremiumItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0x88, typeof(ExPeriodicItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0x89, typeof(ExJumpToLocation))]
        //[Subtype(nameof(ExtendedOpcode), 0x8A, typeof(ExPVPMatchCCRecord))]
        //[Subtype(nameof(ExtendedOpcode), 0x8B, typeof(ExPVPMatchCCMyRecord))]
        //[Subtype(nameof(ExtendedOpcode), 0x8C, typeof(ExPVPMatchCCRetire))]
        //[Subtype(nameof(ExtendedOpcode), 0x8D, typeof(ExShowTerritory))]
        //[Subtype(nameof(ExtendedOpcode), 0x8E, typeof(ExNpcQuestHtmlMessage))]
        //[Subtype(nameof(ExtendedOpcode), 0x8F, typeof(ExSendUIEvent))]
        //[Subtype(nameof(ExtendedOpcode), 0x90, typeof(ExNotifyBirthDay))]
        //[Subtype(nameof(ExtendedOpcode), 0x91, typeof(ExShowDominionRegistry))]
        //[Subtype(nameof(ExtendedOpcode), 0x92, typeof(ExReplyRegisterDominion))]
        //[Subtype(nameof(ExtendedOpcode), 0x93, typeof(ExReplyDominionInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x94, typeof(ExShowOwnthingPos))]
        //[Subtype(nameof(ExtendedOpcode), 0x95, typeof(ExCleftList))]
        //[Subtype(nameof(ExtendedOpcode), 0x96, typeof(ExCleftState))]
        //[Subtype(nameof(ExtendedOpcode), 0x97, typeof(ExDominionChannelSet))]
        //[Subtype(nameof(ExtendedOpcode), 0x98, typeof(ExBlockUpSetList))]
        //[Subtype(nameof(ExtendedOpcode), 0x99, typeof(ExBlockUpSetState))]
        //[Subtype(nameof(ExtendedOpcode), 0x9A, typeof(ExStartScenePlayer))]
        //[Subtype(nameof(ExtendedOpcode), 0x9B, typeof(ExAirShipTeleportList))]
        //[Subtype(nameof(ExtendedOpcode), 0x9C, typeof(ExMpccRoomInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x9D, typeof(ExListMpccWaiting))]
        //[Subtype(nameof(ExtendedOpcode), 0x9E, typeof(ExDissmissMpccRoom))]
        //[Subtype(nameof(ExtendedOpcode), 0x9F, typeof(ExManageMpccRoomMember))]
        //[Subtype(nameof(ExtendedOpcode), 0xA0, typeof(ExMpccRoomMember))]
        //[Subtype(nameof(ExtendedOpcode), 0xA1, typeof(ExVitalityPointInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xA2, typeof(ExShowSeedMapInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xA3, typeof(ExMpccPartymasterList))]
        //[Subtype(nameof(ExtendedOpcode), 0xA4, typeof(ExDominionWarStart))]
        //[Subtype(nameof(ExtendedOpcode), 0xA5, typeof(ExDominionWarEnd))]
        //[Subtype(nameof(ExtendedOpcode), 0xA6, typeof(ExShowLines))]
        //[Subtype(nameof(ExtendedOpcode), 0xA7, typeof(ExPartyMemberRenamed))]
        //[Subtype(nameof(ExtendedOpcode), 0xA8, typeof(ExEnchantSkillResult))]
        //[Subtype(nameof(ExtendedOpcode), 0xA9, typeof(ExRefundList))]
        //[Subtype(nameof(ExtendedOpcode), 0xAA, typeof(ExNoticePostArrived))]
        //[Subtype(nameof(ExtendedOpcode), 0xAB, typeof(ExShowReceivedPostList))]
        //[Subtype(nameof(ExtendedOpcode), 0xAC, typeof(ExReplyReceivedPost))]
        //[Subtype(nameof(ExtendedOpcode), 0xAD, typeof(ExShowSentPostList))]
        //[Subtype(nameof(ExtendedOpcode), 0xAE, typeof(ExReplySentPost))]
        //[Subtype(nameof(ExtendedOpcode), 0xAF, typeof(ExResponseShowStepOne))]
        //[Subtype(nameof(ExtendedOpcode), 0xB0, typeof(ExResponseShowStepTwo))]
        //[Subtype(nameof(ExtendedOpcode), 0xB1, typeof(ExResponseShowContents))]
        //[Subtype(nameof(ExtendedOpcode), 0xB2, typeof(ExShowPetitionHtml))]
        //[Subtype(nameof(ExtendedOpcode), 0xB3, typeof(ExReplyPostItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0xB4, typeof(ExChangePostState))]
        //[Subtype(nameof(ExtendedOpcode), 0xB5, typeof(ExNoticePostSent))]
        //[Subtype(nameof(ExtendedOpcode), 0xB6, typeof(ExInitializeSeed))]
        //[Subtype(nameof(ExtendedOpcode), 0xB7, typeof(ExRaidReserveResult))]
        //[Subtype(nameof(ExtendedOpcode), 0xB8, typeof(ExBuySellList))]
        //[Subtype(nameof(ExtendedOpcode), 0xB9, typeof(ExCloseRaidSocket))]
        //[Subtype(nameof(ExtendedOpcode), 0xBA, typeof(ExPrivateMarketList))]
        //[Subtype(nameof(ExtendedOpcode), 0xBB, typeof(ExRaidCharacterSelected))]
        //[Subtype(nameof(ExtendedOpcode), 0xBC, typeof(ExAskCoupleAction))]
        //[Subtype(nameof(ExtendedOpcode), 0xBD, typeof(ExBrBroadcastEventState))]
        //[Subtype(nameof(ExtendedOpcode), 0xBE, typeof(ExBR_LoadEventTopRankers))]
        //[Subtype(nameof(ExtendedOpcode), 0xBF, typeof(ExChangeNpcState))]
        //[Subtype(nameof(ExtendedOpcode), 0xC0, typeof(ExAskModifyPartyLooting))]
        //[Subtype(nameof(ExtendedOpcode), 0xC1, typeof(ExSetPartyLooting))]
        //[Subtype(nameof(ExtendedOpcode), 0xC2, typeof(ExRotation))]
        //[Subtype(nameof(ExtendedOpcode), 0xC3, typeof(ExChangeClientEffectInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xC4, typeof(ExMembershipInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xC5, typeof(ExReplyHandOverPartyMaster))]
        //[Subtype(nameof(ExtendedOpcode), 0xC6, typeof(ExQuestNpcLogList))]
        //[Subtype(nameof(ExtendedOpcode), 0xC7, typeof(ExQuestItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0xC8, typeof(ExGMViewQuestItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0xC9, typeof(ExRestartResponse))]
        //[Subtype(nameof(ExtendedOpcode), 0xCA, typeof(ExVoteSystemInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xCB, typeof(ExShuttleInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xCC, typeof(ExSuttleGetOn))]
        //[Subtype(nameof(ExtendedOpcode), 0xCD, typeof(ExSuttleGetOff))]
        //[Subtype(nameof(ExtendedOpcode), 0xCE, typeof(ExSuttleMove))]
        //[Subtype(nameof(ExtendedOpcode), 0xCF, typeof(ExMoveToLocationInSuttle))]
        //[Subtype(nameof(ExtendedOpcode), 0xD0, typeof(ExStopMoveInShuttle))]
        //[Subtype(nameof(ExtendedOpcode), 0xD1, typeof(ExValidateLocationInShuttle))]
        //[Subtype(nameof(ExtendedOpcode), 0xD2, typeof(ExAgitAuctionCmd))]
        //[Subtype(nameof(ExtendedOpcode), 0xD3, typeof(ExConfirmAddingPostFriend))]
        //[Subtype(nameof(ExtendedOpcode), 0xD4, typeof(ExReceiveShowPostFriend))]
        //[Subtype(nameof(ExtendedOpcode), 0xD5, typeof(ExReceiveOlympiad))]
        //[Subtype(nameof(ExtendedOpcode), 0xD6, typeof(ExBR_GamePoint))]
        //[Subtype(nameof(ExtendedOpcode), 0xD7, typeof(ExBR_ProductList))]
        //[Subtype(nameof(ExtendedOpcode), 0xD8, typeof(ExBR_ProductInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xD9, typeof(ExBR_BuyProduct))]
        //[Subtype(nameof(ExtendedOpcode), 0xDA, typeof(ExBR_PremiumState))]
        //[Subtype(nameof(ExtendedOpcode), 0xDB, typeof(ExBrExtraUserInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xDC, typeof(ExBrBuffEventState))]
        //[Subtype(nameof(ExtendedOpcode), 0xDD, typeof(ExBR_RecentProductList))]
        //[Subtype(nameof(ExtendedOpcode), 0xDE, typeof(ExBR_MinigameLoadScores))]
        //[Subtype(nameof(ExtendedOpcode), 0xDF, typeof(ExBR_AgathionEnergyInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xE0, typeof(ExShowChannelingEffect))]
        //[Subtype(nameof(ExtendedOpcode), 0xE1, typeof(ExGetCrystalizingEstimation))]
        //[Subtype(nameof(ExtendedOpcode), 0xE2, typeof(ExGetCrystalizingFail))]
        //[Subtype(nameof(ExtendedOpcode), 0xE3, typeof(ExNavitAdventPointInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xE4, typeof(ExNavitAdventEffect))]
        //[Subtype(nameof(ExtendedOpcode), 0xE5, typeof(ExNavitAdventTimeChange))]
        //[Subtype(nameof(ExtendedOpcode), 0xE6, typeof(ExAbnormalStatusUpdateFromTarget))]
        //[Subtype(nameof(ExtendedOpcode), 0xE7, typeof(ExStopScenePlayer))]
        //[Subtype(nameof(ExtendedOpcode), 0xE8, typeof(ExFlyMove))]
        //[Subtype(nameof(ExtendedOpcode), 0xE9, typeof(ExDynamicQuest))]
        //[Subtype(nameof(ExtendedOpcode), 0xEA, typeof(ExSubjobInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xEB, typeof(ExChangeMPCost))]
        //[Subtype(nameof(ExtendedOpcode), 0xEC, typeof(ExFriendDetailInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xED, typeof(ExBlockAddResult))]
        //[Subtype(nameof(ExtendedOpcode), 0xEE, typeof(ExBlockRemoveResult))]
        //[Subtype(nameof(ExtendedOpcode), 0xEF, typeof(ExBlockDetailInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xF0, typeof(ExLoadInzonePartyHistory))]
        //[Subtype(nameof(ExtendedOpcode), 0xF1, typeof(ExFriendNotifyNameChange))]
        //[Subtype(nameof(ExtendedOpcode), 0xF2, typeof(ExShowCommission))]
        //[Subtype(nameof(ExtendedOpcode), 0xF3, typeof(ExResponseCommissionItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0xF4, typeof(ExResponseCommissionInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xF5, typeof(ExResponseCommissionRegister))]
        //[Subtype(nameof(ExtendedOpcode), 0xF6, typeof(ExResponseCommissionDelete))]
        //[Subtype(nameof(ExtendedOpcode), 0xF7, typeof(ExResponseCommissionList))]
        //[Subtype(nameof(ExtendedOpcode), 0xF8, typeof(ExResponseCommissionBuyInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xF9, typeof(ExResponseCommissionBuyItem))]
        //[Subtype(nameof(ExtendedOpcode), 0xFA, typeof(AcquireSkillList))]
        //[Subtype(nameof(ExtendedOpcode), 0xFB, typeof(ExMagicAttackInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xFC, typeof(ExAcquireSkillInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0xFD, typeof(ExNewSkillToLearnByLevelUp))]
        //[Subtype(nameof(ExtendedOpcode), 0xFE, typeof(ExCallToChangeClass))]
        //[Subtype(nameof(ExtendedOpcode), 0xFF, typeof(ExChangeToAwakenedClass))]
        //[Subtype(nameof(ExtendedOpcode), 0x100, typeof(ExTacticalSign))]
        //[Subtype(nameof(ExtendedOpcode), 0x101, typeof(ExLoadStatWorldRank))]
        //[Subtype(nameof(ExtendedOpcode), 0x102, typeof(ExLoadStatUser))]
        //[Subtype(nameof(ExtendedOpcode), 0x103, typeof(ExLoadStatHotLink))]
        //[Subtype(nameof(ExtendedOpcode), 0x104, typeof(ExGetWebSessionID))]
        [Subtype(nameof(ExtendedOpcode), 0x105, typeof(Ex2ndPasswordCheck))]
        [Subtype(nameof(ExtendedOpcode), 0x106, typeof(Ex2ndPasswordVerify))]
        [Subtype(nameof(ExtendedOpcode), 0x107, typeof(Ex2ndPasswordAck))]
        //[Subtype(nameof(ExtendedOpcode), 0x108, typeof(ExFlyMoveBroadcast))]
        //[Subtype(nameof(ExtendedOpcode), 0x109, typeof(ExShowUsm))]
        //[Subtype(nameof(ExtendedOpcode), 0x10A, typeof(ExShowStatPage))]
        [Subtype(nameof(ExtendedOpcode), 0x10B, typeof(ExIsCharNameCreatable))]
        //[Subtype(nameof(ExtendedOpcode), 0x10C, typeof(ExGoodsInventoryChangedNotify))]
        //[Subtype(nameof(ExtendedOpcode), 0x10D, typeof(ExGoodsInventoryInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x10E, typeof(ExGoodsInventoryResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x10F, typeof(ExAlterSkillRequest))]
        //[Subtype(nameof(ExtendedOpcode), 0x110, typeof(ExNotifyFlyMoveStart))]
        //[Subtype(nameof(ExtendedOpcode), 0x111, typeof(ExDummy))]
        //[Subtype(nameof(ExtendedOpcode), 0x112, typeof(ExCloseCommission))]
        //[Subtype(nameof(ExtendedOpcode), 0x113, typeof(ExChangeAttributeItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0x114, typeof(ExChangeAttributeInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x115, typeof(ExChangeAttributeOk))]
        //[Subtype(nameof(ExtendedOpcode), 0x116, typeof(ExChangeAttributeFail))]
        //[Subtype(nameof(ExtendedOpcode), 0x117, typeof(ExLightingCandleEvent))]
        //[Subtype(nameof(ExtendedOpcode), 0x118, typeof(ExVitalityEffectInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x119, typeof(ExLoginVitalityEffectInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x11A, typeof(ExBR_PresentBuyProduct))]
        //[Subtype(nameof(ExtendedOpcode), 0x11B, typeof(ExMentorList))]
        //[Subtype(nameof(ExtendedOpcode), 0x11C, typeof(ExMentorAdd))]
        //[Subtype(nameof(ExtendedOpcode), 0x11D, typeof(ExMenteeListWaiting))]
        //[Subtype(nameof(ExtendedOpcode), 0x11E, typeof(ExInzoneWaitingInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x11F, typeof(ExCuriousHouseState))]
        //[Subtype(nameof(ExtendedOpcode), 0x120, typeof(ExCuriousHouseEnter))]
        //[Subtype(nameof(ExtendedOpcode), 0x121, typeof(ExCuriousHouseLeave))]
        //[Subtype(nameof(ExtendedOpcode), 0x122, typeof(ExCuriousHouseMemberList))]
        //[Subtype(nameof(ExtendedOpcode), 0x123, typeof(ExCuriousHouseMemberUpdate))]
        //[Subtype(nameof(ExtendedOpcode), 0x124, typeof(ExCuriousHouseRemainTime))]
        //[Subtype(nameof(ExtendedOpcode), 0x125, typeof(ExCuriousHouseResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x126, typeof(ExCuriousHouseObserveList))]
        //[Subtype(nameof(ExtendedOpcode), 0x127, typeof(ExCuriousHouseObserveMode))]
        //[Subtype(nameof(ExtendedOpcode), 0x128, typeof(ExSysstring))]
        //[Subtype(nameof(ExtendedOpcode), 0x129, typeof(ExChoose_Shape_Shifting_Item))]
        //[Subtype(nameof(ExtendedOpcode), 0x12A, typeof(ExPut_Shape_Shifting_Target_Item_Result))]
        //[Subtype(nameof(ExtendedOpcode), 0x12B, typeof(ExPut_Shape_Shifting_Extraction_Item_Result))]
        //[Subtype(nameof(ExtendedOpcode), 0x12C, typeof(ExShape_Shifting_Result))]
        //[Subtype(nameof(ExtendedOpcode), 0x12D, typeof(ExCastleState))]
        [Subtype(nameof(ExtendedOpcode), 0x12E, typeof(ExNCGuardReceiveDataFromServer))]
        //[Subtype(nameof(ExtendedOpcode), 0x12F, typeof(ExKalieEvent))]
        //[Subtype(nameof(ExtendedOpcode), 0x130, typeof(ExKalieEventJackpotUser))]
        //[Subtype(nameof(ExtendedOpcode), 0x131, typeof(ExAbnormalVisualEffectInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x132, typeof(ExNpcInfoSpeed))]
        //[Subtype(nameof(ExtendedOpcode), 0x133, typeof(ExSetPledgeEmblemAck))]
        //[Subtype(nameof(ExtendedOpcode), 0x134, typeof(ExShowBeautyMenu))]
        //[Subtype(nameof(ExtendedOpcode), 0x135, typeof(ExResponseBeautyList))]
        //[Subtype(nameof(ExtendedOpcode), 0x136, typeof(ExResponseBeautyRegistReset))]
        //[Subtype(nameof(ExtendedOpcode), 0x137, typeof(ExResponseResetList))]
        //[Subtype(nameof(ExtendedOpcode), 0x138, typeof(ExShuffleSeedAndPublicKey))]
        //[Subtype(nameof(ExtendedOpcode), 0x139, typeof(ExCheck_SpeedHack))]
        //[Subtype(nameof(ExtendedOpcode), 0x13A, typeof(ExBR_NewIConCashBtnWnd))]
        //[Subtype(nameof(ExtendedOpcode), 0x13B, typeof(ExEventCampaignInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x13C, typeof(ExUnReadMailCount))]
        //[Subtype(nameof(ExtendedOpcode), 0x13D, typeof(ExPledgeCount))]
        //[Subtype(nameof(ExtendedOpcode), 0x13E, typeof(ExAdenaInvenCount))]
        //[Subtype(nameof(ExtendedOpcode), 0x13F, typeof(ExPledgeRecruitInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x140, typeof(ExPledgeRecruitApplyInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x141, typeof(ExPledgeRecruitBoardSearch))]
        //[Subtype(nameof(ExtendedOpcode), 0x142, typeof(ExPledgeRecruitBoardDetail))]
        //[Subtype(nameof(ExtendedOpcode), 0x143, typeof(ExPledgeWaitingListApplied))]
        //[Subtype(nameof(ExtendedOpcode), 0x144, typeof(ExPledgeWaitingList))]
        //[Subtype(nameof(ExtendedOpcode), 0x145, typeof(ExPledgeWaitingUser))]
        //[Subtype(nameof(ExtendedOpcode), 0x146, typeof(ExPledgeDraftListSearch))]
        //[Subtype(nameof(ExtendedOpcode), 0x147, typeof(ExPledgeWaitingListAlarm))]
        //[Subtype(nameof(ExtendedOpcode), 0x148, typeof(ExValidateActiveCharacter))]
        //[Subtype(nameof(ExtendedOpcode), 0x149, typeof(ExCloseCommissionRegister))]
        //[Subtype(nameof(ExtendedOpcode), 0x14A, typeof(ExTeleportToLocationActivate))]
        //[Subtype(nameof(ExtendedOpcode), 0x14B, typeof(ExNotifyWebPetitionReplyAlarm))]
        //[Subtype(nameof(ExtendedOpcode), 0x14C, typeof(ExEventShowXMasWishCard))]
        //[Subtype(nameof(ExtendedOpcode), 0x14D, typeof(ExInvitation_Event_UI_Setting))]
        //[Subtype(nameof(ExtendedOpcode), 0x14E, typeof(ExInvitation_Event_Ink_Energy))]
        //[Subtype(nameof(ExtendedOpcode), 0x14F, typeof(ExCheckAbusing))]
        //[Subtype(nameof(ExtendedOpcode), 0x150, typeof(ExGMVitalityEffectInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x151, typeof(ExPathToAwakeningAlarm))]
        //[Subtype(nameof(ExtendedOpcode), 0x152, typeof(ExPutEnchantScrollItemResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x153, typeof(ExRemoveEnchantSupportItemResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x154, typeof(ExShowCardRewardList))]
        //[Subtype(nameof(ExtendedOpcode), 0x155, typeof(ExGmViewCharacterInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x156, typeof(ExUserInfoEquipSlot))]
        [Subtype(nameof(ExtendedOpcode), 0x157, typeof(ExUserInfoCubic))]
        [Subtype(nameof(ExtendedOpcode), 0x158, typeof(ExUserInfoAbnormalVisualEffect))]
        //[Subtype(nameof(ExtendedOpcode), 0x159, typeof(ExUserInfoFishing))]
        //[Subtype(nameof(ExtendedOpcode), 0x15A, typeof(ExPartySpelledInfoUpdate))]
        //[Subtype(nameof(ExtendedOpcode), 0x15B, typeof(ExDivideAdenaStart))]
        //[Subtype(nameof(ExtendedOpcode), 0x15C, typeof(ExDivideAdenaCancel))]
        //[Subtype(nameof(ExtendedOpcode), 0x15D, typeof(ExDivideAdenaDone))]
        //[Subtype(nameof(ExtendedOpcode), 0x15E, typeof(ExPetInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x15F, typeof(ExAcquireAPSkillList))]
        //[Subtype(nameof(ExtendedOpcode), 0x160, typeof(ExStartLuckyGame))]
        //[Subtype(nameof(ExtendedOpcode), 0x161, typeof(ExBettingLuckyGameResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x162, typeof(ExTrainingZone_Admission))]
        //[Subtype(nameof(ExtendedOpcode), 0x163, typeof(ExTrainingZone_Leaving))]
        //[Subtype(nameof(ExtendedOpcode), 0x164, typeof(ExPeriodicHenna))]
        //[Subtype(nameof(ExtendedOpcode), 0x165, typeof(ExShowAPListWnd))]
        [Subtype(nameof(ExtendedOpcode), 0x166, typeof(ExUserInfoInvenWeight))]
        //[Subtype(nameof(ExtendedOpcode), 0x167, typeof(ExCloseAPListWnd))]
        //[Subtype(nameof(ExtendedOpcode), 0x168, typeof(ExEnchantOneOK))]
        //[Subtype(nameof(ExtendedOpcode), 0x169, typeof(ExEnchantOneFail))]
        //[Subtype(nameof(ExtendedOpcode), 0x16A, typeof(ExEnchantOneRemoveOK))]
        //[Subtype(nameof(ExtendedOpcode), 0x16B, typeof(ExEnchantOneRemoveFail))]
        //[Subtype(nameof(ExtendedOpcode), 0x16C, typeof(ExEnchantTwoOK))]
        //[Subtype(nameof(ExtendedOpcode), 0x16D, typeof(ExEnchantTwoFail))]
        //[Subtype(nameof(ExtendedOpcode), 0x16E, typeof(ExEnchantTwoRemoveOK))]
        //[Subtype(nameof(ExtendedOpcode), 0x16F, typeof(ExEnchantTwoRemoveFail))]
        //[Subtype(nameof(ExtendedOpcode), 0x170, typeof(ExEnchantSucess))]
        //[Subtype(nameof(ExtendedOpcode), 0x171, typeof(ExEnchantFail))]
        //[Subtype(nameof(ExtendedOpcode), 0x172, typeof(ExEnchantRetryToPutItemOk))]
        //[Subtype(nameof(ExtendedOpcode), 0x173, typeof(ExEnchantRetryToPutItemFail))]
        //[Subtype(nameof(ExtendedOpcode), 0x174, typeof(ExAccountAttendanceInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x175, typeof(ExWorldChatCnt))]
        //[Subtype(nameof(ExtendedOpcode), 0x176, typeof(ExAlchemySkillList))]
        //[Subtype(nameof(ExtendedOpcode), 0x177, typeof(ExTryMixCube))]
        //[Subtype(nameof(ExtendedOpcode), 0x178, typeof(ExAlchemyConversion))]
        //[Subtype(nameof(ExtendedOpcode), 0x179, typeof(ExBeautyItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0x17A, typeof(ExReceiveClientINI))]
        //[Subtype(nameof(ExtendedOpcode), 0x17B, typeof(ExAutoFishAvailable))]
        //[Subtype(nameof(ExtendedOpcode), 0x17C, typeof(ExChannlChatEnterWorld))]
        //[Subtype(nameof(ExtendedOpcode), 0x17D, typeof(ExChannlChatPledgeInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x17E, typeof(ExVipAttendanceItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0x17F, typeof(ExConfirmVipAttendanceCheck))]
        //[Subtype(nameof(ExtendedOpcode), 0x180, typeof(ExShowEnsoulWindow))]
        //[Subtype(nameof(ExtendedOpcode), 0x181, typeof(ExEnsoulResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x182, typeof(ExMultiSellResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x183, typeof(ExCastleWarSeasonResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x184, typeof(ExCastleWarSeasonReward))]
        //[Subtype(nameof(ExtendedOpcode), 0x185, typeof(ReceiveVipProductList))]
        //[Subtype(nameof(ExtendedOpcode), 0x186, typeof(ReceiveVipLuckyGameInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x187, typeof(ReceiveVipLuckyGameItemList))]
        //[Subtype(nameof(ExtendedOpcode), 0x188, typeof(ReceiveVipLuckyGameResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x189, typeof(ReceiveVipInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x18A, typeof(ReceiveVipInfoRemainTime))]
        //[Subtype(nameof(ExtendedOpcode), 0x18B, typeof(ReceiveVipBotCaptchaImage))]
        //[Subtype(nameof(ExtendedOpcode), 0x18C, typeof(ReceiveVipBotCaptchaAnswerResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x18D, typeof(ExPledgeSigninForOpenJoiningMethod))]
        //[Subtype(nameof(ExtendedOpcode), 0x18E, typeof(ExRequestMatchArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x18F, typeof(ExCompleteMatchArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x190, typeof(ExConfirmMatchArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x191, typeof(ExCancelMatchArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x192, typeof(ExStartChooseClassArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x193, typeof(ExChangeClassArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x194, typeof(ExConfirmClassArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x195, typeof(ExStartBattleReadyArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x196, typeof(ExBattleReadyArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x197, typeof(ExDecoNPCInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x198, typeof(ExDecoNPCSet))]
        //[Subtype(nameof(ExtendedOpcode), 0x199, typeof(ExFactionInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x19A, typeof(ExBattleResultArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x19B, typeof(ExClosingArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x19C, typeof(ExClosedArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x19D, typeof(ExDieInArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x19F, typeof(ExArenaDashboard))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A0, typeof(ExArenaUpdateEquipSlot))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A1, typeof(ExArenaKillInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A2, typeof(ExExitArena))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A3, typeof(ExBalthusEvent))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A4, typeof(ExBalthusEventJackpotUser))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A5, typeof(ExPartyMatchingRoomHistory))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A6, typeof(ExAIContentUIEvent))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A7, typeof(ExArenaCustomNotification))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A8, typeof(ExOneDayReceiveRewardList))]
        //[Subtype(nameof(ExtendedOpcode), 0x1A9, typeof(ExConnectedTimeAndGetTableReward))]
        //[Subtype(nameof(ExtendedOpcode), 0x1AA, typeof(ExTodoListRecommend))]
        //[Subtype(nameof(ExtendedOpcode), 0x1AB, typeof(ExTodoListInzone))]
        //[Subtype(nameof(ExtendedOpcode), 0x1AC, typeof(ExTodoListHTML))]
        [Subtype(nameof(ExtendedOpcode), 0x1AD, typeof(ExQueueTicket))]
        //[Subtype(nameof(ExtendedOpcode), 0x1AE, typeof(ExPledgeBonusOpen))]
        //[Subtype(nameof(ExtendedOpcode), 0x1AF, typeof(ExPledgeBonusList))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B0, typeof(ExPledgeBonusMarkReset))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B1, typeof(ExPledgeBonusUpdate))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B2, typeof(ExSSOAuthnToken))]
        [Subtype(nameof(ExtendedOpcode), 0x1B3, typeof(ExQueueTicketLogin))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B4, typeof(ExEnSoulExtractionShow))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B5, typeof(ExEnSoulExtractionResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B6, typeof(ExFieldEventStep))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B7, typeof(ExFieldEventPoint))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B8, typeof(ExFieldEventEffect))]
        //[Subtype(nameof(ExtendedOpcode), 0x1B9, typeof(ExRaidBossSpawnInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x1BA, typeof(ExRaidServerInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x1BB, typeof(ExShowAgitSiegeInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x1BC, typeof(ExItemAuctionStatus))]
        //[Subtype(nameof(ExtendedOpcode), 0x1BD, typeof(ExMonsterBook))]
        //[Subtype(nameof(ExtendedOpcode), 0x1BE, typeof(ExMonsterBookRewardIcon))]
        //[Subtype(nameof(ExtendedOpcode), 0x1BF, typeof(ExMonsterBookOnFactionUI))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C0, typeof(ExMonsterBookOpenResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C1, typeof(ExMonsterBookCloseForce))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C2, typeof(ExFactionLevelUpNotify))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C3, typeof(ExItemAuctionNextInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C4, typeof(ExItemAuctionUpdatedBiddingInfo))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C5, typeof(ExPrivateStoreBuyingResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C6, typeof(ExPrivateStoreSellingResult))]
        [Subtype(nameof(ExtendedOpcode), 0x1C7, typeof(ExEnterWorld))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C8, typeof(ExMatchGroup))]
        //[Subtype(nameof(ExtendedOpcode), 0x1C9, typeof(ExMatchGroupAsk))]
        //[Subtype(nameof(ExtendedOpcode), 0x1CA, typeof(ExMatchGroupWithdraw))]
        //[Subtype(nameof(ExtendedOpcode), 0x1CB, typeof(ExMatchGroupOust))]
        //[Subtype(nameof(ExtendedOpcode), 0x1CC, typeof(ExArenaShowEnemyPartyLocation))]
        //[Subtype(nameof(ExtendedOpcode), 0x1CD, typeof(ExDressRoomUIOpen))]
        //[Subtype(nameof(ExtendedOpcode), 0x1CE, typeof(ExDressHangerList))]
        //[Subtype(nameof(ExtendedOpcode), 0x1CF, typeof(ExShowUpgradeSystem))]
        //[Subtype(nameof(ExtendedOpcode), 0x1D0, typeof(ExUpgradeSystemResult))]
        //[Subtype(nameof(ExtendedOpcode), 0x1D1, typeof(ExUserBanInfo))]
        public virtual ExtendedSendBaseFrame ExtendedFrame { get; set; }
    }
}
