﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class UserInfoContent : UIPacketContent
    {
        [FieldOrder(1)] [FieldBitLength(1)] public bool HasRoles { get; set; }
        [FieldOrder(2)] [FieldBitLength(1)] public bool HasBaseInfo { get; set; }
        [FieldOrder(3)] [FieldBitLength(1)] public bool HasModifierInfo { get; set; }
        [FieldOrder(4)] [FieldBitLength(1)] public bool HasMaxStats { get; set; }
        [FieldOrder(5)] [FieldBitLength(1)] public bool HasCurrentStats { get; set; }
        [FieldOrder(6)] [FieldBitLength(1)] public bool HasEnchantGlow { get; set; }
        [FieldOrder(7)] [FieldBitLength(1)] public bool HasHeadStyle { get; set; }
        [FieldOrder(8)] [FieldBitLength(1)] public bool HasStore { get; set; }

        [FieldOrder(9)] [FieldBitLength(1)] public bool HasBasicStats { get; set; }
        [FieldOrder(10)] [FieldBitLength(1)] public bool HasAttributes { get; set; }
        [FieldOrder(11)] [FieldBitLength(1)] public bool HasPosition { get; set; }
        [FieldOrder(12)] [FieldBitLength(1)] public bool HasMovementSpeed { get; set; }
        [FieldOrder(13)] [FieldBitLength(1)] public bool HasMovementMultiplier { get; set; }
        [FieldOrder(14)] [FieldBitLength(1)] public bool HasCollisionInfo { get; set; }
        [FieldOrder(15)] [FieldBitLength(1)] public bool HasAttackAttributeInfo { get; set; }
        [FieldOrder(16)] [FieldBitLength(1)] public bool HasClan { get; set; }

        [FieldOrder(17)] [FieldBitLength(1)] public bool HasSocialInfo { get; set; }
        [FieldOrder(18)] [FieldBitLength(1)] public bool HasFameInfo { get; set; }
        [FieldOrder(19)] [FieldBitLength(1)] public bool HasAccessoiry { get; set; }
        [FieldOrder(20)] [FieldBitLength(1)] public bool HasMovementInfo { get; set; }
        [FieldOrder(21)] [FieldBitLength(1)] public bool HasColorInfo { get; set; }
        [FieldOrder(22)] [FieldBitLength(1)] public bool HasMountInfo { get; set; }
        [FieldOrder(23)] [FieldBitLength(1)] public bool HasUnkParty { get; set; }
        [FieldOrder(24)] [FieldBitLength(1)] public bool HasUnk { get; set; }

        [FieldOrder(25)] [SerializeWhen(nameof(HasRoles), true)] public UserInfoUserRoles UserRoles { get; set; }
        [FieldOrder(26)] [SerializeWhen(nameof(HasBaseInfo), true)] public UserInfoPartWrapper<UserInfoBaseInfo> BaseInfo { get; set; }
        [FieldOrder(27)] [SerializeWhen(nameof(HasModifierInfo), true)] public UserInfoPartWrapper<UserInfoModifierInfo> ModifierInfo { get; set; }
        [FieldOrder(28)] [SerializeWhen(nameof(HasMaxStats), true)] public UserInfoPartWrapper<UserInfoMaxStats> MaxStats { get; set; }
        [FieldOrder(29)] [SerializeWhen(nameof(HasCurrentStats), true)] public UserInfoPartWrapper<UserInfoCurrentStats> CurStats { get; set; }
        [FieldOrder(30)] [SerializeWhen(nameof(HasEnchantGlow), true)] public UserInfoPartWrapper<UserInfoEnchantGlow> EnchantGlow { get; set; }
        [FieldOrder(31)] [SerializeWhen(nameof(HasHeadStyle), true)] public UserInfoPartWrapper<UserInfoHeadStyleInfo> HeadStyle { get; set; }
        [FieldOrder(32)] [SerializeWhen(nameof(HasStore), true)] public UserInfoPartWrapper<UserInfoStoreInfo> Store { get; set; }

        [FieldOrder(33)] [SerializeWhen(nameof(HasBasicStats), true)] public UserInfoPartWrapper<UserInfoBasicStats> BasicStats { get; set; }
        [FieldOrder(34)] [SerializeWhen(nameof(HasAttributes), true)] public UserInfoPartWrapper<UserInfoAttributeInfo> Attributes { get; set; }
        [FieldOrder(35)] [SerializeWhen(nameof(HasPosition), true)] public UserInfoPartWrapper<UserInfoPosition> Position { get; set; }
        [FieldOrder(36)] [SerializeWhen(nameof(HasMovementSpeed), true)] public UserInfoPartWrapper<UserInfoMovementSpeedInfo> MovementSpeed { get; set; }
        [FieldOrder(37)] [SerializeWhen(nameof(HasMovementMultiplier), true)] public UserInfoPartWrapper<UserInfoMovementMultiplierInfo> MovementMultiplier { get; set; }
        [FieldOrder(38)] [SerializeWhen(nameof(HasCollisionInfo), true)] public UserInfoPartWrapper<UserInfoCollisionInfo> CollisionInfo { get; set; }
        [FieldOrder(39)] [SerializeWhen(nameof(HasAttackAttributeInfo), true)] public UserInfoPartWrapper<UserInfoAttackAttributeInfo> AttackAttributeInfo { get; set; }
        [FieldOrder(40)] [SerializeWhen(nameof(HasClan), true)] public UserInfoPartWrapper<UserInfoClanInfo> Clan { get; set; }

        [FieldOrder(41)] [SerializeWhen(nameof(HasSocialInfo), true)] public UserInfoPartWrapper<UserInfoSocialInfo> SocialInfo { get; set; }
        [FieldOrder(42)] [SerializeWhen(nameof(HasFameInfo), true)] public UserInfoPartWrapper<UserInfoFameInfo> FameInfo { get; set; }
        [FieldOrder(43)] [SerializeWhen(nameof(HasAccessoiry), true)] public UserInfoPartWrapper<UserInfoAccessoiryInfo> Accessoiry { get; set; }
        [FieldOrder(44)] [SerializeWhen(nameof(HasMovementInfo), true)] public UserInfoPartWrapper<UserInfoMovementInfo> MovementInfo { get; set; }
        [FieldOrder(45)] [SerializeWhen(nameof(HasColorInfo), true)] public UserInfoPartWrapper<UserInfoColorInfo> ColorInfo { get; set; }
        [FieldOrder(46)] [SerializeWhen(nameof(HasMountInfo), true)] public UserInfoPartWrapper<UserInfoMountInfo> MountInfo { get; set; }
        [FieldOrder(47)] [SerializeWhen(nameof(HasUnkParty), true)] public UserInfoPartWrapper<UserInfoPartyInfo> UnkParty { get; set; }
        [FieldOrder(48)] [SerializeWhen(nameof(HasUnk), true)] public UserInfoPartWrapper<UserInfoUnkownInfo> Unkown { get; set; }
    }
}
