﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoCollisionInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Collision Info
        [FieldOrder(0)]
        public double CollisionRadius { get; set; }
        [FieldOrder(1)]
        public double CollisionHeight { get; set; }
    }
}
