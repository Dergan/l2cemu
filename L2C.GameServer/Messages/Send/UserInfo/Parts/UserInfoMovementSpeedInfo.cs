﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoMovementSpeedInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Movement Speed Info
        [FieldOrder(0)]
        public ushort RunSpeed { get; set; }
        [FieldOrder(1)]
        public ushort WalkSpeed { get; set; }
        [FieldOrder(2)]
        public ushort RunSpeedWater { get; set; }
        [FieldOrder(3)]
        public ushort WalkSpeedWater { get; set; }
        [FieldOrder(4)]
        public ushort RunSpeedElse { get; set; }
        [FieldOrder(5)]
        public ushort WalkSpeedElse { get; set; }
        [FieldOrder(6)]
        public ushort RunSpeedAir { get; set; }
        [FieldOrder(7)]
        public ushort WalkSpeedAir { get; set; }
    }
}
