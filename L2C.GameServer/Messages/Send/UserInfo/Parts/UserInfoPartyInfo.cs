﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoPartyInfo : UserInfoPart
    {
        [FieldOrder(0)]
        public uint Unk0 { get; set; }
        [FieldOrder(1)]
        public ushort Unk1 { get; set; }
        [FieldOrder(2)]
        public byte Unk2 { get; set; }
    }
}