﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoMovementInfo : UserInfoPart
    {
        [FieldOrder(0)]
        public bool Swimming { get; set; }
        [FieldOrder(1)]
        public byte MovementType { get; set; } = 1;
    }
}