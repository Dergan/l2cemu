﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoStoreInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Store Info
        [FieldOrder(0)]
        public byte MountType { get; set; } = 0;
        [FieldOrder(1)]
        public byte PrivateStoreType { get; set; }
        [FieldOrder(2)]
        public byte HasDwarvenCraft { get; set; }
        [FieldOrder(3)]
        public byte AbilityPoints { get; set; } = 0;
    }
}
