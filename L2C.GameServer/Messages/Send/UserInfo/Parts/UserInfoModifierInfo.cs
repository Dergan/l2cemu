﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoModifierInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Modifier Info
        [FieldOrder(0)]
        public ushort STR { get; set; }
        [FieldOrder(1)]
        public ushort DEX { get; set; }
        [FieldOrder(2)]
        public ushort CON { get; set; }
        [FieldOrder(3)]
        public ushort INT { get; set; }
        [FieldOrder(4)]
        public ushort WIT { get; set; }
        [FieldOrder(5)]
        public ushort MEN { get; set; }
        [FieldOrder(6)]
        public ushort LUC { get; set; }
        [FieldOrder(7)]
        public ushort CHA { get; set; }
    }
}
