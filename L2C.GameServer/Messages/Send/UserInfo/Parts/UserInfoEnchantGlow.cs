﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoEnchantGlow : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Mount Info
        [FieldOrder(0)]
        public byte WeaponEnchantGlow { get; set; }
        [FieldOrder(1)]
        public byte ArmorEnchantGlow { get; set; }
    }
}
