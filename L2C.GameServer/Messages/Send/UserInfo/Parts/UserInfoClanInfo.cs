﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoClanInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Title/Clan Info
        [FieldOrder(0)]
        public LengthPrefixedString Title { get; set; } = "";
        [FieldOrder(1)]
        public ushort PledgeType { get; set; }
        [FieldOrder(2)]
        public uint Clanid { get; set; }
        [FieldOrder(3)]
        public uint ClanCrestLargeId { get; set; }
        [FieldOrder(4)]
        public uint ClanCrestId { get; set; }
        [FieldOrder(5)]
        public uint ClanPrivilegesBitmask { get; set; }
        [FieldOrder(6)]
        public bool IsClanLeader { get; set; }
        [FieldOrder(7)]
        public uint AllyId { get; set; }
        [FieldOrder(8)]
        public uint AllyCrestId { get; set; }
        [FieldOrder(9)]
        public bool IsInPartyMatchRoom { get; set; }
    }
}
