﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoUserRoles : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Relations
        // 0x40 leader rights
        // siege flags: attacker - 0x180 sword over name, defender - 0x80 shield, 0xC0 crown (|leader), 0x1C0 flag (|leader)
        [FieldOrder(0)]
        public uint UserRoles { get; set; } = 0;
    }
}
