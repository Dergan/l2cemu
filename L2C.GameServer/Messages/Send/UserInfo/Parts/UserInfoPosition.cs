﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoPosition : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Position
        [FieldOrder(0)]
        public int X { get; set; }
        [FieldOrder(1)]
        public int Y { get; set; }
        [FieldOrder(2)]
        public int Z { get; set; }
        [FieldOrder(3)]
        public int VehicleObjectId { get; set; }
    }
}
