﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoMaxStats : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Max Stats
        [FieldOrder(0)]
        public uint MaxHP { get; set; }
        [FieldOrder(1)]
        public uint MaxMP { get; set; }
        [FieldOrder(2)]
        public uint MaxCP { get; set; }
    }
}
