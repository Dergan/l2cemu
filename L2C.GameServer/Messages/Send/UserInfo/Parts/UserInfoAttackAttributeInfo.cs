﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoAttackAttributeInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Attack Attribute Info
        [FieldOrder(0)]
        public byte AttackElementAttribute { get; set; }
        [FieldOrder(1)]
        public ushort AttackAttribute { get; set; }
    }
}
