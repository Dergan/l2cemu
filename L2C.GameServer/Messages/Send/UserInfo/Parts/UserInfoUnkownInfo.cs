﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoUnkownInfo : UserInfoPart
    {
        [FieldOrder(0)]
        public uint Unk1 { get; set; } = uint.MaxValue;
        [FieldOrder(1)]
        public uint Unk2 { get; set; }
        [FieldOrder(2)]
        public uint Unk3 { get; set; }
        [FieldOrder(3)]
        public uint Unk4 { get; set; }
        [FieldOrder(4)]
        public uint Unk5 { get; set; }
        [FieldOrder(5)]
        public uint Unk6 { get; set; }
    }
}
