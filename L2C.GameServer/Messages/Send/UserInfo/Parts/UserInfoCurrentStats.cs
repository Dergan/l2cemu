﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoCurrentStats : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Current Stats
        [FieldOrder(0)]
        public uint CurHP { get; set; }
        [FieldOrder(1)]
        public uint CurMP { get; set; }
        [FieldOrder(2)]
        public uint CurCP { get; set; }
        [FieldOrder(3)]
        public long SP { get; set; }
        [FieldOrder(4)]
        public long EXP { get; set; }
        [FieldOrder(5)]
        public double ExpPercent { get; set; }
    }
}
