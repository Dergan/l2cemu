﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoFameInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Fame Info
        [FieldOrder(0)]
        public uint Vitality { get; set; }
        [FieldOrder(1)]
        public byte Unk1 { get; set; }
        [FieldOrder(2)]
        public uint Fame { get; set; }
        [FieldOrder(3)]
        public uint RaidPoints { get; set; }
    }
}
