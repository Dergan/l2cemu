﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoSocialInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Social Info
        [FieldOrder(0)]
        public bool IsInPvp { get; set; }
        [FieldOrder(1)]
        public int Karma { get; set; }
        [FieldOrder(2)]
        public bool isNoble { get; set; }
        [FieldOrder(3)]
        public bool isHero { get; set; }
        [FieldOrder(4)]
        public byte PledgeClass { get; set; }
        [FieldOrder(5)]
        public uint PK { get; set; }
        [FieldOrder(6)]
        public uint PVP { get; set; }
        [FieldOrder(7)]
        public ushort RecomLeft { get; set; }
        [FieldOrder(8)]
        public ushort RecomHave { get; set; }
    }
}
