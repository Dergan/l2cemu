﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoAttributeInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Attribute Info
        [FieldOrder(0)]
        public ushort Fire { get; set; }
        [FieldOrder(1)]
        public ushort Water { get; set; }
        [FieldOrder(2)]
        public ushort Wind { get; set; }
        [FieldOrder(3)]
        public ushort Earth { get; set; }
        [FieldOrder(4)]
        public ushort Holy { get; set; }
        [FieldOrder(5)]
        public ushort Dark { get; set; }
    }
}
