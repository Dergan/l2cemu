﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoMountInfo : UserInfoPart
    {
        [FieldOrder(0)]
        public uint Mount { get; set; }
        [FieldOrder(1)]
        public ushort InventorySlots { get; set; }
        [FieldOrder(2)]
        public byte HideTitle { get; set; }
    }
}