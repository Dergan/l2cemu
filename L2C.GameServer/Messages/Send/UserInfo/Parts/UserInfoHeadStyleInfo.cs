﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoHeadStyleInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Head Style Info
        [FieldOrder(0)]
        public int HairStyle { get; set; }
        [FieldOrder(1)]
        public int HairColor { get; set; }
        [FieldOrder(2)]
        public int Face { get; set; }
        [FieldOrder(3)]
        public bool ShowHairAccessory { get; set; } = true;
    }
}
