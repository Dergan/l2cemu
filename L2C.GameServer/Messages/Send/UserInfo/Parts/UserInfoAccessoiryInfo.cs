﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoAccessoiryInfo : UserInfoPart
    {
        [FieldOrder(0)]
        public byte TalismanSlots { get; set; }
        [FieldOrder(1)]
        public bool HideHairAccessory { get; set; }
        [FieldOrder(2)]
        public bool CloakSlot { get; set; }
        [FieldOrder(3)]
        public byte DuelTeam { get; set; }
        [FieldOrder(4)]
        public uint UnkownTeam { get; set; }
        [FieldOrder(5)]
        public ushort Unkown { get; set; }
    }
}