﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoBasicStats : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Basic Stats
        [FieldOrder(0)]
        public ushort ActiveWeaponType { get; set; }//Active Weapon Type 0 = None / 80 = Pole / 500 = Bow / 400 = Crossbow / 40 = Else
        [FieldOrder(1)]
        public uint Patk { get; set; }
        [FieldOrder(2)]
        public uint AttackSpeed { get; set; }
        [FieldOrder(3)]
        public uint Pdef { get; set; }
        [FieldOrder(4)]
        public uint Evasion { get; set; }
        [FieldOrder(5)]
        public uint Accuracy { get; set; }
        [FieldOrder(6)]
        public uint Critical { get; set; }
        [FieldOrder(7)]
        public uint MAtk { get; set; }
        [FieldOrder(8)]
        public uint CastSpeed { get; set; }
        [FieldOrder(9)]
        public uint AttackSpeed2 { get; set; }
        [FieldOrder(10)]
        public uint MEvasion { get; set; }
        [FieldOrder(11)]
        public uint Mdef { get; set; }
        [FieldOrder(12)]
        public uint MAccuracy { get; set; }
        [FieldOrder(13)]
        public uint MCritical { get; set; }
    }
}
