﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoMovementMultiplierInfo : UserInfoPart
    {
        /////////////////////////////////////////////////////////////////Movement Multiplier Info
        [FieldOrder(0)]
        public double MovementMultiplier { get; set; }
        [FieldOrder(1)]
        public double AttackMultiplier { get; set; }
    }
}
