﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoColorInfo : UserInfoPart
    {
        [FieldOrder(0)]
        public uint NameColor { get; set; }
        [FieldOrder(1)]
        public uint TitleColor { get; set; }
    }
}