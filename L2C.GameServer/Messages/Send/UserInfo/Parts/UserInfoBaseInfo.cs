﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    public class UserInfoBaseInfo : UserInfoPart
    {
        [FieldOrder(0)]
        public LengthPrefixedString Name { get; set; }
        [FieldOrder(1)]
        public byte BuilderLevel { get; set; }
        [FieldOrder(2)]
        public byte RaceId { get; set; }
        [FieldOrder(3)]
        public byte Gender { get; set; }
        [FieldOrder(4)]
        public uint VisibleClassId { get; set; }
        [FieldOrder(5)]
        public uint ClassId { get; set; }
        [FieldOrder(6)]
        public byte Level { get; set; }
    }
}
