﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class FieldLengthInclusiveConverter : IValueConverter
    {
        public object Convert(object value, object parameter, BinarySerializationContext context)
        {
            var val = System.Convert.ToUInt16(value);
            return val - 2;
        }

        public object ConvertBack(object value, object parameter, BinarySerializationContext context)
        {
            var val = System.Convert.ToUInt16(value);
            return val + 2;
        }
    }

    public class UserInfoPartWrapper<TPart>
        where TPart : UserInfoPart
    {

        [FieldOrder(0)]
        public ushort PartLength { get; set; }

        [FieldOrder(1)]
        [FieldLength(nameof(PartLength), ConverterType = typeof(FieldLengthInclusiveConverter))]
        //[SubtypeFactory("Flags", typeof(UserInfoPartFactory), RelativeSourceMode = RelativeSourceMode.FindAncestor, AncestorType = typeof(UserInfoContent))]
        public TPart Part { get; set; }


        public UserInfoPartWrapper(TPart part)
        {
            Part = part;
        }

        public static implicit operator UserInfoPartWrapper<TPart>(TPart val)
        {
            return new UserInfoPartWrapper<TPart>(val);
        }
        public static implicit operator TPart(UserInfoPartWrapper<TPart> val)
        {
            return val.Part;
        }
    }
}
