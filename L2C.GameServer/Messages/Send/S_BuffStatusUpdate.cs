﻿namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_BuffStatusUpdate : L2SendBaseFrame
    {
        public uint Id { get; set; }
        public uint Level { get; set; }
        public uint Duration { get; set; }

        //public override void Run(GameClient client, params object[] parameters)
        //{
        //}
    }
}