﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_PetWindowAdd : SendBasePacket
    {
        L2Player plr;
        L2Npc Pet;
        public S_PetWindowAdd(L2Player player, L2Npc Pet)
            : base()
        {
            plr = player;
            this.Pet = Pet;
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteShort(0x18);
            WriteInteger(Pet.ObjectId);
            WriteInteger(Pet.NpcId + 1000000);
            WriteInteger(0);//type
            WriteInteger(plr.ObjectId);
            WriteString(Pet.Name);
            WriteInteger((int)Pet.CurHP);
            WriteInteger((int)Pet.MaxHP);
            WriteInteger((int)Pet.CurMP);
            WriteInteger((int)Pet.MaxMP);
            WriteInteger(Pet.Level);
        }
    }
}
