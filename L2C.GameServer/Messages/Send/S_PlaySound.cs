﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_PlaySound : SendBasePacket
    {
        L2Player player;
        string Sound;
        public S_PlaySound(L2Player player, string Sound)
            : base()
        {
            this.player = player;
            this.Sound = Sound;
        }

        protected internal override void Write()
        {
            WriteByte(0x9E);
            WriteInteger(player.isOnShip ? 1 : 0);
            WriteString(Sound);
            WriteInteger(player.isOnShip ? 1 : 0);
            WriteInteger(player.isOnShip ? 1 : 0);
            WriteInteger(player.Position.X);
            WriteInteger(player.Position.Y);
            WriteInteger(player.Position.Z);
            WriteInteger(0);
        }
    }
}