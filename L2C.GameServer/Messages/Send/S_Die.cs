﻿namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_Die : L2SendBaseFrame
    {
        public uint ObjectId { get; set; }
        public uint CanTeleport { get; set; }
        public uint CanGoHideOut { get; set; }
        public uint CanGoCastle { get; set; }
        public uint CanGoSiegeHQ { get; set; }
        public uint CanGoSweepable { get; set; }
        public uint CanFixedRessurect { get; set; }
        public uint CanGoFortress { get; set; }
    }
}