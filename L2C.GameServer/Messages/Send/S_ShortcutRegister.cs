﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;
using L2C.Game.Collections;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ShortcutRegister : SendBasePacket
    {
        private L2ShortcutInfo shortcut;
        public S_ShortcutRegister(L2ShortcutInfo shortcut)
            : base()
        {
            this.shortcut = shortcut;
        }

        protected internal override void Write()
        {
            WriteByte(0x44);

            WriteInteger(shortcut.type); //type
            WriteInteger(shortcut.slot + shortcut.page * 12);

            switch(shortcut.type) //type
            {
                case 1:
                    //item
                    WriteInteger(shortcut.id); //id
                    WriteInteger(shortcut.charType); //entity type
                    WriteInteger(0); //getSharedReuseGroup
                    WriteInteger(0);
                    WriteInteger(0);
                    WriteInteger(0);//augment id
                    break;
                case 2: //skill
                    WriteInteger(shortcut.id); //id
                    WriteInteger(shortcut.level); //level
                    WriteByte(0);
                    WriteInteger(shortcut.charType); //entity type
                    break;
                default:
                    WriteInteger(shortcut.id);
                    WriteInteger(shortcut.type);
                    break;
            }

            //default:
			//{
			//	writeD(_shortcut.getId());
			//	writeD(_shortcut.getCharacterType());
			//}
        }
    }
}