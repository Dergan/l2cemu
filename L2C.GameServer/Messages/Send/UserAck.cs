﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class UserAck : SendBaseFrame
    {
        [FieldOrder(0)]
        public uint ObjectId { get; set; }
        [FieldOrder(1)]
        public uint PK { get; set; }
        [FieldOrder(2)]
        public uint UnkId { get; set; }
    }
}
