﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_CharInfo : L2SendBaseFrame
    {
        private L2Player chr;

        public S_CharInfo(L2Player character)
            : base()
        {
            chr = character;
        }

        protected internal override void Write()
        {
            WriteByte(0x31);
            WriteInteger(chr.Position.X);
            WriteInteger(chr.Position.Y);
            WriteInteger(chr.Position.Z);
            WriteInteger(0); //vehicleId
            WriteInteger(chr.ObjectId); //ObjectId
            WriteString(chr.Name);
            WriteInteger(chr.RaceId); //Race
            WriteInteger(chr.Gender); //Sex
            WriteInteger(chr.ClassId); //ClassId

            //Item Id's
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.UNDERWEAR).ID);
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.HEAD).ID); //HEAD
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).ID); //RHAND
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND).ID); //LHAND
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.GLOVES).ID); //GLOVES
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST).ID); //CHEST
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.LEGS).ID); //LEGS
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.FEET).ID); //FEET
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.BACK).ID); //BACK
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.LR_HAND).ID); //LRHAND
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR).ID); //HAIR
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR2).ID); //HAIR2


            //Write Inventory Items AugmentIDs
            //for (int i = 0; i < 21; i++)
            //WriteInteger(0);

            WriteInteger(0x00);//Paperdoll_Rhand
            WriteInteger(0x00);//Paperdoll_LHand
            WriteInteger(0x00);//Paperdoll_lrhand
            WriteByte(0x00);//Talismanslots
            
            //NEED TO FIX THE DECO'S
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO1
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO2
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO3
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO4
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO5
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO6
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.BELT).ID); //BELT
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.BELT).ID); //BELT
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.BELT).ID); //BELT

            WriteByte((byte)chr.PvpFlag); //0-non-pvp  1-pvp = violett name
            WriteInteger(-chr.Karma); //Karma

            WriteInteger(chr.CastSpeed); //Matk Spd
            WriteInteger(chr.AttackSpeed); //PAtk Spd

            //WriteInteger(0); //??

            //need to fix soon
            WriteInteger(chr.RunSpeed); //RunSpd
            WriteInteger(chr.RunSpeed); //WalkSpd
            WriteInteger(chr.RunSpeed); // swimRunspeed
            WriteInteger(chr.RunSpeed); // swimWalkspeed
            WriteInteger(chr.RunSpeed); //?? Speed
            WriteInteger(chr.RunSpeed); //?? Speed
            WriteInteger(chr.RunSpeed); //FlyRunSpeed
            WriteInteger(chr.RunSpeed); //FlyWalkSpeed
            WriteDouble(1); //MoveMultiplier
            WriteDouble(1); //AttackSpeedMultiplier

            WriteDouble(chr.CollisionRadius); //Collision Radius
            WriteDouble(chr.CollisionHeight); //Collision Height

            WriteInteger(chr.HairStyle); //HairStyle
            WriteInteger(chr.HairColor); //HairColor
            WriteInteger(chr.Face); //Face

            WriteString(chr.Title); //Title
            if (chr.clan != null)
            {
                WriteInteger(chr.clan.ClanId); // ClanId
                WriteInteger(chr.clan.CrestId); // ClanCrestId
                WriteInteger(chr.clan.AllianceId); //AllyId
                WriteInteger(chr.clan.AllianceCrestId); // ally crest id
            }
            else
            {
                WriteInteger(0);
                WriteInteger(0);
                WriteInteger(0);
                WriteInteger(0);
            }

            // In UserInfo leader rights and siege flags, but here found nothing??
            // Therefore RelationChanged packet with that info is required
            //WriteInteger(0);

            WriteByte(1); // standing = 1  sitting = 0
            WriteByte(1); // running = 1   walking = 0
            WriteByte(0); //InCombat 0 = false 1 = true

            WriteByte((byte)(chr.isDead ? 1 : 0)); //LikeDead 0 = false 1 = true

            WriteByte(0); // invisible = 1  visible =0

            WriteByte(0); // 1-on Strider, 2-on Wyvern, 3-on Great Wolf, 0-no mount
            WriteByte(0); // 1 - sellshop

            WriteShort((short)chr.CubicList.Count); //Cubic Count
            for (int i = 0; i < chr.CubicList.Count; i++)
                WriteShort((short)chr.CubicList[i].type);

            WriteByte(0); // find party members
            //WriteInteger(0); //Abnormal Effect
            WriteByte(0); //1 = Swimming / 2 = Flying / 0 = Walking

            WriteShort(chr.RecomHave); //Recom Have
            WriteInteger(0); //Mount NpcId + 1000000
            WriteInteger(chr.ClassId); //ClassId
            WriteInteger(0); //
            WriteByte(0); //IsMounted? Or Enchant Effect

            WriteByte(0); //Team

            WriteInteger(0); //Clan Crest Large Id
            WriteByte(chr.isNoble); //IsNobless
            WriteByte(chr.isHero); //IsHero

            WriteByte(0); //0x01: Fishing Mode (Cant be undone by setting back to 0)
            WriteInteger(0); //fishing x
            WriteInteger(0); //fishing y
            WriteInteger(0); //fishing z

            WriteInteger(0xFFFFFF); //Name Color

            WriteInteger(chr.Position.Heading);//Heading

            WriteInteger(chr.PledgeClass); //PledgeClass
            WriteInteger(chr.PledgeType); //PledgeType

            WriteInteger(0xFF0000); //Title Color

            WriteInteger(0); //Level Cursed Weapon ??
            WriteInteger(0); //Reputation Score

            // T1
            WriteInteger(0); //Transformation ID
            WriteInteger(0); //Agathion ID

            // T2
            WriteByte(1); //??

            //T2.3
            //WriteInteger(0); //Special Effect
            WriteInteger((int)chr.CurCP);
            WriteInteger((int)chr.MaxHP);
            WriteInteger((int)chr.CurHP);
            WriteInteger((int)chr.MaxMP);
            WriteInteger((int)chr.CurMP);
            WriteByte(0);
            WriteInteger(0);//Effect List?
            WriteByte(0);//??
            WriteByte(1);//??
            WriteByte(0);//??
        }
    }
}