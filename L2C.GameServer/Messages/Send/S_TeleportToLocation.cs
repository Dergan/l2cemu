﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.World;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_TeleportToLocation : L2SendBaseFrame
    {
        public uint ObjectId { get; set; }
        public uint X { get; set; }
        public uint Y { get; set; }
        public uint Z { get; set; }
        public uint Unk1 { get; set; }
        public uint Heading { get; set; }        
    }
}