﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send.Extended
{
    class Ex2ndPasswordAck : ExtendedSendBaseFrame
    {
        [FieldOrder(0)]
        public uint Unk2 { get; } = 0;
    }
}