﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send.Extended
{
    class ExQueueTicket : ExtendedSendBaseFrame
    {
        [FieldOrder(0)]
        public bool Unk1 { get; } = true;
        [FieldOrder(1)]
        public uint Unk2 { get; } = 0;
    }
}