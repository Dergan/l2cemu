﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send.Extended
{
    class ExIsCharNameCreatable : ExtendedSendBaseFrame
    {
        [FieldOrder(0)]
        public uint Result { get; set; }
    }
}
