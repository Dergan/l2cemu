﻿namespace L2C.GameServer.Messages.Send.Extended
{
    class ExUserInfoInvenWeight : ExtendedSendBaseFrame
    {

        public uint ObjectId { get; set; }
        public uint CurrentWeight { get; set; }
        public uint MaximumWeight { get; set; }
    }
}
