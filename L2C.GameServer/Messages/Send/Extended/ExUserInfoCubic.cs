﻿using System.Collections.Generic;
using BinarySerialization;

namespace L2C.GameServer.Messages.Send.Extended
{
    class ExUserInfoCubic : ExtendedSendBaseFrame
    {
        [FieldOrder(0)]
        public uint ObjectId { get; set; }

        [FieldOrder(1)]
        public ushort CubicCount { get; set; }

        [FieldOrder(2)]
        [FieldCount(nameof(CubicCount))]
        public List<ushort> Cubics { get; set; } = new List<ushort>();

        [FieldOrder(3)]
        public uint Agathion { get; set; }
    }
}
