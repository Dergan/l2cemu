﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send.Extended
{
    class Ex2ndPasswordVerify : ExtendedSendBaseFrame
    {
        [FieldOrder(0)]
        public byte Unk2 { get; } = 0;

        [FieldOrder(1)]
        public uint Unk3 { get; } = 0;

        [FieldOrder(2)]
        public uint Unk4 { get; } = 0;
    }
}