﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send.Extended
{
    class ExQueueTicketLogin : ExtendedSendBaseFrame
    {
        [FieldOrder(0)]
        public bool Unk1 { get; } = true;
    }
}