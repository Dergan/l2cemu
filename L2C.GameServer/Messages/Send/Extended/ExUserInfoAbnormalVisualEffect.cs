﻿using System.Collections.Generic;
using BinarySerialization;

namespace L2C.GameServer.Messages.Send.Extended
{
    class ExUserInfoAbnormalVisualEffect : ExtendedSendBaseFrame
    {

        [FieldOrder(0)]
        public uint ObjectId { get; set; }

        [FieldOrder(1)]
        public uint Transformation { get; set; }

        [FieldOrder(2)]
        public uint VisualEffectsCount { get; set; }

        [FieldOrder(3)]
        [FieldCount(nameof(VisualEffectsCount))]
        public List<ushort> VisualEffects { get; set; } = new List<ushort>();
    }
}
