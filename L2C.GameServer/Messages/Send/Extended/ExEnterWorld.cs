﻿using System;

namespace L2C.GameServer.Messages.Send.Extended
{
    class ExEnterWorld : ExtendedSendBaseFrame
    {

        public uint SecondsSinceEpoch { get; set; } = (uint)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds;
    }
}
