﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send.Extended
{
    class ExNCGuardReceiveDataFromServer : ExtendedSendBaseFrame
    {
        [FieldOrder(0)]
        public uint Size { get; set; }

        [FieldOrder(1)]
        [FieldLength(nameof(Size))]
        public byte[] Data { get; set; }
    }
}
