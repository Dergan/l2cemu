﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send.Extended
{
    class Ex2ndPasswordCheck : ExtendedSendBaseFrame
    {
        [FieldOrder(0)]
        public uint Unk2 { get; } = 1;

        [FieldOrder(1)]
        public uint Unk3 { get; } = 0;
    }
}