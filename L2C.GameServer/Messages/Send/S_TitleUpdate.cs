﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_TitleUpdate : SendBasePacket
    {
        private L2Player player;
        public S_TitleUpdate(L2Player player)
            : base()
        {
            this.player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0xCC);
            WriteInteger(player.ObjectId);
            WriteString(player.Title);
        }
    }
}