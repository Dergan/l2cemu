﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_InventoryUpdate : SendBasePacket
    {
        List<ItemInfo> itemList;

        public void AddItem(L2Item item, Modifier modifier)
        {
            itemList.Add(new ItemInfo(item, modifier));
        }

        public enum Modifier
        {
            add = 1,
            modify = 2,
            delete = 3
        }

        private class ItemInfo
        {
            public L2Item item;
            public int modifier;

            public ItemInfo(L2Item item, Modifier modifier)
            {
                this.item = item;
                this.modifier = (int)modifier;
            }
        }
        
        public S_InventoryUpdate()
            : base()
        {
            itemList = new List<ItemInfo>();
        }

        protected internal override void Write()
        {
            WriteByte(0x21); //PacketId

            short itemcount = 0;
            foreach(ItemInfo item in itemList)
            {
                if(item.item.ItemObjId != 0)
                    itemcount++;
            }

            WriteShort(itemcount);

            foreach(ItemInfo item in itemList)
            {
                if(item.item.ItemObjId != 0)
                {
                    WriteShort((short)item.modifier); //1=add, 2=modify, 3=remove
                    WriteByte(0);
                    WriteInteger(item.item.ItemObjId);//ObjectId
                    WriteInteger(item.item.ID); //item id
                    WriteByte((byte)(item.item.isEquipped ? 0 : 0xFF));// location slot
                    WriteLong(item.item.ItemCount); //count
                    WriteByte((byte)item.item.itemType2); // item type1
                    WriteByte(0);
                    WriteShort((short)(item.item.isEquipped ? 1 : 0)); //is equipped ? 1 : 0
                    WriteLong((long)item.item.BodyPart); // item type2
                    WriteShort(0);//Enchant -127 - 127 or Pet level
                    WriteUInteger(0xFFFFFFFF); //Mana
                    WriteUInteger(0xFFFFD8F1); //Time

                    WriteByte(1); //Enable/Diable
                }
            }
        }
    }
}
