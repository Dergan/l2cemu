﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;
using L2C.Game.World;
using L2C.Game.Database.Tables;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_NpcInfo : SendBasePacket
    {
        private L2Npc _npc;
        public S_NpcInfo(L2Npc npc)
            : base()
        {
            _npc = npc;
        }

        protected internal override void Write()
        {
            WriteByte(0x0c);
            WriteInteger(_npc.ObjectId);
			WriteByte(0x00);
			WriteByte(0x25);
			WriteByte(0x00);
            WriteByte(0xED);

            //Flag
            if (_npc.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND) != null ||
               _npc.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST) != null ||
               _npc.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND) != null)
            {
                //if rhand, chest, lhand > 0 0xFE else 0xBE
                WriteByte(0xFE);
            }
            else
                WriteByte(0xBE);

            WriteByte(0x4E);
            WriteByte(0xA2);
            WriteByte(0x0C);

            //WriteByte((byte)(7 + (_npc.Title.Length * 2)));
            //WriteShort(0);
            //WriteShort(0);
            //WriteString(_npc.Title);
            WriteByte(7);
            WriteByte((byte)_npc.isAttackable);//isAttackable
            WriteShort(0);
            WriteShort(0);
            WriteString("");

            if (_npc.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND) != null ||
               _npc.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST) != null ||
               _npc.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND) != null)
            {
                //if rhand, chest, lhand > 0 68 else 56
                WriteShort(68);
            }
            else
                WriteShort(56);

            WriteInteger(_npc.NpcId + 1000000); // npctype id
            WriteInteger(_npc.Position.X);
            WriteInteger(_npc.Position.Y);
            WriteInteger(_npc.Position.Z);
            WriteInteger(_npc.Position.Heading);
            WriteInteger(_npc.MAtk);
            WriteInteger(_npc.Patk);
            //WriteFloat((float)_npc.MovementMultiplier);
            //WriteFloat((float)_npc.PatkSpdMultiplier);
            WriteFloat(1.0f);
            WriteFloat(1.0f);
            if(_npc.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND) != null || 
               _npc.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST)  != null || 
               _npc.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND) != null)
            {
                WriteInteger(_npc.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).ItemObjId);
                WriteInteger(_npc.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST).ItemObjId);
                WriteInteger(_npc.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND).ItemObjId);
            }
            WriteByte(1);
            WriteByte((byte)(_npc.isRunning ? 1 : 0));
            WriteByte(0);//Land = 0 / Water = 1 / Flying = 2
            WriteInteger(0);//IsFlying
            WriteByte(0);
            WriteByte(0);
            WriteShort(0);
            WriteInteger((int)_npc.CurHP);
            WriteInteger((int)_npc.MaxHP);

            WriteByte((byte)(
                (byte)(_npc.isFighting ? 1 : 0) +
                (byte)(_npc.isDead ? 2 : 0) +
                4 + //(byte)(_npc.isTargetable ? 4 : 0) +
                8//(byte)(_npc.isShowName ? 8 : 0)
                ));

            //Do Effectlist
            WriteShort(0);//Length of list
            //WriteShort(0);//EffectId
        }
    }
}