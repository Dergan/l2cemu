﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_CloseParty : SendBasePacket
    {
        public S_CloseParty()
            : base()
        {
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteShort(0x09);
        }
    }
}