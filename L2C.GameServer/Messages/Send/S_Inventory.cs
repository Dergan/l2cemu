﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_Inventory : SendBasePacket
    {
        L2Player plr;
        public S_Inventory(L2Player player)
            : base()
        {
            plr = player;
        }

        protected internal override void Write()
        {
            WriteByte(0x11);
            WriteShort(1); //show window
            WriteShort((short)(plr.Inventory.ItemCount));//item count

            foreach (L2Item item in plr.Inventory.ItemList.Values)
            {
                string name = item.ItemName;

                WriteByte(0);
                WriteInteger(item.ItemObjId);//ObjectId
                WriteInteger(item.ID); //item id
                WriteByte((byte)(item.isEquipped ? 0 : 0xFF));// location slot
                WriteLong(item.ItemCount); //count
                WriteByte((byte)item.itemType2); // item type1
                WriteByte(0);
                WriteShort((short)(item.isEquipped ? 1 : 0)); //is equipped ? 1 : 0
                WriteLong((long)item.BodyPart); // item type2
                WriteShort(0);//Enchant -127 - 127 or Pet level
                WriteUInteger(0xFFFFFFFF); //Mana
                WriteUInteger(0xFFFFD8F1); //Time

                WriteByte(1); //Enable/Diable

                //if (containsMask(mask, ItemListType.AUGMENT_BONUS))
                //{
                //    writeD(item.getAugmentationBonus());
                //}
                //if (containsMask(mask, ItemListType.ELEMENTAL_ATTRIBUTE))
                //{
                //    writeItemElemental(item);
                //}
                //if (containsMask(mask, ItemListType.ENCHANT_EFFECT))
                //{
                //    writeItemEnchantEffect(item);
                //}
                //if (containsMask(mask, ItemListType.VISUAL_ID))
                //{
                //    writeD(item.getVisualId()); // Item remodel visual ID
                //}
            }

            //if (inventory.hasInventoryBlock())
            //{
            //    writeH(inventory.getBlockItems().length);
            //    writeC(inventory.getBlockMode());
            //    for (int i : inventory.getBlockItems())
            //    {
            //        writeD(i);
            //    }
            //}
            //else
            //{
            WriteShort(0);
            //}
        }
    }
}