﻿
using System;
using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class CharacterSelected : SendBaseFrame
    {
        [FieldOrder(0)] public SerializedString Name { get; set; }
        [FieldOrder(1)] public uint ObjectId { get; set; }
        [FieldOrder(2)] public SerializedString Title { get; set; }
        [FieldOrder(3)] public uint SessionId { get; set; }
        [FieldOrder(4)] public uint PledgeId { get; set; }
        [FieldOrder(5)] public uint BuilderLevel { get; } = 0;
        [FieldOrder(6)] public uint Gender { get; set; }
        [FieldOrder(7)] public uint RaceId { get; set; }
        [FieldOrder(8)] public uint ClassId { get; set; }
        [FieldOrder(9)] public uint IsActive { get; set; } = 1;
        [FieldOrder(10)] public int X { get; set; }
        [FieldOrder(11)] public int Y { get; set; }
        [FieldOrder(12)] public int Z { get; set; }
        [FieldOrder(13)] public double CurrentHP { get; set; }
        [FieldOrder(14)] public double CurrentMP { get; set; }
        [FieldOrder(15)] public long SP { get; set; }
        [FieldOrder(16)] public long EXP { get; set; }
        [FieldOrder(17)] public uint Level { get; set; }
        [FieldOrder(18)] public uint Karma { get; set; }
        [FieldOrder(19)] public uint PK { get; set; }
        [FieldOrder(20)] public uint GameTime { get; set; } = (uint)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds;
        [FieldOrder(21)] public uint Unk2 { get; set; }
        [FieldOrder(22)] public uint ClassId2 { get; set; }
        [FieldOrder(23)] public uint GG1 { get; set; }
        [FieldOrder(24)] public uint GG2 { get; set; }
        [FieldOrder(25)] public uint GG3 { get; set; }
        [FieldOrder(26)] public uint GG4 { get; set; }
        [FieldOrder(27)] public byte[] GG5 { get; } = new byte[64];
        [FieldOrder(28)] public uint ObfuscationKey { get; } = 0;
    }
}
