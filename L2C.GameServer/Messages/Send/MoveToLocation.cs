﻿using L2C.GameServer.Messages.SerializableTypes;

namespace L2C.GameServer.Messages.Send
{
    class MoveToLocation : SendBaseFrame
    {

        public uint ObjectId { get; set; }

        public Vector3 Destination { get; set; }
        public Vector3 Current { get; set; }
    }
}