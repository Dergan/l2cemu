﻿namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_AuthUnk : L2SendBaseFrame
    {
        public uint Unk1 { get; } = 0xFFFFFFFF;
        public uint Unk2 { get; } = 0x00000000;

        //public override void Run(GameClient client, params object[] parameters)
        //{

        //}
    }
}