﻿using L2C.GameServer.Messages;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    class NewChar : SendBaseFrame
    {


        protected internal override void Write()
        {
            WriteByte(0x0d);
            WriteInteger(10);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0x46);
            WriteInteger(50);
            WriteInteger(0x0a);
            WriteInteger(0x46);
            WriteInteger(50);
            WriteInteger(0x0a);
            WriteInteger(0x46);
            WriteInteger(50);
            WriteInteger(0x0a);
            WriteInteger(0x46);
            WriteInteger(50);
            WriteInteger(0x0a);
            WriteInteger(0x46);
            WriteInteger(50);
            WriteInteger(0x0a);
            WriteInteger(0x46);
            WriteInteger(50);
            WriteInteger(0x0a);
        }
    }
}