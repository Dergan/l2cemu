﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;
using L2C.Game.Collections;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_SkillList : SendBasePacket
    {
        SkillCollection skills;
        public S_SkillList(SkillCollection skills)
            : base()
        {
            this.skills = skills;
        }

        protected internal override void Write()
        {
            WriteByte(0x5F);

            WriteInteger(skills.Count);

            for (int i = 0; i < skills.Count; i++)
            {
                WriteInteger(skills[i].Passive); //passive
                WriteInteger(skills[i].Level); //Level
                WriteInteger(skills[i].ID); //Skill Id
                WriteByte(skills[i].Disabled); //Disabled
                WriteByte(skills[i].Enchanted); //Enchanted
            }
        }
    }
}