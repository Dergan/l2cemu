﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_SystemMessage : L2SendBaseFrame
    {
        private int _msgId;
        private List<Argument> _args;
        
        public enum MsgType
        {
            Text = 0,
            Number = 1,
            Npc = 2,
            Item = 3,
            Skill = 4,
            Castle = 5,
            Item_Count = 6,
            Zone = 7,
            Element = 9,
            Instance = 10,
            Player = 12,
            System_String = 13
        }

        private struct Argument
        {
            public MsgType Type { get; set; }
            public object Value { get; set; }

            public Argument(MsgType type, object value) : this()
            {
                this.Type = type;
                this.Value = value;
            }
        }

        public S_SystemMessage(string message) : this(SystemMessages.S1, message) { }
        public S_SystemMessage(int messageId) : this((SystemMessages)messageId) { }
        public S_SystemMessage(SystemMessages MessageId, params object[] arguments)
            : base()
        {
            _msgId = (int)MessageId;
            _args = new List<Argument>();
            ParseArguments(arguments);
        }

        private void ParseArguments(object[] arguments)
        {
            foreach (object arg in arguments)
            {
                if (arg == null)
                    continue;

                if (typeof(string).IsInstanceOfType(arg))
                {
                    _args.Add(new Argument(MsgType.Text, arg));
                }
                else if (IsNumber(arg.GetType()))
                {
                    _args.Add(new Argument(MsgType.Number, arg));
                }
                else if (typeof(long).IsInstanceOfType(arg))
                {
                    _args.Add(new Argument(MsgType.Item_Count, arg));
                }
                else if (typeof(L2Npc).IsInstanceOfType(arg))
                {
                    _args.Add(new Argument(MsgType.Npc, ((L2Npc)arg).NpcId));
                }
                else if (typeof(L2Item).IsInstanceOfType(arg))
                {
                    _args.Add(new Argument(MsgType.Item, ((L2Item)arg).ID));
                }
                else if (typeof(L2Skill).IsInstanceOfType(arg))
                {
                    _args.Add(new Argument(MsgType.Skill, new int[] { ((L2Skill)arg).ID, ((L2Skill)arg).Level }));
                }
                //TODO Castle
                //TODO Zone
                //TODO Element
                //TODO Instance
                else if (typeof(L2Player).IsInstanceOfType(arg))
                {
                    _args.Add(new Argument(MsgType.Player, ((L2Player)arg).Name));
                }
                //else if (typeof(S_SystemMessage).IsInstanceOfType(arg))
                //{
                //    _args.Add(new Argument(MsgType.System_String, 0));
                //}
            }
        }

        private bool IsNumber(object number)
        {
            if (number.GetType() == typeof(byte) ||
                number.GetType() == typeof(sbyte) ||
                number.GetType() == typeof(short) ||
                number.GetType() == typeof(ushort) ||
                number.GetType() == typeof(int) ||
                number.GetType() == typeof(uint) ||
                number.GetType() == typeof(double) ||
                number.GetType() == typeof(float))
                return true;
            return false;
        }


        protected internal override void Write()
        {
            WriteByte(0x62);
            WriteShort((short)_msgId);
            WriteByte((byte)_args.Count); //count

            for (int i = 0; i < _args.Count; i++)
            {
                int t = (int)_args[i].Type;
                WriteByte((byte)t);

                switch ((MsgType)t)
                {
                    case MsgType.Text:
                    case MsgType.Player:
                        WriteString((string)_args[i].Value);
                        break;

                    case MsgType.Item_Count:
                        WriteLong((long)_args[i].Value);
                        break;

                    case MsgType.Item:
                    case MsgType.Castle:
                    case MsgType.Number:
                    case MsgType.Npc:
                    case MsgType.Element:
                    case MsgType.System_String:
                    case MsgType.Instance:
                        WriteInteger((int)_args[i].Value);
                        break;

                    case MsgType.Skill:
                        WriteInteger(((int[])_args[i].Value)[0]);//Skill ID
                        WriteInteger(((int[])_args[i].Value)[1]);//Skill Level
                        break;

                    case MsgType.Zone:
                        WriteInteger(((int[])_args[i].Value)[0]);//X
                        WriteInteger(((int[])_args[i].Value)[1]);//Y
                        WriteInteger(((int[])_args[i].Value)[2]);//Z
                        break;
                }
            }
        }
    }
}
