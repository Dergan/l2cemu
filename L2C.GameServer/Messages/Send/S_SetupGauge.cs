﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_SetupGauge : SendBasePacket
    {
        private int ObjId;
        private int time;

        public S_SetupGauge(int ObjId, int time)
            : base()
        {
            this.ObjId = ObjId;
            this.time = time;
        }

        protected internal override void Write()
        {
            WriteByte(0x27);
            WriteInteger(ObjId);
            WriteInteger(0); //??
            WriteInteger(time); //??
            WriteInteger(time); //??
        }
    }
}