﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ExUserInfoAbnormalVisualEffect : SendBasePacket
    {
        private L2Player player;
        public S_ExUserInfoAbnormalVisualEffect(L2Player player)
            : base()
        {
            this.player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteUShort(0x158);
            WriteInteger(player.ObjectId);
            WriteInteger(player.TransformId);
            WriteInteger(0);//EffectList
            //Foreach WriteShort(EffectId);
        }
    }
}