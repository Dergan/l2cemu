﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_StatusUpdate : SendBasePacket
    {
        private int ObjId;
        private List<UpdateInfo> _updateInfo;
        public S_StatusUpdate(int ObjId)
            : base()
        {
            this.ObjId = ObjId;
            _updateInfo = new List<UpdateInfo>();
        }

        public enum TypeInfo
        {
	        LEVEL = 0x01,
	        EXP = 0x02,
	        STR = 0x03,
	        DEX = 0x04,
	        CON = 0x05,
	        INT = 0x06,
	        WIT = 0x07,
	        MEN = 0x08,
	        CUR_HP = 0x09,
	        MAX_HP = 0x0a,
	        CUR_MP = 0x0b,
	        MAX_MP = 0x0c,
	        SP = 0x0d,
	        CUR_LOAD = 0x0e,
	        MAX_LOAD = 0x0f,
	        P_ATK = 0x11,
	        ATK_SPD = 0x12,
	        P_DEF = 0x13,
	        EVASION = 0x14,
	        ACCURACY = 0x15,
	        CRITICAL = 0x16,
	        M_ATK = 0x17,
	        CAST_SPD = 0x18,
	        M_DEF = 0x19,
	        PVP_FLAG = 0x1a,
	        KARMA = 0x1b,
	        CUR_CP = 0x21,
	        MAX_CP = 0x22
        }

        public class UpdateInfo
        {
            public int UpdateType;
            public int Value;

            public UpdateInfo(TypeInfo typeInfo, int Value)
            {
                this.UpdateType = (int)typeInfo;
                this.Value = Value;
            }
        }

        public void AddValue(TypeInfo typeInfo, int Value)
        {
            _updateInfo.Add(new UpdateInfo(typeInfo, Value));
        }

        protected internal override void Write()
        {
            WriteByte(0x18);
            WriteInteger(ObjId);
            WriteInteger(_updateInfo.Count);

            foreach(UpdateInfo info in _updateInfo)
            {
                WriteInteger(info.UpdateType);
                WriteInteger(info.Value);
            }
        }
    }
}
