﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;
using L2C.Game.Collections;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_PartyList : SendBasePacket
    {
        private PartyCollection party;
        private int ExcludeObjectId;
        public S_PartyList(PartyCollection party, int ExcludeObjectId)
            : base()
        {
            this.party = party;
            this.ExcludeObjectId = ExcludeObjectId;
        }

        protected internal override void Write()
        {
            WriteByte(0x4E);
            WriteInteger(party.Owner.ObjectId);
            WriteInteger(0); //party loot info
            WriteInteger(party.Count-1);

            foreach (L2Player player in party)
            {
                if (player.ObjectId != ExcludeObjectId)
                {
                    WriteInteger(player.ObjectId);
                    WriteString(player.Name);
                    WriteInteger((int)player.CurCP);
                    WriteInteger((int)player.maxCP);
                    WriteInteger((int)player.CurHP);
                    WriteInteger((int)player.MaxHP);
                    WriteInteger((int)player.CurMP);
                    WriteInteger((int)player.MaxMP);
                    WriteInteger(player.Level);
                    WriteInteger(player.ClassId);
                    WriteInteger(0); //need to test
                    WriteInteger(player.RaceId);
                    WriteInteger(0);//test
                    WriteInteger(0);//test

                    //need to add the pet info here
                    if (player.Pet.pet != null)
                    {

                    }



                    if (player.Pet.pet == null)
                        WriteInteger(0);
                }
            }
        }
    }
}