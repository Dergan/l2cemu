﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_PartyMemberRemove : SendBasePacket
    {
        private L2Player _player;

        public S_PartyMemberRemove(L2Player player)
            : base()
        {
            this._player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0x51);
            WriteInteger(_player.ObjectId);
            WriteString(_player.Name);
        }
    }
}