﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;
using L2C.Game.Collections;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_PartyMemberAdd : SendBasePacket
    {
        private L2Player _player;
        private PartyCollection _party;

        public S_PartyMemberAdd(L2Player player, PartyCollection party)
            : base()
        {
            _player = player;
            _party = party;
        }

        protected internal override void Write()
        {
            WriteByte(0x4F);

            WriteInteger(_party.Owner.ObjectId);
            WriteInteger(0);//Party Loot.
            WriteInteger(_player.ObjectId);
            WriteString(_player.Name);
            WriteInteger((int)_player.CurCP);
            WriteInteger((int)_player.MaxCP);
            WriteInteger((int)_player.CurHP);
            WriteInteger((int)_player.MaxHP);
            WriteInteger((int)_player.CurMP);
            WriteInteger((int)_player.MaxMP);
            WriteInteger((int)_player.Level);
            WriteInteger((int)_player.ClassId);
            WriteInteger(0);
            WriteInteger(0);
        }
    }
}