﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_DropItem : SendBasePacket
    {
        L2Player player;
        L2Item item;
        int x;
        int y;
        int z;
        long count;
        public S_DropItem(L2Player player, L2Item item, int x, int y, int z, long count)
            : base()
        {
            this.player = player;
            this.item = item;
            this.x = x;
            this.y = y;
            this.z = z;
            this.count = count;
        }

        protected internal override void Write()
        {
            WriteByte(0x16);
            WriteInteger(player.ObjectId);
            WriteInteger(item.ItemObjId);
            WriteInteger(item.ID);
            WriteInteger(x);
            WriteInteger(y);
            WriteInteger(z);
            WriteInteger(item.IsEquippable ? 1 : 0);
            WriteLong(count);
            WriteInteger(1);
        }
    }
}