﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_CommunityBoard : SendBasePacket
    {
        public S_CommunityBoard()
            : base()
        {
        }

        string LineageTopImage = @"<br><center><table width=256 heigth=32 border=0>
                                   <tr>
                                       <td><img src=""Icon.etc_alphabet_l_i00"" width=32 height=32></td>
                                       <td><img src=""Icon.etc_alphabet_i_i00"" width=32 height=32></td>
                                       <td><img src=""Icon.etc_alphabet_n_i00"" width=32 height=32></td>
                                       <td><img src=""Icon.etc_alphabet_e_i00"" width=32 height=32></td>
                                       <td><img src=""Icon.etc_alphabet_a_i00"" width=32 height=32></td>
                                       <td><img src=""Icon.etc_alphabet_g_i00"" width=32 height=32></td>
                                       <td><img src=""Icon.etc_alphabet_e_i00"" width=32 height=32></td>
                                       <td><img src=""Icon.etc_alphabet_ii_i00"" width=32 height=32></td>
                                   </tr></table>";

        protected internal override void Write()
        {
            WriteByte(0x7B);
            WriteByte(1); //Show/Hide
            WriteString("bypass bbs_top");
            WriteString("bypass bbs_favorite");
            WriteString("bypass bbs_region");
            WriteString("bypass bbs_clan");
            WriteString("bypass bbs_memo");
            WriteString("bypass bbs_mail");
            WriteString("bypass bbs_friend");
            WriteString("bypass bbs_add_fav");
            WriteString("<html><body><br>" + LineageTopImage + "</body></html>");
        }
    }
}