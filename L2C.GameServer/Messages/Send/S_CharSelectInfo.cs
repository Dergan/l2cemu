﻿using System.Collections.Generic;

namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_CharSelectInfo : L2SendBaseFrame
    {
        private GameClient client;

        public S_CharSelectInfo(GameClient client)
        {
            this.client = client;
        }

        public int PlayerCount { get; }
        public int MaxCharacters { get; set; } = 7;
        public bool CanCreateCharacter { get; set; } = true;
        public byte PlayMode { get; } = 1;//1 create only, 2 char in  regular lobby
        public int Unk3 { get; } = 2;
        public byte SuggestPremiumAccount { get; }

        List<CharacterInfo> Characters { get; set; } = new List<CharacterInfo>();

        //public override void Run(GameClient client, params object[] parameters)
        //{
        //}

        public class CharacterInfo
        {
            public string Name { get; set; }
            public int ObjectId { get; set; }

            public string LoginName { get; set; } = "L2C Wins";
            public int SessionId { get; set; } = 1;
            public int ClanId { get; set; }
            public int Unk1 { get; }

            public int Gender { get; set; }
            public int RaceId { get; set; }
            public int ClassId { get; set; }
            public int ServerId { get; } = 1;

            public int X { get; set; }
            public int Y { get; set; }
            public int Z { get; set; }

            public double CurrentHP { get; set; }
            public double CurrentMP { get; set; }

            public long SP { get; set; }
            public long EXP { get; set; }
            public float ExpPercent { get; set; }            
            public int Level { get; set; }
            
            public int Karma { get; set; }
            public int PK { get; set; }
            public int PVP { get; set; }

            //Unkown 603
            public int Unk2 { get; }
            public int Unk3 { get; }
            public int Unk4 { get; }
            public int Unk5 { get; }
            public int Unk6 { get; }
            public int Unk7 { get; }
            public int Unk8 { get; }

            //Erthreia
            public int Unk9 { get; }
            public int Unk10 { get; }

            //ItemIds
            public int RHAND { get; set; }
            public int LHAND { get; set; }
            public int GLOVES { get; set; }
            public int CHEST { get; set; }
            public int LEGS { get; set; }
            public int FEET { get; set; }
            public int HAIR { get; set; }
            public int HAIR2 { get; set; }


            //Visual ItemIds
            public int VisualRHAND { get; set; }
            public int VisualLHAND { get; set; }
            public int VisualGLOVES { get; set; }
            public int VisualCHEST { get; set; }
            public int VisualLEGS { get; set; }
            public int VisualFEET { get; set; }
            public int VisualHAIR { get; set; }
            public int VisualHAIR2 { get; set; }

            public int Unk26 { get; }
            public int Unk27 { get; }
            public int Unk30 { get; }
            public ushort Unk41 { get; }

            public int HairStyle { get; set; }
            public int HairColor { get; set; }
            public int Face { get; set; }

            public double MaxHP { get; set; }
            public double MaxMP { get; set; }

            public int DeleteTime { get; set; }
            public int ClassId2 { get; set; }
            public int AutoSelect { get; set; } = 1;
            public byte EnchantEffect { get; set; }
            public int AugmentId { get; set; }
            public int TransformID { get; set; }

            public int PedId { get; set; }
            public int PedLevel { get; set; }
            public int Unk42 { get; set; }
            public int PedFood { get; set; }
            public double PedMaxHP { get; set; }
            public double PedMaxMP { get; set; }

            public int Unk43 { get; } //63 helios

            public int VitalityPoints { get; set; } = 140000;
            public int VitalityExpBonus { get; set; } = 200;//Freya
            public int VitalityItemsUsed { get; set; } = 5;
            public int IsActive2 { get; set; } = 1;
            public bool IsNoble { get; set; }
            public bool IsHero { get; set; }
            public bool IsHairAccessoryEnabled { get; set; }
        }
    }
}