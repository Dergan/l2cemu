﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class VersionCheck : SendBaseFrame
    {
        [FieldOrder(0)]
        public bool ProtocolOk { get; set; }

        [FieldOrder(1)]
        [FieldLength(8)]
        public byte[] BlowfishKey { get; set; }

        [FieldOrder(2)]
        public int CipherEnabled { get; } = 1;

        [FieldOrder(3)]
        public int ServerId { get; set; }

        [FieldOrder(4)]
        public bool IsMergedServer { get; set; } = false;

        [FieldOrder(5)]
        public int ObfuscationKey { get; } = 0;

        [FieldOrder(6)]
        public bool ClassicMode { get; set; } = true;

        [FieldOrder(7)]
        public bool ArenaMode { get; set; } = true;
    }
}
