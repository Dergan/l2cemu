﻿using System.Collections.Generic;
using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class ItemList : SendBaseFrame
    {
        [FieldOrder(0)]
        public byte OpenWindow { get; set; }

        [FieldOrder(1)]
        public uint ItemCount { get; set; }

        [FieldOrder(2)]
        public uint TotalItemCount { get; set; } = 0x1D;

        [FieldOrder(3)]
        [FieldCount(nameof(ItemCount))]
        public List<byte> Items { get; set; }
    }
}
