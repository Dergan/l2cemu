﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class UIPacket : SendBaseFrame
    {

        [FieldOrder(0)] public uint ObjectId { get; set; }
        [FieldOrder(1)] public uint Length { get; set; }

        [FieldOrder(2)]
        public ushort StructType { get; set; }

        [FieldOrder(3)]
        [FieldLength(nameof(Length))]
        [Subtype(nameof(StructType), 0x18, typeof(UserInfoContent))]
        public UIPacketContent DynamicContent { get; set; }
    }

    abstract class UIPacketContent
    {
    }
}
