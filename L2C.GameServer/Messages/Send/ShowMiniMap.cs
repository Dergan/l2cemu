﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class ShowMinimap : SendBaseFrame
    {

        [FieldOrder(0)]
        public uint Map { get; set; } = 0;

        [FieldOrder(1)]
        public byte SevenSignsPeriod { get; set; }
    }
}