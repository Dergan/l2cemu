﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ClanShowMemberListAll : SendBasePacket
    {
        private L2Clan clan;
        public S_ClanShowMemberListAll(L2Clan clan)
        {
            this.clan = clan;
        }

        protected internal override void Write()
        {
            WriteByte(0x5A);
            WriteInteger(0);
            WriteInteger(clan.ClanId);
            WriteInteger(0); //pledge type
            WriteString(clan.ClanName);
            WriteString(clan.ClanLeader.Name);
            WriteInteger(clan.CrestId);
            WriteInteger(clan.ClanLevel);
            WriteInteger(0); //has castle
            WriteInteger(0); //hide out
            WriteInteger(0); //has fort
            WriteInteger(0); //rank
            WriteInteger(clan.RepScore);
            WriteInteger(0);//?
            WriteInteger(0);//?
            WriteInteger(clan.AllianceId);
            WriteString(clan.AllianceName);
            WriteInteger(clan.AllianceCrestId);
            WriteInteger(0); //war
            WriteInteger(0); //territory castle id
            WriteInteger(clan.members.Count);

            foreach (ClanMember member in new List<ClanMember>(clan.members.Values))
            {
                WriteString(member.Name);
                WriteInteger(member.Level);
                WriteInteger(member.ClassId);
                WriteInteger(member.Sex);
                WriteInteger(member.Race);

                WriteInteger((member.player != null) ? 1 : 0);
                WriteInteger(0); //sponsor
            }
        }
    }
}