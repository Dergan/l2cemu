﻿using L2C.Game.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ExSubjobInfo : SendBasePacket
    {
        private L2Player player;

        public S_ExSubjobInfo(L2Player player)
            : base()
        {
            this.player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteUShort(0xEA);
            
            WriteInteger(player.ClassId);
            WriteInteger(player.RaceId);
            WriteInteger(0+1); //subclasses

            WriteInteger(0);//ClassIndex
            WriteInteger(0);//BaseClass
            WriteInteger(1);//BaseLevel
            WriteByte(0);//0 Main, 1 dual, 2 sub

            //WriteInteger(0);//ClassIndex
            //WriteInteger(0);//BaseClass
            //WriteInteger(1);//BaseLevel
            //WriteByte(0);//0 Main, 1 dual, 2 sub
        }
    }
}