﻿namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_SelectTarget : L2SendBaseFrame
    {
        public uint ObjectId { get; set; }
        public uint TargetObjectId { get; set; }
        public uint X { get; set; }
        public uint Y { get; set; }
        public uint Z { get; set; }
        public uint Unk1 { get; set; }
    }
}