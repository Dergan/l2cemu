﻿namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_DeleteObject : L2SendBaseFrame
    {
        public uint ObjectId { get; set; }
        public uint Unk1 { get; } = 0;
    }
}