﻿namespace L2C.GameServer.Messages.Send
{
    class CharacterCreateFail : SendBaseFrame
    {
        public uint Reason { get; set; }
    }
}
