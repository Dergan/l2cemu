﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ExVitalityEffectInfo : SendBasePacket
    {
        public S_ExVitalityEffectInfo()
            : base()
        {
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteByte(0x18);
            WriteByte(0x01);

            WriteInteger(140000);
            WriteInteger(200);
            WriteShort(5);
            WriteShort(5);
        }
    }
}