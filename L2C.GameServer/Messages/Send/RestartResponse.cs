﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class RestartResponse : SendBaseFrame
    {

        [FieldOrder(0)]
        public uint Restart { get; set; } = 1;
    }
}