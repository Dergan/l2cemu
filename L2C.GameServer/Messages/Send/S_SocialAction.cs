﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_SocialAction : SendBasePacket
    {
        private int socialid;
        private int ObjId;
        public S_SocialAction(int socialid, int ObjId)
            : base()
        {
            this.socialid = socialid;
            this.ObjId = ObjId;
        }

        public S_SocialAction(SocialActions SocialAction, int ObjId)
            : base()
        {
            this.socialid = (int)SocialAction;
            this.ObjId = ObjId;
        }


        public enum SocialActions
        {
            Bow = 7,
            Applause = 11,
            Yes = 6,
            Waiting = 9,
            Victory = 3,
            Unaware = 8,
            Sad = 13,
            No = 5,
            Laugh = 10,
            Hello = 2,
            Dance = 12,
            Charm = 14,
            Charge = 4,
            Shyness = 15,
            TwoPersonBow = 16,
            HiFive = 17,
            CoupleDance = 18
        }

        protected internal override void Write()
        {
            WriteByte(0x27);
            WriteInteger(ObjId);
            WriteInteger(socialid);
        }
    }
}