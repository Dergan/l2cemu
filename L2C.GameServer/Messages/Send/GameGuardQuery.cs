﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class GameGuardQuery : SendBaseFrame
    {
        [FieldOrder(0)]
        public uint Unk1 => 0x27533DD9;

        [FieldOrder(1)]
        public uint Unk2 => 0x2E72A51D;

        [FieldOrder(2)]
        public uint Unk3 => 0x2017038B;

        [FieldOrder(3)]
        public uint Unk4 => 0xC35B1EA3;
    }
}