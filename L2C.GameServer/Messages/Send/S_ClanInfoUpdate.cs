﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ClanInfoUpdate : SendBasePacket
    {
        private L2Clan clan;
        public S_ClanInfoUpdate(L2Clan clan)
            : base()
        {
            this.clan = clan;
        }

        protected internal override void Write()
        {
            WriteByte(0x8E);
            WriteInteger(clan.ClanId);
            WriteInteger(clan.CrestId);
            WriteInteger(clan.ClanLevel);
            WriteInteger(0); //has castle
            WriteInteger(0); //hide out
            WriteInteger(0); //has fort
            WriteInteger(0); //rank
            WriteInteger(clan.RepScore);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(clan.AllianceId);
            WriteString(clan.AllianceName);
            WriteInteger(clan.AllianceCrestId);
            WriteInteger(0); //war
        }
    }
}