﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public enum PartyAnswer : int
    {
        Yes = 1,
        No = 0
    }

    public class S_PartyAnswer : SendBasePacket
    {

        PartyAnswer answer;
        public S_PartyAnswer(PartyAnswer answer)
            : base()
        {
            this.answer = answer;
        }

        protected internal override void Write()
        {
            WriteByte(0x3A);
            WriteInteger((int)answer);
        }
    }
}