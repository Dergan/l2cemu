﻿using L2C.GameServer.Objects;

namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_CharSelected : L2SendBaseFrame
    {
        private L2Player playerInstance;

        public S_CharSelected(L2Player playerInstance)
        {
            this.playerInstance = playerInstance;
        }

        public string Name { get; set; }
        public uint ObjectId { get; set; }
        public string Title { get; set; }
        public uint SessionId { get; set; }
        public uint ClanId { get; set; }
        public uint Unk1 { get; } = 0;
        public uint Gender { get; set; }
        public uint RaceId { get; set; }
        public uint ClassId { get; set; }
        public uint IsActive { get; set; } = 1;
        public uint X { get; set; }
        public uint Y { get; set; }
        public uint Z { get; set; }
        public double CurrentHP { get; set; }
        public double CurrentMP { get; set; }
        public long SP { get; set; }
        public long EXP { get; set; }
        public uint Level { get; set; }
        public uint Karma { get; set; }
        public uint PK { get; set; }

        public uint GameTime { get; set; }
        public uint Unk2 { get; set; }

        public uint ClassId2 { get; set; }
        public byte[] Unk3 { get; } = new byte[16];

        public uint Unk4 { get; } = 0;
        public uint Unk5 { get; } = 0;
        public uint Unk6 { get; } = 0;
        public uint Unk7 { get; } = 0;
        public uint Unk8 { get; } = 0;
        public uint Unk9 { get; } = 0;
        public uint Unk10 { get; } = 0;
        public uint Unk11 { get; } = 0;
        public uint Unk12 { get; } = 0;
        public byte[] Unk13 { get; } = new byte[28];
        public uint Unk14 { get; } = 0;

        //public override void Run(GameClient client, params object[] parameters)
        //{
        //}
    }
}
