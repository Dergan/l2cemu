﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_SkillLaunched : SendBasePacket
    {
        private L2Entity player;
        private L2Skill skill;
        public S_SkillLaunched(L2Entity player, L2Skill skill)
            : base()
        {
            this.player = player;
            this.skill = skill;
        }

        protected internal override void Write()
        {
            WriteByte(0x54);
            WriteInteger(player.ObjectId);
            WriteInteger(skill.ID);
            WriteInteger(skill.Level);
            WriteInteger(1); //number of targets, for now its 1
            WriteInteger(player.Target.ObjectId);
        }
    }
}