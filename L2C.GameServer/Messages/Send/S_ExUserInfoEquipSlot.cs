﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;
using System.Collections;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ExUserInfoEquipSlot : SendBasePacket
    {
        private L2Player player;
        public S_ExUserInfoEquipSlot(L2Player player)
            : base()
        {
            this.player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteUShort(0x156);
            WriteInteger(player.ObjectId);
            WriteUShort(33);

            BitArray mask = new BitArray(40);

            
            //Ma
            WriteByte(255);
            WriteByte(255);
            WriteByte(255);
            WriteByte(255);
            WriteByte(255);
            ///////////////////////////////////////////////////////////////
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.UNDERWEAR).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.UNDERWEAR).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.UNDERWEAR).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_EAR).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_EAR).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_EAR).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_EAR).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_EAR).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_EAR).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.NECK).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.NECK).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.NECK).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_FINGER).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_FINGER).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_FINGER).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_FINGER).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_FINGER).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_FINGER).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.HEAD).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.HEAD).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.HEAD).AugmentId); 
            WriteInteger(0);
            if (0 == 0)//_airShipHelm
            {
                WriteUShort(18);
                WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).ID);
                WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).ItemObjId);
                WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).AugmentId); 
                WriteInteger(0);
                WriteUShort(18);
                WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND).ID);
                WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND).ItemObjId);
                WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND).AugmentId); 
                WriteInteger(0);
            }
            else
            {
                WriteUShort(18);
                WriteInteger(0);//_airShipHelm
                WriteInteger(0);
                WriteInteger(0);
                WriteInteger(0);
                WriteUShort(18);
                WriteInteger(0);
                WriteInteger(0);
                WriteInteger(0);
                WriteInteger(0);
            }
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.GLOVES).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.GLOVES).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.GLOVES).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.LEGS).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.LEGS).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.LEGS).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.FEET).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.FEET).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.FEET).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.BACK).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.BACK).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.BACK).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR2).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR2).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR2).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_BRACELET).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_BRACELET).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.R_BRACELET).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_BRACELET).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_BRACELET).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.L_BRACELET).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).AugmentId); 
            WriteInteger(0);
            WriteUShort(18);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.BELT).ID);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.BELT).ItemObjId);
            WriteInteger(player.Inventory.GetEquipedItem(L2Item.ItemSlot.BELT).AugmentId); 
            WriteInteger(0);
            ///////////////////////////////////////////////////////////////
            WriteUShort(18); // 603-1 BROOCH
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteUShort(18); // 603-2 BROOCH_JEWEL1
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteUShort(18); // 603-3 BROOCH_JEWEL2
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteUShort(18); // 603-4 BROOCH_JEWEL3
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteUShort(18); // 603-5 BROOCH_JEWEL4
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteUShort(18); // 603-6 BROOCH_JEWEL5
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteUShort(18); // 603-7 BROOCH_JEWEL6
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
        }
    }
}