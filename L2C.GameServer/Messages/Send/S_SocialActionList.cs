﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_SocialActionList : SendBasePacket
    {
        public S_SocialActionList()
            : base()
        {
        }

        public static int[] getDefaultActionList()
        {
            int count1 = 90;
            int count2 = 61;
            int[] actionIds = new int[count1 + count2];

            int index = 0;
            for (int i = 0; i <= count1 - 1; i++)
            {
                actionIds[index] = i;
                index += 1;
            }
            for (int i = 0; i <= count2 - 1; i++)
            {
                actionIds[index] = 1000 + i;
                index += 1;
            }
            return actionIds;
        }


        protected internal override void Write()
        {
            int[] ActionList = getDefaultActionList();

            WriteByte(0xFE);
            WriteShort(0x5F);
            WriteInteger(ActionList.Length);
            for (int i = 0; i < ActionList.Length; i++)
                WriteInteger(ActionList[i]);
        }
    }
}
