﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_RequestJoinParty : SendBasePacket
    {
        private L2Entity entity;
        private string Invitor;
        public S_RequestJoinParty(L2Entity entity, string Invitor)
        {
            this.entity = entity;
            this.Invitor = Invitor;
        }

        protected internal override void Write()
        {
            WriteByte(0x39);
            WriteString(Invitor);
            WriteInteger(0); //Item....
        }
    }
}