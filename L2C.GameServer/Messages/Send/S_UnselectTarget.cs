﻿using L2C.GameServer.Objects;

namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_UnselectTarget : L2SendBaseFrame
    {
        private L2Player playerInstance;

        public S_UnselectTarget(L2Player playerInstance)
        {
            this.playerInstance = playerInstance;
        }

        public uint ObjectId { get; set; }
        public uint X { get; set; }
        public uint Y { get; set; }
        public uint Z { get; set; }
        public uint Unk1 { get; set; }
    }
}