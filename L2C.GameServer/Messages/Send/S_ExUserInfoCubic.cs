﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ExUserInfoCubic : SendBasePacket
    {
        private L2Player player;
        public S_ExUserInfoCubic(L2Player player)
            : base()
        {
            this.player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteUShort(0x157);
            WriteInteger(player.ObjectId);
            WriteUShort(0);//Cubic Count
            //Foreach WriteShort(CubicId);
            WriteInteger(0);//AgathionId
        }
    }
}