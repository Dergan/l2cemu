﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_UserInfo : SendBasePacket
    {
        private L2Player chr;

        public S_UserInfo(L2Player client)
            : base()
        {
            chr = client;
        }

        protected internal override void Write()
        {
            WriteByte(0x32); //PacketId

            WriteInteger(chr.Position.X);
            WriteInteger(chr.Position.Y);
            WriteInteger(chr.Position.Z);
            WriteInteger(0); //Vehicle Object Id
            WriteInteger(chr.ObjectId);
            WriteString(chr.Name);
            WriteInteger(chr.RaceId); //Race
            WriteInteger(chr.Gender); //Sex
            WriteInteger(chr.ClassId); //Class Id
            WriteInteger(chr.Level); //Level
            WriteLong(chr.EXP); //Exp
            WriteDouble(0.2d); //Exp %
            WriteInteger(1);//chr.STR); //STR
            WriteInteger(2);//chr.DEX); //DEX
            WriteInteger(3);//chr.CON); //CON
            WriteInteger(4);//chr.INT); //INT
            WriteInteger(5);//chr.WIT); //WIT
            WriteInteger(6);//chr.MEN); //MEN
            WriteInteger(10);//(int)chr.MaxHP); //Max HP
            WriteInteger(11);//(int)chr.CurHP); //Cur HP
            WriteInteger(12);//(int)chr.MaxMP); //Max MP
            WriteInteger(13);//(int)chr.CurMP); //Cur MP
            WriteInteger(7);//chr.SP); //SP
            WriteInteger(chr.Inventory.ItemCount); //Current Load
            WriteInteger(chr.InventoryLimit); //Max Load

            if (chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND).ItemObjId != 0 ||
                chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).ItemObjId != 0 ||
               chr.Inventory.GetEquipedItem(L2Item.ItemSlot.LR_HAND).ItemObjId != 0)//Weapon Equipped (40=Equipped, 20=No Weapon)
                WriteInteger(40);
            else
                WriteInteger(20);

            //Item ObjectIds
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.UNDERWEAR).ItemObjId);
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_EAR).ItemObjId); //REAR
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_EAR).ItemObjId); //LEAR
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.NECK).ItemObjId); //NECK
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_FINGER).ItemObjId); //RFINGER
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_FINGER).ItemObjId); //LFINGER
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.HEAD).ItemObjId); //HEAD
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).ItemObjId); //RHAND
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND).ItemObjId); //LHAND
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.GLOVES).ItemObjId); //GLOVES
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST).ItemObjId); //CHEST
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.LEGS).ItemObjId); //LEGS
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.FEET).ItemObjId); //FEET
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.BACK).ItemObjId); //BACK
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.LR_HAND).ItemObjId); //LRHAND
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR).ItemObjId); //HAIR
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR2).ItemObjId); //HAIR2
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_BRACELET).ItemObjId); //RBRACELET
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_BRACELET).ItemObjId); //LBRACELET

            //NEED TO FIX DECO'S
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId); //DECO1
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId); //DECO2
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId); //DECO3
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId); //DECO4
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId); //DECO5
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ItemObjId); //DECO6
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.BELT).ItemObjId); //BELT

            //Item Id's
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.UNDERWEAR).ID);
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_EAR).ID); //REAR
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_EAR).ID); //LEAR
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.NECK).ID); //NECK
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_FINGER).ID); //RFINGER
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_FINGER).ID); //LFINGER
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.HEAD).ID); //HEAD
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_HAND).ID); //RHAND
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_HAND).ID); //LHAND
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.GLOVES).ID); //GLOVES
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.CHEST).ID); //CHEST
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.LEGS).ID); //LEGS
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.FEET).ID); //FEET
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.BACK).ID); //BACK
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.LR_HAND).ID); //LRHAND
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR).ID); //HAIR
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.HAIR2).ID); //HAIR2
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.R_BRACELET).ID); //RBRACELET
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.L_BRACELET).ID); //LBRACELET

            //NEED TO FIX DECO'S
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO1
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO2
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO3
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO4
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO5
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.DECO).ID); //DECO6
            WriteInteger(chr.Inventory.GetEquipedItem(L2Item.ItemSlot.BELT).ID); //BELT

            //Write Item Slots AugmentIds
            for (int i = 0; i < 52; i++)
                WriteShort(0);

            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);
            WriteInteger(0);

            WriteInteger(1);//talisman count
            WriteInteger(2); //cloak status
            WriteInteger(chr.Patk); //Patk
            WriteInteger(chr.AttackSpeed); //PAtkSpd??
            WriteInteger(chr.Pdef); //Pdef
            WriteInteger(chr.Evasion); //Evasion
            WriteInteger(chr.Accuracy); //Accuracy
            WriteInteger(chr.Critical); //Critical
            WriteInteger(chr.MAtk); //MAtk
            WriteInteger(chr.CastSpeed); //MatkSpd
            WriteInteger(chr.AttackSpeed); //PatkSpd??
            WriteInteger(chr.Mdef); //Mdef
            WriteInteger(13);//MEvasion
            WriteInteger(14);//MAccuracy
            WriteInteger(15);//MCritical
            WriteInteger(chr.PVP); //PvpFlag (0=White, 1=Violet name)
            WriteInteger(chr.Karma); //Karma

            //need to fix soon
            WriteInteger(chr.RunSpeed); //RunSpd
            WriteInteger(chr.WalkSpeed); //WalkSpd
            WriteInteger(chr.RunSpeed); //RunSpd Water
            WriteInteger(chr.WalkSpeed); //WalkSpd Water
            WriteInteger(chr.RunSpeed); //??
            WriteInteger(chr.WalkSpeed); //??
            WriteInteger(chr.RunSpeed); //RunSpd Fly
            WriteInteger(chr.WalkSpeed); //WalkSpd Fly
            WriteDouble(1); //MoveMultiplier

            //some sick calculation i found out :)
            double PatkSpdMultiplier = Convert.ToDouble((chr.AttackSpeed / 500)/* * 1.833333*/);
            WriteDouble(PatkSpdMultiplier); //PatkSpdMultiplier

            WriteDouble(chr.CollisionRadius); //Collision Radius
            WriteDouble(chr.CollisionHeight); //Collision Height
            WriteInteger(chr.HairStyle); //HairStyle
            WriteInteger(chr.HairColor); //HairColor
            WriteInteger(chr.Face); //Face
            WriteInteger(chr.isGM ? 1 : 0); //Builder Level (1=GM, 0=Player)
            WriteString(chr.Title); //Title

            if (chr.clan != null)
            {
                WriteInteger(chr.clan.ClanId); // ClanId
                WriteInteger(chr.clan.CrestId); // ClanCrestId
                WriteInteger(chr.clan.AllianceId); //AllyId
                WriteInteger(chr.clan.AllianceCrestId); // ally crest id
            }
            else
            {
                WriteInteger(0);
                WriteInteger(0);
                WriteInteger(0);
                WriteInteger(0);
            }

            WriteInteger(0); //Leader Rights??? WTF? XD
            WriteByte(0); //MountType
            WriteByte(0); //StoreType
            WriteByte(0); //DwarvenCraft (0 or 1)
            WriteInteger(chr.PK); //Pk Kills
            WriteInteger(chr.PVP); //Pvp Kills

            WriteShort((short)chr.CubicList.Count); //Cubic Count
            for (int i = 0; i < chr.CubicList.Count; i++)
                WriteShort((short)chr.CubicList[i].type);

            WriteByte(0);//Find Party Member (0 or 1)
            //WriteInteger(chr.AbnormalEffectId); //Abnormal Effect
            WriteByte(0); //IsFlyMount (0 or 2)
            WriteInteger(0); //Clan Privileges
            WriteShort(chr.RecomLeft); //Recom Left
            WriteShort(chr.RecomHave); //Recom Have
            WriteInteger(0); //NpcMount (NpcId + 1000000 or 0)

            WriteShort(chr.InventoryLimit); //Inventory Limit
            WriteInteger(chr.ClassId); //Class Id
            WriteInteger(0); //Special Effects??
            WriteInteger((int)chr.maxCP); //Max CP
            WriteInteger((int)chr.CurCP); //Cur CP
            WriteByte(0); //IsMounted (Enchant Effect or 0)
            WriteByte(chr.TeamCircle); //Team Circle (0=None, 1=Blue, 2=Red)
            WriteInteger(0); //ClanCrestId
            WriteByte(chr.isNoble); //IsNoble
            WriteByte(chr.isHero); //IsHero
            WriteByte(0); //isFishing
            WriteInteger(0); //Fish X
            WriteInteger(0); //Fish Y
            WriteInteger(0); //Fish Z
            WriteInteger(0xFFFFFF); //Name Color
            WriteByte(chr.isRunning); //IsRunning (Changes Speed in display)
            WriteInteger(chr.PledgeClass); //PledgeClass
            WriteInteger(chr.PledgeType); //PledgeType
            WriteInteger(0xECF9A2); //Title Color
            WriteInteger(0); //IsCursedWeaponEquipped (CursedWeaponLevel or 0)
            WriteInteger(0); //Transform Id
            //Attributes
            WriteShort(1); //Attack
            WriteShort(2); //Attack Element
            WriteShort(3); //Fire
            WriteShort(4); //Water
            WriteShort(5); //Wind
            WriteShort(6); //Earth
            WriteShort(7); //Holy
            WriteShort(8); //Dark

            WriteInteger(0); //AgathionId
            WriteInteger(0); //Fame
            WriteInteger(1); //MiniMap Allowed (Hellbound)
            WriteInteger(10); //Vitality Points
            WriteInteger(0); //Special Effect
            WriteInteger(0);
            WriteInteger(0);
            WriteByte(0);
            WriteInteger(0);//Count
            //WriteInteger(0);
            WriteByte(0);
        }
    }
}
