﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_VitalityUpdate : SendBasePacket
    {
        GameClient client;
        public S_VitalityUpdate(GameClient client)
            : base()
        {
            this.client = client;
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteByte(0x1F);

            WriteByte(0x01);//??

            WriteInteger(200);//Bonus %
            WriteInteger(20);//Items can be used
        }
    }
}