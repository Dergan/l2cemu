﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;
using System.IO;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_HtmlMessage : SendBasePacket
    {
        private int npcId;
        private int ItemId;
        private string HTML;
        private L2Npc npc;

        public S_HtmlMessage(int npcId, int ItemId, string HTML)
            : base()
        {
            this.npcId = npcId;
            this.ItemId = ItemId;
            this.HTML = HTML;
        }

        /// <summary>
        /// This constructor is not secure against packet hacking
        /// </summary>
        public S_HtmlMessage(L2Npc npc, int ItemId, string HTML)
            : base()
        {
            //not secure... lets remove this constructor ?
            this.npcId = npc.NpcId;
            this.ItemId = ItemId;
            this.HTML = HTML;
            this.npc = npc;
        }

        public S_HtmlMessage(L2Npc npc, int ItemId)
            : base()
        {
            this.npcId = npc.NpcId;
            this.ItemId = ItemId;
            this.npc = npc;
            this.HTML = "";
        }

        public void LoadHTML(short ChatIndex)
        {
            string file = "";
            if (ChatIndex > 0)
                file = npc.NpcId + "-" + ChatIndex + ".htm";
            else
                file = npc.NpcId + ".htm";

            if (File.Exists(Environment.CurrentDirectory + "\\Data\\html\\" + npc.NpcType + "\\" + npc.NpcId))
            {
                HTML = File.ReadAllText(Environment.CurrentDirectory + "\\Data\\html\\" + npc.NpcType + "\\" + npc.NpcId);
            }
            else
            {
                HTML = "<html><body>The file " + Environment.CurrentDirectory + "\\Data\\html\\" + npc.NpcType + "\\" + npc.NpcId + ".htm" + " is not found, Report this to the administrator</body></html>";
                Logger.Info("HTML Page is not found " + Environment.CurrentDirectory + "\\Data\\html\\" + npc.NpcType + "\\" + npc.NpcId, Logging.Logger.LogType.Error);
            }
        }



        protected internal override void Write()
        {
            WriteByte(0x19);
            WriteInteger(npcId);

            HTML = HTML.Replace("%objectId%", npcId.ToString());

            WriteString(HTML);
            WriteInteger(ItemId);
        }
    }
}