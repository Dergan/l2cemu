﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_UseSkill : SendBasePacket
    {
        private L2Skill Skill;
        L2Player client;
        public S_UseSkill(L2Player client, L2Skill skill)
            : base()
        {
            this.Skill = skill;
            this.client = client;
        }

        protected internal override void Write()
        {
            WriteByte(0x48);
            WriteInteger(0);
            WriteInteger(client.ObjectId);
            WriteInteger(client.Target.ObjectId);
            WriteInteger(Skill.ID);

            if(Skill.Level <= 0)
                WriteInteger(1);
            else
                WriteInteger(Skill.Level);

            WriteInteger(Skill.HitTime); //hit time
            WriteInteger(0); //hit time
            WriteInteger(Skill.ReuseDelay); //reuse delay
            WriteInteger(client.Position.X);
            WriteInteger(client.Position.Y);
            WriteInteger(client.Position.Z);
            WriteInteger(0);
            WriteInteger(client.Target.Position.X);
            WriteInteger(client.Target.Position.Y);
            WriteInteger(client.Target.Position.Z);
            WriteInteger(0);
            WriteInteger(0);
        }
    }
}