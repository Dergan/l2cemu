﻿using System.Collections.Generic;
using BinarySerialization;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_AbnormalStatusUpdate : L2SendBaseFrame
    {
        public class Buff
        {
            public int Id { get; set; }
            public ushort Level { get; set; }
            public uint Durarion { get; set; }
        }

        [FieldOrder(0)]
        public ushort BuffCount { get; set; }

        [FieldOrder(1)]
        [FieldCount("BuffCount")]
        public List<Buff> Buffs { get; set; }

        public override void Run(GameClient client, params object[] parameters)
        {
        }
    }
}