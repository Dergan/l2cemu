﻿namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_MoveToPosition : L2SendBaseFrame
    {
        public uint ObjectId { get; set; }
        public uint TargetX { get; set; }
        public uint TargetY { get; set; }
        public uint TargetZ { get; set; }

        public uint CurrentX { get; set; }
        public uint CurrentY { get; set; }
        public uint CurrentZ { get; set; }
    }
}
