﻿using System.Collections.Generic;
using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class SkillList : SendBaseFrame
    {
        [FieldOrder(0)]
        public uint SkillCount { get; set; }

        [FieldOrder(1)]
        [FieldCount(nameof(SkillCount))]
        public List<byte> Items { get; set; }

        [FieldOrder(2)]
        public uint NewLearned { get; set; }
    }
}
