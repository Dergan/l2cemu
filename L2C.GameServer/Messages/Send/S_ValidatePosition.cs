﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.GameServer.Objects;

namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_ValidatePos : L2SendBaseFrame
    {
        L2Player chr;
        public S_ValidatePos(L2Player character)
            : base()
        {
            this.chr = character;
        }

        protected internal override void Write()
        {
            WriteByte(0x79);
            WriteInteger(chr.ObjectId);
            WriteInteger(chr.Position.X);
            WriteInteger(chr.Position.Y);
            WriteInteger(chr.Position.Z);
            WriteInteger(chr.Position.Heading);
        }
    }
}