﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ClanMemberInfo : SendBasePacket
    {
        private L2Player player;
        public S_ClanMemberInfo(L2Player player)
            : base()
        {
            this.player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteShort(0x3E);

            WriteInteger(player.PledgeType);
            WriteString(player.Name);
            WriteString(player.Title);
            WriteInteger(0); //power grade

            WriteString(player.clan.ClanName);
            WriteString(""); //sponsor
        }
    }
}