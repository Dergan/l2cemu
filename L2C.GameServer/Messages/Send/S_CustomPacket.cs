﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;
using System.IO;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_CustomPacket : SendBasePacket
    {
        private byte[] bytes;

        public S_CustomPacket(byte[] bytes)
            : base()
        {
            this.bytes = bytes;
        }

        public S_CustomPacket(string bytes)
            : base()
        {

            using (MemoryStream ms = new MemoryStream())
            {
                for (int i = 0; i < bytes.Length; i+=2)
                {
                    string b = bytes[i].ToString() + bytes[i + 1].ToString();
                    ms.WriteByte(byte.Parse(b, System.Globalization.NumberStyles.HexNumber));
                }
                this.bytes = ms.ToArray();
            }

        }

        protected internal override void Write()
        {
            WriteBytes(bytes);
        }
    }
}