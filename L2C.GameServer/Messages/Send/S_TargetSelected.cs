﻿namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_TargetSelected : L2SendBaseFrame
    {
        public enum Colors : short
        {
            Blue = -9,
            LightBlue = -5,
            Green = -3,
            White = 1,
            Yellow = 3,
            LightRed = 6,
            Red = 9
        }

        public uint TargetObjectId { get; set; }
        public Colors TargetColor { get; set; }
        public uint Unk1 { get; } = 0;
    }
}