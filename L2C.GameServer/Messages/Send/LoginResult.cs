﻿using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class LoginResult : SendBaseFrame
    {

        [FieldOrder(0)]
        public uint Unk1 { get; } = 0xFFFFFFFF;

        [FieldOrder(1)]
        public uint Unk2 { get; } = 0x00000000;
    }
}