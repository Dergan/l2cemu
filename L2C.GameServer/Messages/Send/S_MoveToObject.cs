﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_MoveToObject : SendBasePacket
    {
        L2Entity client;
        int TargetX;
        int TargetY;
        int TargetZ;
        int ObjectId;

        public S_MoveToObject(L2Entity client)
            : base()
        {
            this.client = client;
            this.TargetX = client.Target.Position.X;
            this.TargetY = client.Target.Position.Y;
            this.TargetZ = client.Target.Position.Z;
            this.ObjectId = client.Target.ObjectId;
        }

        public S_MoveToObject(L2Entity client, int TargetX, int TargetY, int TargetZ, int ObjectId)
            : base()
        {
            this.client = client;
            this.TargetX = TargetX;
            this.TargetY = TargetY;
            this.TargetZ = TargetZ;
            this.ObjectId = ObjectId;
        }

        protected internal override void Write()
        {
            WriteByte(0x72);
            WriteInteger(client.ObjectId);
            WriteInteger(ObjectId);
            WriteInteger((int)Formulas.Distance3D(client.Position, new L2Position(TargetX, TargetY, TargetZ, 0)));

            WriteInteger(client.Position.X);
            WriteInteger(client.Position.Y);
            WriteInteger(client.Position.Z);

            WriteInteger(TargetX);
            WriteInteger(TargetY);
            WriteInteger(TargetZ);
        }
    }
}