﻿namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_KeyPacket : L2SendBaseFrame
    {
        public bool ProtocolOk { get; set; }
        public byte[] BlowfishKey { get; set; }
        public int Unk1 { get; } = 1;
        public int ServerId { get; set; }
        public byte Unk2 { get; } = 1;
        public int ObfuscationKey { get; } = 0;
    }
}
