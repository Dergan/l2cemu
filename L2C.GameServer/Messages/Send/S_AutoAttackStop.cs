﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_AutoAttackStop : SendBasePacket
    {
        int objectid;
        public S_AutoAttackStop(int objectid)
            : base()
        {
            this.objectid = objectid;
        }

        protected internal override void Write()
        {
            WriteByte(0x26);
            WriteInteger(objectid);
        }
    }
}