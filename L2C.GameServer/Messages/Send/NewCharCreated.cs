﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_NewCharCreated : SendBasePacket
    {
        public S_NewCharCreated()
            : base()
        {
        }

        protected internal override void Write()
        {
            WriteByte(0x0f);
            WriteInteger(1);
        }
    }
}