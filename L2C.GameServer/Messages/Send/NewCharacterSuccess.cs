﻿using System.Collections.Generic;
using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class NewCharacterSuccess : SendBaseFrame
    {
        [FieldOrder(0)]
        public uint TemplateCount { get; set; }
        [FieldOrder(1)]
        public List<NewCharacterTemplate> Templates { get; set; }

    }

    class NewCharacterTemplate
    {
        [FieldOrder(0)]
        public uint Race { get; set; }
        [FieldOrder(1)]
        public uint Class { get; set; }

        [FieldOrder(2)]
        public MaxRecMin STR { get; set; } = new MaxRecMin();
        [FieldOrder(3)]
        public MaxRecMin DEX { get; set; } = new MaxRecMin();
        [FieldOrder(4)]
        public MaxRecMin CON { get; set; } = new MaxRecMin();
        [FieldOrder(5)]
        public MaxRecMin INT { get; set; } = new MaxRecMin();
        [FieldOrder(6)]
        public MaxRecMin WIT { get; set; } = new MaxRecMin();
        [FieldOrder(7)]
        public MaxRecMin MEN { get; set; } = new MaxRecMin();
        [FieldOrder(8)]
        public MaxRecMin LUC { get; set; } = new MaxRecMin();
        [FieldOrder(9)]
        public MaxRecMin CHA { get; set; } = new MaxRecMin();

    }

    class MaxRecMin
    {
        [FieldOrder(0)]
        public uint Maximum { get; set; }
        [FieldOrder(1)]
        public uint Recommended { get; set; }
        [FieldOrder(2)]
        public uint Minimum { get; set; }
    }
}
