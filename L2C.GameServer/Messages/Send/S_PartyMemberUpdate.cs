﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_PartyMemberUpdate : SendBasePacket
    {
        private L2Player _player;
        public S_PartyMemberUpdate(L2Player player)
            : base()
        {
            this._player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0x52);

            WriteInteger(_player.ObjectId);
            WriteString(_player.Name);
            WriteInteger((int)_player.CurCP);
            WriteInteger((int)_player.MaxCP);
            WriteInteger((int)_player.CurHP);
            WriteInteger((int)_player.MaxHP);
            WriteInteger((int)_player.CurMP);
            WriteInteger((int)_player.MaxMP);
            WriteInteger((int)_player.Level);
            WriteInteger((int)_player.ClassId);
        }
    }
}