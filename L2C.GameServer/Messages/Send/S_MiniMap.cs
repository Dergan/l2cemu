﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_MiniMap : SendBasePacket
    {
        public S_MiniMap()
            : base()
        {
        }

        protected internal override void Write()
        {
            WriteByte(0xa3);
            WriteInteger(1665);
            WriteByte(0); //Seven signs
        }
    }
}