﻿using L2C.Game.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ExUserInfoInvenWeight : SendBasePacket
    {
        L2Player player;

        public S_ExUserInfoInvenWeight(L2Player player)
            : base()
        {
            this.player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteByte(0x66);
            WriteByte(0x01);

            WriteInteger(player.ObjectId);
            WriteInteger(1);
            WriteInteger(50);
        }
    }
}