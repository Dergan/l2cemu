﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Collections;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ExBrUserInfo : SendBasePacket
    {
        private L2Player player;

        public S_ExBrUserInfo(L2Player player)
            : base()
        {
            this.player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0xFE);
            WriteShort(0x0160);

            WriteInteger(player.ObjectId);
            long totalLength = this.Length;
            WriteInteger(0x00);//Length Placeholder

            WriteByte(0x17); //?????
            //WriteInteger(0x00BC7800);
            WriteByte(0x00);
            WriteByte(0xFF);
            WriteByte(0xFF);
            WriteByte(0xFE);

            WriteInteger(0x00);//??            

            long curPos = this.Length;

            //Name
            WriteShort(0x00); //Length Placeholder
            WriteShort((short)player.Name.Length);
            WriteString(player.Name);
            WriteByte(0);//???
            WriteInteger(player.ClassId);//ClassId
            WriteInteger(0x0C);//??
            WriteByte((byte)player.Level);

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);


            //Modifiers
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteShort(0x01);//(short)player.STR);
            WriteShort(0x02);//(short)player.DEX);
            WriteShort(0x03);//(short)player.CON);
            WriteShort(0x04);//(short)player.INT);
            WriteShort(0x05);//(short)player.WIT);
            WriteShort(0x06);//(short)player.MEN);

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);


            //HP/MP/CP
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteInteger((short)player.CurHP);
            WriteInteger((short)player.CurMP);
            WriteInteger((short)player.CurCP);

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);


            //HP/MP/CP/SP ???????
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteInteger((short)player.CurHP);
            WriteInteger((short)player.CurMP);
            WriteInteger((short)player.CurCP);
            WriteInteger((short)player.SP);
            WriteInteger(0x00);//??
            WriteInteger(24878165);//??
            WriteInteger(0x00);//??
            WriteLong(4602897903408518401);//??
            WriteByte(0x00);
            WriteShort(0);
            WriteShort(0x0F);
            WriteInteger(2);
            WriteInteger(2);
            WriteInteger(0);
            WriteByte(0);

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //??? Unkown
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteByte(0x00);
            WriteByte(0x00);
            WriteByte(0x00);

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);


            //Stats
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteShort(0x14);
            WriteInteger(player.Patk);
            WriteInteger(player.AttackSpeed);
            WriteInteger(player.Pdef);
            WriteInteger(player.Evasion);
            WriteInteger(player.Accuracy);
            WriteInteger(player.Critical);
            WriteInteger(player.MAtk);
            WriteInteger(player.CastSpeed);
            WriteInteger(player.AttackSpeed);
            WriteInteger(0x01); //M.Evasion
            WriteInteger(player.Mdef);
            WriteInteger(0x02); //M.Accuracy
            WriteInteger(0x03); //M.Critical

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);


            //??? Unkown
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteInteger(0x00);
            WriteInteger(0x00);
            WriteInteger(0x00);

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);



            //Position
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteInteger(player.Position.X);
            WriteInteger(player.Position.Y);
            WriteInteger(player.Position.Z);
            WriteInteger(player.Position.Heading);

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //????
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteShort(0x7C);
            WriteShort(0x4E);
            WriteShort(0x32);
            WriteShort(0x32);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00); 

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Multipliers
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteDouble(1.26935483870968d); //Multiplier? Atk? Speed?
            WriteDouble(1.7902177058304d); //Multiplier? Atk? Speed?

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Collision
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteDouble(7.5d); //Collision Height?
            WriteDouble(22.8d); //Collision Width?

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Unkown?
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteByte(0xFE);
            WriteByte(0x00);
            WriteByte(0x00);

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Unkown?
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteBytes("000000000000000000000000000000000000000000000000000000000000");

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Unkown?
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteBytes("0000000000000000000000000000000014000000");

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Unkown?
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteBytes("E02202000000000000");

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Unkown?
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteBytes("00000000000000");

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Unkown?
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteBytes("0001");

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Unkown?
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteBytes("FFFFFF00A2F9EC00");

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Unkown?
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteBytes("00000000500000");

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);

            //Unkown?
            curPos = this.Length;
            WriteShort(0x00); //Length Placeholder
            WriteBytes("0100000000000000000000");

            SetPosition(curPos);
            WriteShort((short)(this.Length - curPos));
            SetPosition(this.Length);



            //Set Length Total Packet
            SetPosition(totalLength);
            WriteInteger((int)(this.Length - totalLength));
            SetPosition(this.Length);
        }
    }
}