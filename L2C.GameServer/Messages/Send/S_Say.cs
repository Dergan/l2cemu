﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_Say : SendBasePacket
    {
        L2Say msg;
        public S_Say(L2Say message)
            : base()
        {
            msg = message;
        }

        protected internal override void Write()
        {
            WriteByte(0x4a);
            WriteInteger(msg.player.ObjectId);
            WriteInteger(msg.type);
            WriteString(msg.Messanger);
            WriteUInteger(0xFFFFFFFF);
            WriteString(msg.Message);
        }
    }
}