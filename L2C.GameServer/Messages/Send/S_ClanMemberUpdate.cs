﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ClanMemberUpdate : SendBasePacket
    {
        private L2Player player;
        private bool isOnline;
        public S_ClanMemberUpdate(L2Player player, bool isOnline)
            : base()
        {
            this.player = player;
            this.isOnline = isOnline;
        }

        protected internal override void Write()
        {
            WriteByte(0x5B);
            WriteString(player.Name);
            WriteInteger(player.Level);
            WriteInteger(player.ClassId);
            WriteInteger(player.Gender);
            WriteInteger(player.RaceId);
            if (isOnline)
            {
                WriteInteger(player.ObjectId);
                WriteInteger(player.PledgeType);
            }
            else
            {
                WriteInteger(0);
                WriteInteger(0);
            }
            WriteInteger(0); //sponsor
        }
    }
}