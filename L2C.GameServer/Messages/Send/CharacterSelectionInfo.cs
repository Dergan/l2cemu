﻿using System.Collections.Generic;
using BinarySerialization;

namespace L2C.GameServer.Messages.Send
{
    class CharacterSelectionInfo : SendBaseFrame
    {
        [FieldOrder(0)]
        public int PlayerCount { get; set; }

        [FieldOrder(1)]
        public int MaxCharacters { get; set; } = 7;

        [FieldOrder(2)]
        public bool CanCreateCharacter { get; set; } = false;

        [FieldOrder(3)]
        public byte PlayMode { get; set; } = 2;//1 create only, 2 char in  regular lobby

        [FieldOrder(4)]
        public int IsKoreaClient { get; } = 2;

        [FieldOrder(5)]
        public ushort SuggestPremiumAccount { get; }

        [FieldOrder(6)]
        [FieldCount(nameof(PlayerCount))]
        public List<CharacterInfo> Characters { get; set; } = new List<CharacterInfo>();

        public class Appearance
        {
            [FieldOrder(0)]
            public int Gender { get; set; }
            [FieldOrder(1)]
            public int RaceId { get; set; }
            [FieldOrder(2)]
            public int ClassId { get; set; }
        }

        public class HealthMana
        {
            [FieldOrder(0)]
            public double HP { get; set; }
            [FieldOrder(1)]
            public double MP { get; set; }
        }

        public class BasicInfo
        {
            [FieldOrder(0)]
            public long SP { get; set; }
            [FieldOrder(1)]
            public long EXP { get; set; }
            [FieldOrder(2)]
            public double ExpPercent { get; set; }
            [FieldOrder(3)]
            public int Level { get; set; }

            [FieldOrder(4)]
            public int Karma { get; set; }
            [FieldOrder(5)]
            public int PK { get; set; }
            [FieldOrder(6)]
            public int PVP { get; set; }
        }

        public class Unknown603
        {
            [FieldOrder(0)]
            public int Unk2 { get; }
            [FieldOrder(1)]
            public int Unk3 { get; }
            [FieldOrder(2)]
            public int Unk4 { get; }
            [FieldOrder(3)]
            public int Unk5 { get; }
            [FieldOrder(4)]
            public int Unk6 { get; }
            [FieldOrder(5)]
            public int Unk7 { get; }
            [FieldOrder(6)]
            public int Unk8 { get; }
            [FieldOrder(7)]
            public int Unk9 { get; }
            [FieldOrder(8)]
            public int Unk10 { get; }
        }

        public class AdditionalItems
        {
            [FieldOrder(0)]
            public uint[] Talisman { get; } = new uint[6];
            [FieldOrder(1)]
            public uint Belt { get; }
            [FieldOrder(2)]
            public uint Brooch { get; }
            [FieldOrder(3)]
            public uint[] Jewels { get; } = new uint[6];
        }

        public class HeadAppearance
        {
            [FieldOrder(0)]
            public int HairStyle { get; set; }
            [FieldOrder(1)]
            public int HairColor { get; set; }
            [FieldOrder(2)]
            public int Face { get; set; }
        }

        public class PedInfo
        {
            [FieldOrder(0)]
            public int PedId { get; set; }
            [FieldOrder(1)]
            public int PedLevel { get; set; }
            [FieldOrder(2)]
            public int PedFood { get; set; }
            [FieldOrder(3)]
            public int PedFoodLevel { get; set; }
            [FieldOrder(4)]
            public double PedMaxHP { get; set; }
            [FieldOrder(5)]
            public double PedMaxMP { get; set; }
        }

        public class VitalityInfo
        {
            [FieldOrder(0)]
            public int VitalityPoints { get; set; } = 140000;
            [FieldOrder(1)]
            public int VitalityExpBonus { get; set; } = 200;//Freya
            [FieldOrder(2)]
            public int VitalityItemsUsed { get; set; } = 10;
        }

        public class EnchantmentLevels
        {
            [FieldOrder(0)]
            public ushort UpperBody { get; set; }
            [FieldOrder(1)]
            public ushort LowerBody { get; set; }
            [FieldOrder(2)]
            public ushort Headgear { get; set; }
            [FieldOrder(3)]
            public ushort Gloves { get; set; }
            [FieldOrder(4)]
            public ushort Boots { get; set; }
        }

        public class CharacterInfo
        {
            [FieldOrder(0)]
            public SerializedString Name { get; set; }

            [FieldOrder(1)]
            public int ObjectId { get; set; }

            [FieldOrder(2)]
            public SerializedString LoginName { get; set; } = "L2C Wins";
            [FieldOrder(3)]
            public int SessionId { get; set; } = 1;
            [FieldOrder(4)]
            public int PledgeId { get; set; }
            [FieldOrder(5)]
            public int BuilderLevel { get; }

            [FieldOrder(6)]
            public Appearance Appearance { get; } = new Appearance();

            [FieldOrder(7)]
            public int ServerId { get; } = 1;

            [FieldOrder(8)]
            public CharacterSelectPosition Position { get; set; } = new CharacterSelectPosition();

            [FieldOrder(11)]
            public HealthMana Current { get; set; } = new HealthMana();

            [FieldOrder(12)]
            public BasicInfo BasicInfo { get; set; } = new BasicInfo();

            //Unkown 603
            [FieldOrder(13)]
            public Unknown603 Unk2 { get; } = new Unknown603();

            //ItemIds
            [FieldOrder(14)]
            public CharacterSelectEquipment EquippedItems { get; set; } = new CharacterSelectEquipment();

            [FieldOrder(15)]
            public AdditionalItems AdditionalItems { get; } = new AdditionalItems();

            [FieldOrder(16)]
            public uint[] Unk42 { get; } = new uint[27];

            [FieldOrder(17)]
            public CharacterSelectVisibleEquipment VisibleEquippedItems { get; set; } = new CharacterSelectVisibleEquipment();

            [FieldOrder(18)]
            public EnchantmentLevels EnchantmentLevels { get; set; } = new EnchantmentLevels();

            [FieldOrder(19)]
            public HeadAppearance HeadAppearance { get; set; } = new HeadAppearance();

            [FieldOrder(20)]
            public HealthMana Maximum { get; set; } = new HealthMana();

            [FieldOrder(21)]
            public int DeleteTime { get; set; }
            [FieldOrder(22)]
            public int ClassId2 { get; set; }
            [FieldOrder(23)]
            public int AutoSelect { get; set; } = 1;
            [FieldOrder(24)]
            public byte WeaponEnchantLevel { get; set; }
            [FieldOrder(25)]
            public int WeaponAugmentEffect1 { get; set; }
            [FieldOrder(26)]
            public int WeaponAugmentEffect2 { get; set; }
            [FieldOrder(27)]
            public int TransformID { get; set; }

            [FieldOrder(28)]
            public PedInfo PedInfo { get; set; } = new PedInfo();

            [FieldOrder(29)]
            public VitalityInfo VitalityPoints { get; set; } = new VitalityInfo();

            [FieldOrder(30)]
            public int IsActive2 { get; set; } = 1;
            [FieldOrder(31)]
            public bool IsNoble { get; set; }
            [FieldOrder(32)]
            public bool IsHero { get; set; }
            [FieldOrder(33)]
            public bool IsHairAccessoryEnabled { get; set; }

            public class CharacterSelectPosition
            {
                [FieldOrder(0)]
                public int X { get; set; }

                [FieldOrder(1)]
                public int Y { get; set; }

                [FieldOrder(2)]
                public int Z { get; set; }
            }
            public class CharacterSelectVisibleEquipment
            {
                [FieldOrder(0)]
                public int Weapon { get; }
                [FieldOrder(1)]
                public int Shield { get; }
                [FieldOrder(2)]
                public int Gloves { get; }
                [FieldOrder(3)]
                public int Upperbody { get; }
                [FieldOrder(4)]
                public int LowerBody { get; }
                [FieldOrder(5)]
                public int Boots { get; }
                [FieldOrder(6)]
                public int WeaponTwoHanded { get; }
                [FieldOrder(7)]
                public int HairAccessoryTop { get; }
                [FieldOrder(8)]
                public int HairAccessoryBottom { get; }
            }

            public class CharacterSelectEquipment
            {
                [FieldOrder(0)]
                public int Underwear { get; set; }
                [FieldOrder(1)]
                public int EarringLeft { get; set; }
                [FieldOrder(2)]
                public int EarringRight { get; set; }
                [FieldOrder(3)]
                public int Necklace { get; set; }
                [FieldOrder(4)]
                public int RingLeft { get; set; }
                [FieldOrder(5)]
                public int RingRight { get; set; }
                [FieldOrder(6)]
                public int Headgear { get; set; }
                [FieldOrder(7)]
                public int Weapon { get; set; }
                [FieldOrder(8)]
                public int Shield { get; set; }
                [FieldOrder(9)]
                public int Gloves { get; set; }
                [FieldOrder(10)]
                public int Upperbody { get; set; }
                [FieldOrder(11)]
                public int Lowerbody { get; set; }
                [FieldOrder(12)]
                public int Boots { get; set; }
                [FieldOrder(13)]
                public int Cloak { get; set; }
                [FieldOrder(14)]
                public int WeaponTwoHanded { get; set; }
                [FieldOrder(15)]
                public int HairAccessoryTop { get; set; }
                [FieldOrder(16)]
                public int HairAccessoryBottom { get; set; }
                [FieldOrder(17)]
                public int BraceletRight { get; set; }
                [FieldOrder(18)]
                public int BraceletLeft { get; set; }
            }
        }

    }
}