﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;
using L2C.Game.Collections;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_ShortCuts : SendBasePacket
    {
        private L2Player player;
        public S_ShortCuts(L2Player player)
            : base()
        {
            this.player = player;
        }

        protected internal override void Write()
        {
            WriteByte(0x45);
            WriteInteger(player.ShortCuts.Count);

            foreach (L2ShortcutInfo info in player.ShortCuts)
            {
                WriteInteger(info.type);
                WriteInteger(info.slot + info.page * 12);

                switch (info.type)
                {
                    case 1:
                        WriteInteger(info.id);
                        WriteInteger(1);
                        WriteInteger(0); //reuse group ?
                        WriteInteger(0);
                        WriteInteger(0);
                        WriteInteger(0);
                        WriteInteger(0);
                        break;
                    case 2:
                        WriteInteger(info.id);
                        WriteInteger(info.level);
                        WriteByte(0);
                        WriteInteger(1);
                        break;
                    case 3:
                    case 4:
                    case 5:
                        WriteInteger(info.id);
                        WriteInteger(1);
                        break;
                    default:
                        WriteInteger(info.id);
                        WriteInteger(1);
                        break;
                }
            }
        }
    }
}