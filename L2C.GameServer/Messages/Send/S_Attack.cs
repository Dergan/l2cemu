﻿namespace L2C.GameServer.Network.GameServer.Packets.Send
{
    public class S_Attack : L2SendBaseFrame
    {
        public enum AttackFlags : int
        {
            UseSS = 0x10,
            Critical = 0x20,
            Shield = 0x40,
            Miss = 0x80
        }

        public int ObjectId { get; set; }
        public int TargetObjectId { get; set; }
        public int Damage { get; set; }
        public AttackFlags Flags { get; set; }

        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public ushort HitCounter { get; set; }
        
        public int TargetX { get; set; }
        public int TargetY { get; set; }
        public int TargetZ { get; set; }
        

        //public override void Run(GameClient client, params object[] parameters)
        //{
        //    ObjectId = (client.PlayerInstance.ObjectId);
        //    TargetObjectId = (client.PlayerInstance.Target.ObjectId);
        //    Damage = (1337); //damage

        //    //Flags = ??

        //    X = (client.PlayerInstance.Position.X);
        //    Y = (client.PlayerInstance.Position.Y);
        //    Z = (client.PlayerInstance.Position.Z);

        //    HitCounter = (1); //Hit counter

        //    TargetX = (client.PlayerInstance.Target.Position.X);
        //    TargetY = (client.PlayerInstance.Target.Position.Y);
        //    TargetZ = (client.PlayerInstance.Target.Position.Z);
        //}
    }
}