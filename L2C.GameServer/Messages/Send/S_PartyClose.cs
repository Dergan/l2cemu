﻿using System;
using System.Collections.Generic;
using System.Text;
using L2C.Game.Objects;

namespace L2C.Game.Network.GameServer.Packets.Send
{
    public class S_PartyClose : SendBasePacket
    {
        public S_PartyClose()
            : base()
        {
        }

        protected internal override void Write()
        {
            WriteByte(0x50);
        }
    }
}