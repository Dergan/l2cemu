﻿using BinarySerialization;

namespace L2C.GameServer.Messages.SerializableTypes
{
    class Vector3 : Vector2
    {
        [FieldOrder(0)]
        public uint Z { get; set; }
    }
}