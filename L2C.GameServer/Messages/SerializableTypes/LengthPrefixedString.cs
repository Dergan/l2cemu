﻿using BinarySerialization;

namespace L2C.GameServer.Messages
{
    public class LengthPrefixedString
    {
        [FieldOrder(0)]
        public ushort Length { get; set; }

        [FieldOrder(1)]
        [FieldLength(nameof(Length), ConverterType = typeof(HalfLengthConverter))]
        public char[] ValueAsBytes { get; set; }

        [Ignore]
        public string Value
        {
            get => new string(ValueAsBytes);
            set => ValueAsBytes = value.ToCharArray();
        }

        public static implicit operator LengthPrefixedString(string d)
        {
            return new LengthPrefixedString() { Value = d };
        }

        public static implicit operator string(LengthPrefixedString d)
        {
            return d.Value;
        }

        class HalfLengthConverter : IValueConverter
        {
            public object Convert(object value, object parameter, BinarySerializationContext context)
            {
                return System.Convert.ToUInt16(value) * 2;
            }

            public object ConvertBack(object value, object parameter, BinarySerializationContext context)
            {
                return System.Convert.ToUInt16(value) / 2;
            }
        }
    }
}
