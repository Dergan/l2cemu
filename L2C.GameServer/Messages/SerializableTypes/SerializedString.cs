﻿using BinarySerialization;

namespace L2C.GameServer.Messages
{
    public class SerializedString
    {
        [FieldOrder(0)]
        [SerializeUntil('\0')]
        public char[] ValueAsBytes { get; set; }

        [Ignore]
        public string Value
        {
            get => new string(ValueAsBytes);
            set => ValueAsBytes = value.ToCharArray();
        }

        public static implicit operator SerializedString(string d)
        {
            return new SerializedString() { Value = d };
        }

        public static implicit operator string(SerializedString d)
        {
            return d.Value;
        }
    }
}
