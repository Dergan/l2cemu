﻿using BinarySerialization;

namespace L2C.GameServer.Messages.SerializableTypes
{
    class Vector2
    {
        [FieldOrder(0)]
        public uint X { get; set; }

        [FieldOrder(1)]
        public uint Y { get; set; }
    }
}