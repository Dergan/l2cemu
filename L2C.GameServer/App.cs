﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using DotNetty.Handlers.Logging;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using L2C.Cryptography;
using L2C.EventBus;
using L2C.GameServer.Events;
using L2C.GameServer.Extensions;
using L2C.GameServer.Handlers;
using L2C.GameServer.Messages;
using L2C.GameServer.Messages.Receive;
using L2C.GameServer.Messages.Receive.Extended;
using L2C.GameServer.Network;
using L2C.GameServer.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace L2C.GameServer
{
    internal class App
    {
        private readonly IEventBus _messageBroker;
        private readonly ISessionManager _sessionManager;
        private readonly IOptionsMonitor<GameServerOptions> _serverSettingsAccessor;
        private readonly ILogger<App> _logger;
        private ServerBootstrap _server;

        private GameServerOptions Settings => _serverSettingsAccessor.CurrentValue;

        public App(IEventBus messageBroker,
            ISessionManager sessionManager,
            IEnumerable<IMessageHandler> messageHandlers,
            IEnumerable<IConsumer> consumers,
            IOptionsMonitor<GameServerOptions> serverSettingsAccessor,
            ILogger<App> logger)
        {
            _messageBroker = messageBroker;
            _sessionManager = sessionManager;
            _serverSettingsAccessor = serverSettingsAccessor;
            _serverSettingsAccessor.OnChange(OnSettingsChangeListener);
            _logger = logger;

            _logger.LogInformation("Starting Game Gateway");

            _logger.LogInformation("Bootstrapping dotNetty");
            _server = new ServerBootstrap();

            _logger.LogInformation("Initialize EventLoop Groups");
            var bossGroup = new MultithreadEventLoopGroup();
            var workerGroup = new MultithreadEventLoopGroup();
            var keyGenerator = new XorKeyGenerator();

            _logger.LogInformation("Setup network pipeline");
            _server.Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .Handler(new LoggingHandler("LSTN"))
                .Option(ChannelOption.SoBacklog, 8192)
                .ChildHandler(new ActionChannelInitializer<IChannel>(channel =>
                {
                    var xorCrypt = new XorCrypt();
                    var xorKey = keyGenerator.Generate();
                    xorCrypt.SetKey(xorKey);

                    var pipeline = channel.Pipeline
                                          .AddLengthPrefix()
                                          .AddCryption(xorCrypt)
                                          .AddLast(new LoggingHandler("CONN"))
                                          .AddBinarySerializer<ReceiveMessage, SendMessage>()
                                          .AddFrameWrapper()
                                          .AddExtendedFrameWrapper()
                                          .AddHandler(new SessionHandler(_sessionManager, xorKey));

                    foreach (var handler in messageHandlers)
                    {
                        pipeline.AddLast(new DotNettyMessageHandlerWrapper(handler, _sessionManager));
                    }

                    pipeline.AddHandler(new MessageToEventHandler(_messageBroker, _sessionManager));
                }));

            foreach (var handler in consumers)
            {
                _messageBroker.SubscribeAsync(handler.EventType, handler.HandleAsync);
            }
        }

        private void OnSettingsChangeListener(GameServerOptions settings)
        {
        }

        public async Task Run()
        {
            _logger.LogInformation($"Binding to endpoint {Settings.Host}:{Settings.Port}");
            IChannel channel = await _server.BindAsync(IPAddress.Parse(Settings.Host), Settings.Port);

            _logger.LogInformation("Game Gateway started, waiting for connections");
            Process.GetCurrentProcess().WaitForExit();
        }
    }

    class MessageToEventHandler : ChannelHandlerAdapter
    {
        private IMapper _mapper;

        private Dictionary<Type, Type> _messageToEventTypeMapping = new Dictionary<Type, Type>();
        private readonly IEventBus _eventBus;
        private readonly ISessionManager _sessionManager;

        public MessageToEventHandler(IEventBus eventBus, ISessionManager sessionManager)
        {
            AddTypeMapping<SendProtocolVersion, SwitchProtocolEvent>();
            AddTypeMapping<RequestLogin, GameAuthenticationEvent>();
            AddTypeMapping<RequestGotoLobby, RequestLobbyEvent>();
            AddTypeMapping<RequestGameStart, CharacterSelectedEvent>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;
                cfg.CreateMap<SendProtocolVersion, SwitchProtocolEvent>()
                        .ForMember(dst => dst.Version, opt => opt.MapFrom(src => src.Version));

                cfg.CreateMap<RequestLogin, GameAuthenticationEvent>();
                cfg.CreateMap<RequestGotoLobby, RequestLobbyEvent>();
                cfg.CreateMap<RequestGameStart, CharacterSelectedEvent>();
            });
            _mapper = config.CreateMapper();
            _eventBus = eventBus;
            _sessionManager = sessionManager;
        }

        private void AddTypeMapping<TMessage, TEvent>()
            where TMessage : ReceiveBaseFrame
            where TEvent : IEvent
        {
            _messageToEventTypeMapping.Add(typeof(TMessage), typeof(TEvent));
        }

        public override void ChannelRead(IChannelHandlerContext context, object message)
        {
            if (!_messageToEventTypeMapping.ContainsKey(message.GetType()))
            {
                base.ChannelRead(context, message);
                return;
            }

            var evtType = _messageToEventTypeMapping[message.GetType()];
            var mapped = _mapper.Map(message, message.GetType(), evtType);
            var session = _sessionManager.FindSession(context.Channel.Id.AsLongText());
            switch (mapped)
            {
                case L2C.Shared.Events.SessionBasedEvent sessionBasedEvent:
                    sessionBasedEvent.SessionId = session.Id.ToString();
                    break;
                case L2C.Shared.Events.ObjectIdBasedEvent objectBasedEvent:
                    objectBasedEvent.ObjectId = session.ObjectId;
                    break;
            }
            _eventBus.PublishAsync(mapped);
            base.ChannelReadComplete(context);
        }
    }

    class DotNettyMessageHandlerWrapper : ChannelHandlerAdapter
    {
        private readonly IMessageHandler _handler;
        private readonly ISessionManager _sessionManager;

        public DotNettyMessageHandlerWrapper(IMessageHandler handler, ISessionManager sessionManager)
        {
            _handler = handler;
            _sessionManager = sessionManager;
        }

        public override void ChannelRead(IChannelHandlerContext context, object message)
        {
            if (message.GetType() != _handler.MessageType)
            {
                base.ChannelRead(context, message);
                return;
            }

            var session = _sessionManager.FindSession(context.Channel.Id.AsLongText());
            _ = _handler.Handle(session, message);
            base.ChannelReadComplete(context);
        }
    }
}
