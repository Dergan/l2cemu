﻿using System;
using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using L2C.Cryptography;
using L2C.GameServer.Codecs;

namespace L2C.GameServer.Extensions
{
    internal static class PipelineExtensions
    {

        internal static IChannelPipeline AddLengthPrefix(this IChannelPipeline pipeline)
            => pipeline.AddLast(new LengthFieldPrepender(ByteOrder.LittleEndian, 2, 0, true))
                       .AddLast(new LengthFieldBasedFrameDecoder(ByteOrder.LittleEndian, 8192, 0, 2, -2, 2, true));

        internal static IChannelPipeline AddCryption(this IChannelPipeline pipeline, ICryptEngine cryption)
            => pipeline.AddLast(new CryptionCodec(cryption));

        internal static IChannelPipeline AddBinarySerializer<TReceive, TSend>(this IChannelPipeline pipeline)
            => pipeline.AddLast(new BinarySerializerDecoder<TReceive>())
                       .AddLast(new BinarySerializerEncoder<TSend>());

        internal static IChannelPipeline AddFrameWrapper(this IChannelPipeline pipeline)
            => pipeline.AddLast(new FrameWrapper());

        internal static IChannelPipeline AddExtendedFrameWrapper(this IChannelPipeline pipeline)
            => pipeline.AddLast(new ExtendedFrameWrapper());

        internal static IChannelPipeline AddHandler(this IChannelPipeline pipeline, IChannelHandler handler)
            => pipeline.AddLast(handler);

        internal static IChannelPipeline AddHandler<TChannelHandler>(this IChannelPipeline pipeline)
            where TChannelHandler : IChannelHandler
            => pipeline.AddLast(Activator.CreateInstance<TChannelHandler>());

        internal static IChannelPipeline AddHandler<TChannelHandler>(this IChannelPipeline pipeline, Func<TChannelHandler> construct)
            where TChannelHandler : IChannelHandler
            => pipeline.AddLast(construct());


    }
}
