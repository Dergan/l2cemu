﻿using System.Threading.Tasks;
using L2C.EventBus;
using L2C.GameServer.Events;
using L2C.GameServer.Messages.Receive;
using L2C.GameServer.Messages.Send.Extended;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    class RequestEnterWorldHandler : BaseMessageHandler<RequestEnterWorld>
    {
        private readonly IEventBus _eventBus;

        public RequestEnterWorldHandler(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public override async Task Handle(ISession ctx, RequestEnterWorld message)
        {
            await ctx.SendAsync(new ExEnterWorld());
            await _eventBus.PublishAsync(new EnterWorldEvent(ctx.ObjectId));
        }
    }
}
