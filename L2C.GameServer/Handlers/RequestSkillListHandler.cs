﻿using System.Threading.Tasks;
using L2C.GameServer.Messages.Receive;
using L2C.GameServer.Messages.Send;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    class RequestSkillListHandler : BaseMessageHandler<RequestSkillList>
    {


        public override async Task Handle(ISession ctx, RequestSkillList message)
        {
            await ctx.SendAsync(new SkillList()
            {

            });
        }
    }
}
