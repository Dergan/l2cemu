﻿using System.Threading.Tasks;
using L2C.GameServer.Messages.Receive.Extended;
using L2C.GameServer.Messages.Send.Extended;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    class RequestEx2ndPasswordHandler : BaseMessageHandler<RequestEx2ndPasswordCheck>
    {

        public RequestEx2ndPasswordHandler()
        {
        }

        public override async Task Handle(ISession ctx, RequestEx2ndPasswordCheck msg)
        {
            await ctx.SendAsync(new Ex2ndPasswordVerify());
        }
    }
}
