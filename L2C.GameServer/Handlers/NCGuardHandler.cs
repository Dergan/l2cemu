﻿using System.Threading.Tasks;
using L2C.GameServer.Messages.Receive.Extended;
using L2C.GameServer.Messages.Send.Extended;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    class NCGuardHandler : BaseMessageHandler<NCGuardSendDataToServer>
    {

        public NCGuardHandler()
        {
        }

        public override async Task Handle(ISession ctx, NCGuardSendDataToServer message)
        {
            await ctx.SendAsync(new ExNCGuardReceiveDataFromServer()
            {
                Data = message.Data
            });
        }
    }
}
