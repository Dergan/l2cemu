﻿using System;
using System.Threading.Tasks;
using L2C.GameServer.Messages;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    abstract class BaseMessageHandler<TMessage> : IMessageHandler<TMessage>
        where TMessage : ReceiveBaseFrame
    {
        public Type MessageType => typeof(TMessage);
        public Task Handle(ISession ctx, object message) => Handle(ctx, message as TMessage);
        public abstract Task Handle(ISession context, TMessage message);
    }
}
