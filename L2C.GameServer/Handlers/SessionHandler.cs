﻿using DotNetty.Transport.Channels;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    internal class SessionHandler : ChannelHandlerAdapter
    {
        private readonly ISessionManager _sessionManager;
        private readonly byte[] _blowfishKey;

        public SessionHandler(ISessionManager sessionManager, byte[] blowfishKey)
        {
            _sessionManager = sessionManager;
            _blowfishKey = blowfishKey;
        }

        public override void ChannelActive(IChannelHandlerContext context)
        {
            base.ChannelActive(context);
            _sessionManager.Add(new DotNettySession(context.Channel, _blowfishKey));
        }

        public override void ChannelInactive(IChannelHandlerContext context)
        {
            base.ChannelInactive(context);

            var sessionId = new DotNettySessionId(context.Channel.Id);
            _sessionManager.Remove(sessionId);
        }
    }
}
