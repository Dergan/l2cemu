﻿using System.Threading.Tasks;
using L2C.EventBus;
using L2C.GameServer.Events;
using L2C.GameServer.Messages.Receive;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    class MoveToLocationHandler : BaseMessageHandler<MoveBackwardToLocation>
    {
        private readonly IEventBus _eventBus;

        public MoveToLocationHandler(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public override async Task Handle(ISession ctx, MoveBackwardToLocation message)
        {
            //TODO: Direct send back move packet when using arrow keys
            //if (message.ControllerType == 0)
            //{
            await _eventBus.PublishAsync(new EntityMoveToLocationEvent(ctx.ObjectId)
            {
                Destination = new Shared.Core.Vector3()
                {
                    X = message.Destination.X,
                    Y = message.Destination.Y,
                    Z = message.Destination.Z,
                },
                Source = new Shared.Core.Vector3()
                {
                    X = message.Current.X,
                    Y = message.Current.Y,
                    Z = message.Current.Z,
                }
            });
            //}
            //else
            //{
            //    await ctx.SendAsync(new MoveToLocation()
            //    {
            //        ObjectId = ctx.ObjectId,
            //        Destination = message.Destination,
            //        Current = message.Current
            //    });
            //}
        }
    }
}
