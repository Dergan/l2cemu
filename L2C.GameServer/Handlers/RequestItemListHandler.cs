﻿using System.Threading.Tasks;
using L2C.GameServer.Messages.Receive;
using L2C.GameServer.Messages.Send;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    class RequestItemListHandler : BaseMessageHandler<RequestItemList>
    {


        public override async Task Handle(ISession ctx, RequestItemList message)
        {
            await ctx.SendAsync(new ItemList()
            {
                OpenWindow = 2
            });
        }
    }
}
