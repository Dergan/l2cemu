﻿using System.Threading.Tasks;
using L2C.EventBus;
using L2C.GameServer.Events;
using L2C.GameServer.Messages.Receive;
using L2C.GameServer.Messages.Send;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    class RequestGameStartHandler : BaseMessageHandler<RequestGameStart>
    {
        private readonly IEventBus _eventBus;

        public RequestGameStartHandler(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public override async Task Handle(ISession ctx, RequestGameStart message)
        {
            ctx.ObjectId = 1;
            await ctx.SendAsync(new CharacterSelected()
            {
                SessionId = 1,
                ObjectId = ctx.ObjectId,
                Name = "Test",
                ClassId = 0,
                ClassId2 = 0,
                CurrentHP = 100d,
                CurrentMP = 200d,
                EXP = 10L,
                Gender = 0,
                IsActive = 1,
                Karma = 0,
                Level = 1,
                PK = 0,
                PledgeId = 0,
                RaceId = 0,
                SP = 100L,
                Title = "Testing",
                //X = 11640,
                //Y = 59286,
                //Z = -3424,
                X = -14453,
                Y = 123484,
                Z = -3424,
            });

            await _eventBus.PublishAsync(new CharacterSelectedEvent(ctx.Id.ToString())
            {
                ObjectId = ctx.ObjectId,
                Name = "Test",
                ClassId = 0,
                CurrentHP = 100d,
                CurrentMP = 200d,
                EXP = 10L,
                Gender = 0,
                Karma = 0,
                Level = 1,
                PK = 0,
                PledgeId = 0,
                RaceId = 0,
                SP = 100L,
                Title = "Testing",
                //X = 11640,
                //Y = 59286,
                //Z = -3424,
                X = -14453,
                Y = 123484,
                Z = -3424,
            });
        }
    }
}
