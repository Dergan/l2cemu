﻿using System.Threading.Tasks;
using L2C.GameServer.Messages.Receive.Extended;
using L2C.GameServer.Messages.Send.Extended;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{
    class RequestQueueTicketLoginHandler : BaseMessageHandler<RequestQueueTicketLogin>
    {


        public override async Task Handle(ISession ctx, RequestQueueTicketLogin message)
        {
            await ctx.SendAsync(new ExQueueTicketLogin());
            await ctx.SendAsync(new ExQueueTicket());
        }
    }
}
