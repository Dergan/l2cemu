﻿using System;
using System.Threading.Tasks;
using L2C.GameServer.Messages;
using L2C.GameServer.Network;

namespace L2C.GameServer.Handlers
{

    interface IMessageHandler
    {
        Type MessageType { get; }
        Task Handle(ISession context, object message);
    }

    interface IMessageHandler<TMessage> : IMessageHandler
        where TMessage : ReceiveBaseFrame
    {

        Task Handle(ISession context, TMessage message);
    }
}
