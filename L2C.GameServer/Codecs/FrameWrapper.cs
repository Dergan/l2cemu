﻿using System.Collections.Generic;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using L2C.GameServer.Messages;

namespace L2C.GameServer.Codecs
{



    class FrameWrapper : MessageToMessageCodec<ReceiveMessage, SendBaseFrame>
    {
        protected override void Decode(IChannelHandlerContext ctx, ReceiveMessage msg, List<object> output)
        {
            if (msg.Frame == null)
                return;
            output.Add(msg.Frame);
        }

        protected override void Encode(IChannelHandlerContext ctx, SendBaseFrame msg, List<object> output)
        {
            output.Add(new SendMessage() { Frame = msg });
        }
    }
}
