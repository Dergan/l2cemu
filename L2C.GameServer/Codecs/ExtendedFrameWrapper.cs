﻿using System.Collections.Generic;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using L2C.GameServer.Messages;

namespace L2C.GameServer.Codecs
{



    class ExtendedFrameWrapper : MessageToMessageCodec<ExtendedReceiveBaseMessage, ExtendedSendBaseFrame>
    {
        protected override void Decode(IChannelHandlerContext ctx, ExtendedReceiveBaseMessage msg, List<object> output)
        {
            if (msg.ExtendedFrame == null)
                return;
            output.Add(msg.ExtendedFrame);
        }

        protected override void Encode(IChannelHandlerContext ctx, ExtendedSendBaseFrame msg, List<object> output)
        {
            output.Add(new ExtendedSendBaseMessage()
            {
                ExtendedFrame = msg
            });
        }
    }
}
