﻿using System.Threading.Tasks;
using L2C.GameServer.Events;
using L2C.GameServer.Messages.Send;
using L2C.GameServer.Network;

namespace L2C.GameServer.EventHandlers
{
    class LogoutEventHandler : SessionBaseEventHandler<LogoutEvent>
    {

        public LogoutEventHandler(ISessionManager sessionManager) : base(sessionManager)
        {
        }

        public override Task HandleAsync(ISession session, LogoutEvent @event)
        {
            return session.SendAsync(new LogOutOk());
        }
    }
}
