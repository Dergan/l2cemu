﻿using System.Threading.Tasks;
using L2C.GameServer.Events;
using L2C.GameServer.Messages.Send;
using L2C.GameServer.Network;

namespace L2C.GameServer.EventHandlers
{
    class PlayerInfoChangedEventHandler : ObjectBaseEventHandler<PlayerInfoChangedEvent>
    {

        public PlayerInfoChangedEventHandler(ISessionManager sessionManager) : base(sessionManager)
        {
        }

        public override Task HandleAsync(ISession session, PlayerInfoChangedEvent @event)
        {
            var ui = new UIPacket()
            {
                ObjectId = 1,
                DynamicContent = new UserInfoContent()
                {
                    //StructType = 0x19,
                    HasRoles = true,
                    UserRoles = new UserInfoUserRoles(),
                    HasBaseInfo = true,
                    BaseInfo = new UserInfoBaseInfo()
                    {
                        Name = "Lenny",
                        BuilderLevel = 0,
                        RaceId = 0,
                        Gender = 0,
                        VisibleClassId = 0,
                        ClassId = 0,
                        Level = 1
                    },
                    HasModifierInfo = true,
                    ModifierInfo = new UserInfoModifierInfo()
                    {
                        STR = 22,
                        DEX = 21,
                        CON = 27,
                        INT = 41,
                        WIT = 20,
                        MEN = 39,
                        LUC = 1,
                        CHA = 1
                    },
                    HasMaxStats = true,
                    MaxStats = new UserInfoMaxStats()
                    {
                        MaxHP = 176,
                        MaxMP = 143,
                        MaxCP = 88
                    },
                    HasCurrentStats = true,
                    CurStats = new UserInfoCurrentStats()
                    {
                        CurHP = 176,
                        CurMP = 122,
                        CurCP = 88,
                        SP = 121,
                        EXP = 35,
                        ExpPercent = 36
                    },
                    HasAttackAttributeInfo = true,
                    AttackAttributeInfo = new UserInfoAttackAttributeInfo()
                    {
                        AttackAttribute = 1,
                        AttackElementAttribute = 1
                    },
                    HasAttributes = true,
                    Attributes = new UserInfoAttributeInfo()
                    {
                        Dark = 12,
                        Earth = 13,
                        Fire = 14,
                        Holy = 15,
                        Water = 16,
                        Wind = 17
                    },
                    HasBasicStats = true,
                    BasicStats = new UserInfoBasicStats()
                    {
                        ActiveWeaponType = 0,
                        Accuracy = 18,
                        AttackSpeed = 19,
                        AttackSpeed2 = 20,
                        CastSpeed = 21,
                        Critical = 22,
                        Evasion = 23,
                        MAccuracy = 24,
                        MAtk = 25,
                        MCritical = 26,
                        Mdef = 27,
                        MEvasion = 28,
                        Patk = 29,
                        Pdef = 30
                    },
                    HasClan = true,
                    Clan = new UserInfoClanInfo(),
                    HasCollisionInfo = true,
                    CollisionInfo = new UserInfoCollisionInfo()
                    {
                        CollisionHeight = 22.3,
                        CollisionRadius = 7.5
                    },
                    HasEnchantGlow = true,
                    EnchantGlow = new UserInfoEnchantGlow(),
                    HasFameInfo = true,
                    FameInfo = new UserInfoFameInfo(),
                    HasHeadStyle = true,
                    HeadStyle = new UserInfoHeadStyleInfo(),
                    HasMovementMultiplier = true,
                    MovementMultiplier = new UserInfoMovementMultiplierInfo()
                    {
                        AttackMultiplier = 1,
                        MovementMultiplier = 20
                    },
                    HasMovementSpeed = true,
                    MovementSpeed = new UserInfoMovementSpeedInfo()
                    {
                        RunSpeed = 100,
                        RunSpeedAir = 100,
                        RunSpeedElse = 100,
                        RunSpeedWater = 100,
                        WalkSpeed = 80,
                        WalkSpeedAir = 80,
                        WalkSpeedElse = 80,
                        WalkSpeedWater = 80
                    },
                    HasPosition = true,
                    Position = new UserInfoPosition()
                    {
                        //-14453 123484 -3123
                        X = -14453,
                        Y = 123484,
                        Z = -3123,
                    },
                    HasSocialInfo = true,
                    SocialInfo = new UserInfoSocialInfo(),
                    HasStore = true,
                    Store = new UserInfoStoreInfo(),
                    HasAccessoiry = true,
                    Accessoiry = new UserInfoAccessoiryInfo(),
                    HasColorInfo = true,
                    ColorInfo = new UserInfoColorInfo()
                    {
                        NameColor = 0x00FFFFFF,
                        TitleColor = 0x00ECF9A2
                    },
                    HasMountInfo = true,
                    MountInfo = new UserInfoMountInfo(),
                    HasMovementInfo = true,
                    MovementInfo = new UserInfoMovementInfo(),
                    HasUnkParty = true,
                    UnkParty = new UserInfoPartyInfo(),
                    HasUnk = true,
                    Unkown = new UserInfoUnkownInfo()
                }
            };
            return session.SendAsync(ui);
        }
    }
}
