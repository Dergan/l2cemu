﻿using System;
using System.Threading.Tasks;
using L2C.EventBus;
using L2C.GameServer.Network;

namespace L2C.GameServer.EventHandlers
{
    abstract class ObjectBaseEventHandler<TEvent> : BaseEventHandler<TEvent>
        where TEvent : class, IEvent
    {
        private readonly ISessionManager _sessionManager;

        public ObjectBaseEventHandler(ISessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }

        public override Task HandleAsync(TEvent message)
        {
            var e = message as L2C.Shared.Events.ObjectIdBasedEvent;
            if (e == null)
                throw new InvalidOperationException("Event is not a session based event");
            return HandleAsync(_sessionManager.FindSession(e.ObjectId), message);
        }

        public abstract Task HandleAsync(ISession session, TEvent message);
    }
}
