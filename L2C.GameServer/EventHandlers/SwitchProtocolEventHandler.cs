﻿using System.Linq;
using System.Threading.Tasks;
using L2C.GameServer.Events;
using L2C.GameServer.Messages.Send;
using L2C.GameServer.Network;

namespace L2C.GameServer.EventHandlers
{
    class SwitchProtocolEventHandler : SessionBaseEventHandler<SwitchProtocolEvent>
    {

        public SwitchProtocolEventHandler(ISessionManager sessionManager) : base(sessionManager)
        {
        }

        public override Task HandleAsync(ISession session, SwitchProtocolEvent @event)
        {
            return session.SendAsync(new VersionCheck()
            {
                ProtocolOk = true,
                ServerId = 1,
                BlowfishKey = session.BlowfishKey.Take(8).ToArray(),
                IsMergedServer = false,
                ClassicMode = true,
                ArenaMode = true
            });
        }
    }
}
