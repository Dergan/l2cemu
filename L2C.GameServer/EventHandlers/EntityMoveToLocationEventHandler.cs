﻿using System.Threading.Tasks;
using L2C.GameServer.Events;
using L2C.GameServer.Messages.Send;
using L2C.GameServer.Network;

namespace L2C.GameServer.EventHandlers
{
    class EntityMoveToLocationEventHandler : ObjectBaseEventHandler<EntityMoveToLocationEvent>
    {

        public EntityMoveToLocationEventHandler(ISessionManager sessionManager) : base(sessionManager)
        {
        }

        public override Task HandleAsync(ISession session, EntityMoveToLocationEvent @event)
        {
            return session.SendAsync(new MoveToLocation()
            {
                ObjectId = session.ObjectId,
                Destination = new Messages.SerializableTypes.Vector3()
                {
                    X = @event.Destination.X,
                    Y = @event.Destination.Y,
                    Z = @event.Destination.Z,
                },
                Current = new Messages.SerializableTypes.Vector3()
                {
                    X = @event.Source.X,
                    Y = @event.Source.Y,
                    Z = @event.Source.Z,
                }
            });
        }
    }
}
