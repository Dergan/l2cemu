﻿using System.Threading.Tasks;
using L2C.GameServer.Events;
using L2C.GameServer.Messages.Send;
using L2C.GameServer.Network;

namespace L2C.GameServer.EventHandlers
{
    class RequestLobbyEventHandler : SessionBaseEventHandler<RequestLobbyEvent>
    {

        public RequestLobbyEventHandler(ISessionManager sessionManager) : base(sessionManager)
        {
        }

        public override Task HandleAsync(ISession session, RequestLobbyEvent @event)
        {
            return session.SendAsync(new CharacterSelectionInfo()
            {
                MaxCharacters = 7,
                CanCreateCharacter = false,
                PlayMode = 1,
                Characters = new System.Collections.Generic.List<CharacterSelectionInfo.CharacterInfo>()
                {
                    new CharacterSelectionInfo.CharacterInfo()
                    {
                        ObjectId = 1,
                        Name = "Test",
                        Maximum =new CharacterSelectionInfo.HealthMana()
                        {
                            HP = 1000,
                            MP = 2000
                        },
                        Current = new CharacterSelectionInfo.HealthMana()
                        {
                            HP = 900,
                            MP = 1900
                        },
                        BasicInfo = new CharacterSelectionInfo.BasicInfo()
                        {
                            SP = 1,
                            EXP = 0,
                            ExpPercent = 0.05,
                            Level = 1,
                            Karma = 0,
                            PK = 0,
                            PVP = 0
                        },
                        Position = new CharacterSelectionInfo.CharacterInfo.CharacterSelectPosition()
                        {
                            //X = 11640,
                            //Y = 59286,
                            //Z = -3424,
                            X = -14453,
                            Y = 123484,
                            Z = -3424,
                        }
                    },
                    new CharacterSelectionInfo.CharacterInfo()
                    {
                        ObjectId = 2,
                        Name="SoulDiner",
                        Position = new CharacterSelectionInfo.CharacterInfo.CharacterSelectPosition()
                        {
                            X = 11640,
                            Y = 59286,
                            Z = -3424,
                        }
                    }
                }
            });
        }
    }
}
