﻿using System.Threading.Tasks;
using L2C.EventBus;
using L2C.GameServer.Events;
using L2C.GameServer.Messages.Send;
using L2C.GameServer.Network;

namespace L2C.GameServer.EventHandlers
{
    class GameAuthenticationEventHandler : SessionBaseEventHandler<GameAuthenticationEvent>
    {
        private readonly IEventBus _eventBus;

        public GameAuthenticationEventHandler(ISessionManager sessionManager, IEventBus eventBus) : base(sessionManager)
        {
            _eventBus = eventBus;
        }

        public override async Task HandleAsync(ISession session, GameAuthenticationEvent message)
        {
            await session.SendAsync(new LoginResult());
            await _eventBus.PublishAsync(new RequestLobbyEvent(session.Id.ToString()));
        }
    }
}
