﻿namespace L2C.GameServer.Options
{
    internal class GameServerOptions
    {

        public string Host { get; set; }
        public int Port { get; set; }
    }
}
