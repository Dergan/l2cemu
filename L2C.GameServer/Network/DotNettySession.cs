﻿using System.Threading.Tasks;
using DotNetty.Transport.Channels;

namespace L2C.GameServer.Network
{
    internal class DotNettySession : ISession
    {
        private readonly IChannel _channel;

        public ISessionId Id { get; }

        public uint ObjectId { get; set; }

        public byte[] BlowfishKey { get; }

        public DotNettySession(IChannel channel, byte[] blowfishKey)
        {
            _channel = channel;
            BlowfishKey = blowfishKey;
            Id = new DotNettySessionId(_channel.Id);
        }

        public Task SendAsync<T>(T packet)
        {
            return _channel.WriteAndFlushAsync(packet);
        }
    }
}