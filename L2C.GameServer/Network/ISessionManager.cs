﻿using System;
using System.Threading.Tasks;

namespace L2C.GameServer.Network
{
    interface ISessionManager
    {

        void Add<T>(T session)
               where T : ISession;
        void Remove(ISessionId sessionId);
        ISession GetSession(ISessionId sessionId);
        ISession FindSession(string sessionId);
        ISession FindSession(uint objectId);
    }

    public interface ISessionId : IEquatable<ISessionId>
    {
        string ToString();
    }

    public interface ISession
    {
        ISessionId Id { get; }
        byte[] BlowfishKey { get; }

        uint ObjectId { get; set; }

        Task SendAsync<T>(T packet);
    }

}
