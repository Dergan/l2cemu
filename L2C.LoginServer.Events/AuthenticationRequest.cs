﻿namespace L2C.Shared.Events.Authentication
{
    public class AuthenticationRequest : SessionBasedEvent
    {


        public AuthenticationRequest(string sessionId) : base(sessionId)
        {
        }

        public string Username { get; set; }
        public string Password { get; set; }
    }
}
