﻿namespace L2C.Shared.Events.Authentication
{
    public class ServerLoginFail : SessionBasedEvent
    {



        public ServerLoginFail(string sessionId) : base(sessionId)
        {
        }

    }
}
