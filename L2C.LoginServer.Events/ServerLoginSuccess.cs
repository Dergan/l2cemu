﻿namespace L2C.Shared.Events.Authentication
{
    public class ServerLoginSuccess : SessionBasedEvent
    {



        public ServerLoginSuccess(string sessionId) : base(sessionId)
        {
        }

    }
}
