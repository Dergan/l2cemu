﻿namespace L2C.Shared.Events.Authentication
{
    public class AuthenticationSuccess : SessionBasedEvent
    {

        public AuthenticationSuccess(string sessionId) : base(sessionId)
        {
        }

    }
}
