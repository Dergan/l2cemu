﻿namespace L2C.Shared.Events.Authentication
{
    public class RequestServerList : SessionBasedEvent
    {

        public ulong Hash { get; set; }

        public RequestServerList(string sessionId) : base(sessionId)
        {
        }

    }
}
