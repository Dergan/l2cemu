﻿namespace L2C.Shared.Events.Authentication
{
    public class ServerListResponse : SessionBasedEvent
    {



        public ServerListResponse(string sessionId) : base(sessionId)
        {
        }

    }
}
