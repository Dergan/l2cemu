﻿namespace L2C.Shared.Events.Authentication
{
    public class RequestServerLogin : SessionBasedEvent
    {

        public RequestServerLogin(string sessionId) : base(sessionId)
        {
        }

    }
}
