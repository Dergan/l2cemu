﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.NpcLib.QueueMessages
{
    public class PlayerMovementMessage
    {
        public Guid PlayerId { get; set; }
        public int Location_X { get; set; }
        public int Location_Y { get; set; }
    }
}