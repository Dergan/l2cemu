﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_CharacterSelectEquipment : GameMessageBase
    {
        [Integer]
        public int Underwear { get; set; }

        [Integer]
        public int EarringLeft { get; set; }

        [Integer]
        public int EarringRight { get; set; }

        [Integer]
        public int Necklace { get; set; }

        [Integer]
        public int RingLeft { get; set; }

        [Integer]
        public int RingRight { get; set; }

        [Integer]
        public int Headgear { get; set; }

        [Integer]
        public int Weapon { get; set; }

        [Integer]
        public int Shield { get; set; }

        [Integer]
        public int Gloves { get; set; }

        [Integer]
        public int Upperbody { get; set; }

        [Integer]
        public int Lowerbody { get; set; }

        [Integer]
        public int Boots { get; set; }

        [Integer]
        public int Cloak { get; set; }

        [Integer]
        public int WeaponTwoHanded { get; set; }

        [Integer]
        public int HairAccessoryTop { get; set; }

        [Integer]
        public int HairAccessoryBottom { get; set; }

        [Integer]
        public int BraceletRight { get; set; }

        [Integer]
        public int BraceletLeft { get; set; }

        public GM_CharacterSelectionInfo_CharacterSelectEquipment()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
