﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_ResponseSecondPasswordCheck : GameMessageBase
    {
        [PacketId(new byte[] { 0xFE, 0x05 })]
        public byte[] PacketId { get; set; }

        [Byte]
        public byte Type { get; set; } = 0x01; //Verify

        [UInteger]
        public uint Unk2 { get; } = 0x02;

        [UInteger]
        public uint Unk3 { get; } = 0x00;

        public GM_ResponseSecondPasswordCheck()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
