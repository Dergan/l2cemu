﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_HeadAppearance : GameMessageBase
    {
        [Integer]
        public int HairStyle { get; set; }

        [Integer]
        public int HairColor { get; set; }

        [Integer]
        public int Face { get; set; }

        public GM_CharacterSelectionInfo_HeadAppearance()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
