﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_PedInfo : GameMessageBase
    {
        [Integer]
        public int PedId { get; set; }

        [Integer]
        public int PedLevel { get; set; }

        [Integer]
        public int PedFood { get; set; }

        [Integer]
        public int PedFoodLevel { get; set; }

        [Integer]
        public int PedMaxHP { get; set; }

        [Integer]
        public int PedMaxMP { get; set; }

        public GM_CharacterSelectionInfo_PedInfo()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
