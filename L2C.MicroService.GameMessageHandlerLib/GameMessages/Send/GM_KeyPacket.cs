﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_KeyPacket : GameMessageBase
    {
        [PacketId(0x2E)]
        public byte PacketId { get; set; }

        [Byte(1)]
        public byte ProtocolOk { get; set; }

        [ByteArray(8)]
        public byte[] XorKey { get; set; }

        [Integer(1)]
        public int CipherEnabled { get; set; }

        [Integer(1)]
        public int ServerId { get; set; }

        [Byte(1)]
        public byte IsMergedServer { get; set; }

        [Integer(0)]
        public int ObfuscationKey { get; set; }

        [Byte(0)]
        public int ClassicMode { get; set; }

        [Byte(0)]
        public byte ArenaMode { get; set; }

        public GM_KeyPacket()
        {

        }

        public GM_KeyPacket(byte[] XorKey)
        {
            this.XorKey = XorKey.Take(8).ToArray();
        }



        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
