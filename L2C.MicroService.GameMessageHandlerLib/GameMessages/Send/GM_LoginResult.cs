﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_LoginResult : GameMessageBase
    {
        [PacketId(0x0A)]
        public byte PacketId { get; set; }

        [UInteger(0xFFFFFFFF)]
        public uint Unknown1 { get; set; }

        [UInteger(0)]
        public uint Unknown2 { get; set; }
        
        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
