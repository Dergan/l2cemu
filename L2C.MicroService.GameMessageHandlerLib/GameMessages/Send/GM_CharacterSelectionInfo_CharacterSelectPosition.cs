﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_CharacterSelectPosition : GameMessageBase
    {
        [Integer]
        public int X { get; set; }

        [Integer]
        public int Y { get; set; }

        [Integer]
        public int Z { get; set; }

        public GM_CharacterSelectionInfo_CharacterSelectPosition()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
