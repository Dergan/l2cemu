﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_Appearance : GameMessageBase
    {
        [Integer]
        public int Gender { get; set; }

        [Integer]
        public int RaceId { get; set; }

        [Integer]
        public int ClassId { get; set; }

        public GM_CharacterSelectionInfo_Appearance()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
