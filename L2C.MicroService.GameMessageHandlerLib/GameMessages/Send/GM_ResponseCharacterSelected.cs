﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_ResponseCharacterSelected : GameMessageBase
    {
        [PacketId(0x0B)]
        public byte PacktId { get; set; }

        [String]
        public string Name { get; set; }

        [UInteger]
        public uint ObjectId { get; set; }

        [String]
        public string Title { get; set; }

        [UInteger]
        public uint SessionId { get; set; }

        [UInteger]
        public uint PledgeId { get; set; }

        [UInteger]
        public uint BuilderLevel { get; } = 0;

        [UInteger]
        public uint Gender { get; set; }

        [UInteger]
        public uint RaceId { get; set; }

        [UInteger]
        public uint ClassId { get; set; }

        [UInteger]
        public uint IsActive { get; set; } = 1;

        [UInteger]
        public uint X { get; set; }

        [UInteger]
        public uint Y { get; set; }

        [UInteger]
        public uint Z { get; set; }

        [Double]
        public double CurrentHP { get; set; }

        [Double]
        public double CurrentMP { get; set; }

        [Long]
        public long SP { get; set; }

        [Long]
        public long EXP { get; set; }

        [UInteger]
        public uint Level { get; set; }

        [UInteger]
        public uint Karma { get; set; }

        [UInteger]
        public uint PK { get; set; }

        [UInteger]
        public uint GameTime { get; set; }

        [UInteger]
        public uint Unk2 { get; set; }

        [UInteger]
        public uint ClassId2 { get; set; }

        [ByteArray]
        public byte[] Unk3 { get; } = new byte[80];

        [UInteger]
        public uint ObfuscationKey { get; } = 0;

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
