﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_EnchantmentLevels : GameMessageBase
    {
        [UShort]
        public ushort UpperBody { get; set; }

        [UShort]
        public ushort LowerBody { get; set; }

        [UShort]
        public ushort Headgear { get; set; }

        [UShort]
        public ushort Gloves { get; set; }

        [UShort]
        public ushort Boots { get; set; }

        public GM_CharacterSelectionInfo_EnchantmentLevels()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
