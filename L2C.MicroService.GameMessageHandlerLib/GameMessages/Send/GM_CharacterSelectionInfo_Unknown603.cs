﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_Unknown603 : GameMessageBase
    {
        [Integer]
        public int Unknown1 { get; set; }

        [Integer]
        public int Unknown2 { get; set; }

        [Integer]
        public int Unknown3 { get; set; }

        [Integer]
        public int Unknown4 { get; set; }

        [Integer]
        public int Unknown5 { get; set; }

        [Integer]
        public int Unknown6 { get; set; }

        [Integer]
        public int Unknown7 { get; set; }

        [Integer]
        public int Unknown8 { get; set; }

        [Integer]
        public int Unknown9 { get; set; }

        public GM_CharacterSelectionInfo_Unknown603()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
