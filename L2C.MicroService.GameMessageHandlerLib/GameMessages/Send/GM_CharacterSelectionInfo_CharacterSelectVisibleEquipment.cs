﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_CharacterSelectVisibleEquipment : GameMessageBase
    {
        [Integer]
        public int Weapon { get; }

        [Integer]
        public int Shield { get; }

        [Integer]
        public int Gloves { get; }

        [Integer]
        public int Upperbody { get; }

        [Integer]
        public int LowerBody { get; }

        [Integer]
        public int Boots { get; }

        [Integer]
        public int WeaponTwoHanded { get; }

        [Integer]
        public int HairAccessoryTop { get; }

        [Integer]
        public int HairAccessoryBottom { get; }

        public GM_CharacterSelectionInfo_CharacterSelectVisibleEquipment()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
