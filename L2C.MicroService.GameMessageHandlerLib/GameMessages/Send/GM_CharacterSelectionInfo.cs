﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo : GameMessageBase
    {
        [PacketId(0x09)]
        public byte PacketId { get; set; }

        [Integer]
        public int PlayerCount { get; set; }

        [Integer(7)]
        public int MaxCharacters { get; set; }

        [Byte(1)]
        public byte CanCreateCharacter { get; set; }

        [Byte(2)]
        public byte PlayMode { get; set; }

        [Integer(2)]
        public int IsKoreaClient { get; set; }

        [UShort(0)]
        public ushort SuggestPremiumAccount { get; set; }

        [List]
        public List<GM_CharacterSelectionInfo_CharacterInfo> Characters { get; set; }

        public GM_CharacterSelectionInfo()
        {
            this.Characters = new List<GM_CharacterSelectionInfo_CharacterInfo>();
        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {
            GM_CharacterSelectionInfo_CharacterInfo newChar = new GM_CharacterSelectionInfo_CharacterInfo();
            newChar.Name = "Test";
            newChar.Current.HP = 10;
            newChar.Current.MP = 11;
            newChar.Maximum.HP = 15;
            newChar.Maximum.MP = 16;
            newChar.BasicInfo.Level = 1;
            newChar.BasicInfo.EXP = 5;
            newChar.BasicInfo.ExpPercent = 12;
            newChar.BasicInfo.SP = 3;
            Characters.Add(newChar);

            PlayerCount = Characters.Count;
        }
    }
}
