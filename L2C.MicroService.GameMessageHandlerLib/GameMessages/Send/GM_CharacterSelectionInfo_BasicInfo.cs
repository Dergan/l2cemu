﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_BasicInfo : GameMessageBase
    {
        [Long]
        public long SP { get; set; }

        [Long]
        public long EXP { get; set; }

        [Double]
        public double ExpPercent { get; set; }

        [Integer]
        public int Level { get; set; }

        [Integer]
        public int Karma { get; set; }

        [Integer]
        public int PK { get; set; }

        [Integer]
        public int PVP { get; set; }

        public GM_CharacterSelectionInfo_BasicInfo()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
