﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_VitalityInfo : GameMessageBase
    {
        [Integer]
        public int VitalityPoints { get; set; } = 140000;

        [Integer]
        public int VitalityExpBonus { get; set; } = 200;

        [Integer]
        public int VitalityItemsUsed { get; set; } = 5;

        public GM_CharacterSelectionInfo_VitalityInfo()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
