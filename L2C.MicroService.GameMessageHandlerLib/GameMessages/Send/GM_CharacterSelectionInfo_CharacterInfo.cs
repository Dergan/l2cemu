﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Send
{
    public class GM_CharacterSelectionInfo_CharacterInfo : GameMessageBase
    {
        [String]
        public string Name { get; set; }

        [Integer]
        public int ObjectId { get; set; }

        [String("L2Cemu")]
        public string LoginName { get; set; }

        [Integer]
        public int SessionId { get; set; } = 1;

        [Integer]
        public int PledgeId { get; set; }

        [Integer]
        public int BuilderLevel { get; set; }

        [PacketObject]
        public GM_CharacterSelectionInfo_Appearance Appearance { get; set; } = new GM_CharacterSelectionInfo_Appearance();

        [Integer]
        public int ServerId { get; set; } = 1;

        [PacketObject]
        public GM_CharacterSelectionInfo_CharacterSelectPosition Position { get; set; } = new GM_CharacterSelectionInfo_CharacterSelectPosition();

        [PacketObject]
        public GM_CharacterSelectionInfo_HealthMana Current { get; set; } = new GM_CharacterSelectionInfo_HealthMana();

        [PacketObject]
        public GM_CharacterSelectionInfo_BasicInfo BasicInfo { get; set; } = new GM_CharacterSelectionInfo_BasicInfo();

        [PacketObject]
        public GM_CharacterSelectionInfo_Unknown603 Unk2 { get; set; } = new GM_CharacterSelectionInfo_Unknown603();

        [PacketObject]
        public GM_CharacterSelectionInfo_CharacterSelectEquipment EquippedItems { get; set; } = new GM_CharacterSelectionInfo_CharacterSelectEquipment();

        [PacketObject]
        public GM_CharacterSelectionInfo_AdditionalItems AdditionalItems { get; set; } = new GM_CharacterSelectionInfo_AdditionalItems();

        [ByteArray(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 })]
        public byte[] Unk42 { get; set; }

        [PacketObject]
        public GM_CharacterSelectionInfo_CharacterSelectVisibleEquipment VisibleEquippedItems { get; set; } = new GM_CharacterSelectionInfo_CharacterSelectVisibleEquipment();

        [PacketObject]
        public GM_CharacterSelectionInfo_EnchantmentLevels EnchantmentLevels { get; set; } = new GM_CharacterSelectionInfo_EnchantmentLevels();

        [PacketObject]
        public GM_CharacterSelectionInfo_HeadAppearance HeadAppearance { get; set; } = new GM_CharacterSelectionInfo_HeadAppearance();

        [PacketObject]
        public GM_CharacterSelectionInfo_HealthMana Maximum { get; set; } = new GM_CharacterSelectionInfo_HealthMana();

        [Integer]
        public int DeleteTime { get; set; }

        [Integer]
        public int ClassId2 { get; set; }

        [Integer]
        public int AutoSelect { get; set; } = 1;

        [Byte]
        public byte WeaponEnchantLevel { get; set; }

        [Integer]
        public int WeaponAugmentEffect1 { get; set; }

        [Integer]
        public int WeaponAugmentEffect2 { get; set; }

        [Integer]
        public int TransformID { get; set; }

        [PacketObject]
        public GM_CharacterSelectionInfo_PedInfo PedInfo { get; set; } = new GM_CharacterSelectionInfo_PedInfo();

        [PacketObject]
        public GM_CharacterSelectionInfo_VitalityInfo VitalityPoints { get; set; } = new GM_CharacterSelectionInfo_VitalityInfo();

        [Integer]
        public int IsActive2 { get; set; } = 1;

        [Bool]
        public bool IsNoble { get; set; }

        [Bool]
        public bool IsHero { get; set; }

        [Bool]
        public bool IsHairAccessoryEnabled { get; set; }

        public GM_CharacterSelectionInfo_CharacterInfo()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
