﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Receive
{
    public class GM_RequestGameStart : GameMessageBase
    {
        [PacketId(0x12)]
        public byte PacketId { get; set; }

        [Byte]
        public byte CharacterSlot { get; set; }

        [Byte]
        public byte CharacterSlotEx { get; set; }

        public GM_RequestGameStart()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
