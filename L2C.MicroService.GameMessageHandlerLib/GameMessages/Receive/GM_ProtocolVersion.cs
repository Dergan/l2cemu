﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Receive
{
    public class GM_ProtocolVersion : GameMessageBase
    {
        [PacketId(0x0E)]
        public byte PacketId { get; set; }

        [Integer]
        public int Version { get; set; }

        public GM_ProtocolVersion()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}
