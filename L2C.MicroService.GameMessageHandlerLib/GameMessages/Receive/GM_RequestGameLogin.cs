﻿using L2C.MicroService.GenericLib.GameMessageParser;
using L2C.MicroService.GenericLib.GameMessageParser.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2C.MicroService.GameMessageHandlerLib.GameMessages.Receive
{
    public class GM_RequestGameLogin : GameMessageBase
    {
        [PacketId(0x2B)]
        public byte PacketId { get; set; }

        [String]
        public string Username { get; set; }

        [Long]
        public long Hash { get; set; }

        [Long]
        public long LoginHash { get; set; }

        [Integer]
        public int Localization { get; set; }
        

        public GM_RequestGameLogin()
        {

        }

        public override void onDeserialize()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }

        public override void onSerialize()
        {

        }
    }
}