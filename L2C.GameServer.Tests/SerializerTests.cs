using System.IO;
using System.Text;
using BinarySerialization;
using L2C.GameServer.Messages.Send;
using NUnit.Framework;

namespace L2C.GameServer.Tests
{
    public class Tests
    {
        private BinarySerializer _serializer;

        [SetUp]
        public void Setup()
        {
            _serializer = new BinarySerialization.BinarySerializer()
            {
                Encoding = Encoding.UTF8,
                Endianness = Endianness.Little
            };
        }

        [Test]
        public void UserSelectInfoTest()
        {
            CharacterSelectionInfo selectionInfo = new CharacterSelectionInfo()
            {
                PlayerCount = 2,
                Characters = new System.Collections.Generic.List<CharacterSelectionInfo.CharacterInfo>()
                {
                    new CharacterSelectionInfo.CharacterInfo()
                    {
                        Name = "WorstMilk",
                        LoginName = "_2538989"
                    },
                    new CharacterSelectionInfo.CharacterInfo()
                    {
                        Name = "SoulDiner",
                        LoginName = "_2538989"
                    }
                }
            };

            using (MemoryStream stream = new MemoryStream())
            {
                _serializer.Serialize(stream, selectionInfo);
                var data = stream.ToArray();
                var str = System.BitConverter.ToString(data);
                Assert.AreEqual(16 + (564 * selectionInfo.PlayerCount), data.Length);
            }
        }

        [Test]
        public void Test1()
        {
            var d = System.BitConverter.ToDouble(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1E, 0x40 });
            d = System.BitConverter.ToDouble(new byte[] { 0xCD, 0xCC, 0xCC, 0xCC, 0xCC, 0x4C, 0x36, 0x40 });


            UIPacket ui = new UIPacket()
            {
                ObjectId = 1,
                DynamicContent = new UserInfoContent()
                {
                    //HasRoles = true,
                    //UserRoles = new UserInfoUserRoles(),
                    HasBaseInfo = true,
                    BaseInfo = new UserInfoPartWrapper<UserInfoBaseInfo>(new UserInfoBaseInfo()
                    {
                        BuilderLevel = 1,
                        ClassId = 0,
                        Gender = 0,
                        Level = 1,
                        Name = "Test",
                        RaceId = 0,
                        VisibleClassId = 0
                    }),
                    HasPosition = true,
                    Position = new UserInfoPartWrapper<UserInfoPosition>(new UserInfoPosition()
                    {
                        X = 11640,
                        Y = 59286,
                        Z = -3424,
                    })
                }
            };

            using (MemoryStream stream = new MemoryStream())
            {
                _serializer.Serialize(stream, ui);
                var data = stream.ToArray();
                var str = System.BitConverter.ToString(data);
                Assert.AreEqual("01-00-00-00-1D-00-00-00-17-00-40-00-00-18-00-04-00-54-00-65-00-73-00-74-00-01-00-00-00-00-00-00-00-00-00-00-01", str);
            }
        }
    }
}