﻿using EasyNetQ;
using L2C.MicroService.NpcLib.QueueMessages;
using System;
using System.Threading;

namespace L2C.MicroService.Npc
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=192.168.3.105"))
            {
                Random rnd = new Random();
                while (true)
                {
                    bus.Publish(new PlayerMovementMessage()
                    {
                         PlayerId = Guid.NewGuid(),
                         Location_X = rnd.Next(),
                         Location_Y = rnd.Next()
                    });
                    //Thread.Sleep(5000);
                }

            }
        }
    }
}
